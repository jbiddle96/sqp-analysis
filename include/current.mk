F90C = ifort
F90FLAGS = -cpp $(OPTS)

# OPTS = -O0 -check all -traceback -assume byterecl -convert big_endian -mkl=sequential
OPTS = -O3 -ip -assume byterecl -convert big_endian -mkl=sequential

LINK = -L$(MKLROOT)/lib/intel64/ -lmkl_lapack95_lp64

INCLUDE = -I../src/ -I$(MKLROOT)/include/


%.o: %.f
	$(F90C) $(F90FLAGS) -extend_source -c $(INCLUDE) $<

%.o: %.f90 ${LIBS}
	$(F90C) $(F90FLAGS) -c $(INCLUDE) $<

%.x: %.f90 ${LIBS}
	$(F90C) $(F90FLAGS) -o $@ $(INCLUDE) $^ $(LINK)
