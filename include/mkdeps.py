#!/usr/bin/env python3
import os
import sys
import fnmatch
import argparse


def scan_program(source):
    """
    Scan fortran source file for modules.

    Inputs:
    --------
    source - .f90 file to be searched for module dependencies

    Returns:
    --------
    module_names - List of all module names following a 'use' statement.
                   Names are all returned in lowercase.

    is_program   - Boolean indicating whether the source code is a program or module.
    """
    f = open(source, "r")
    module_names = []
    is_program = False
    for line in f:
        if(line.lstrip().lower().startswith("program")):
            is_program = True

        if(line.lstrip().lower().startswith("use")):
            line_list = line.split()
            name = remove_punctuation(line_list[1]).lower()
            module_names.append(name)

    f.close()
    return module_names, is_program


def get_name(fileName):
    """Returns filename of path without its extension.

    Inputs:
    --------
    path - Full path to a file

    Returns:
    --------
    basename - Filename with no path or extension
    """
    basename = os.path.splitext(os.path.basename(fileName))[0]
    return basename


def remove_punctuation(input_string):
    """
    Strips a input_string of punctuation
    """
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    for x in input_string:
        if x in punctuations:
            input_string = input_string.replace(x, "")

    return input_string


def find_modules(srcdir, progdir, module_names):
    """Scans directory for modules with matching names.

    Inputs:
    --------
    srcdir  - Directory to search for module dependencies

    progdir - Directory containing file being scanned for dependencies

    module_names - Names of dependencies being searched for

    Returns:
    --------
    located_modules - List of module names found in srcdir.
                      Names have a relative path but no extension.
    """
    located_modules = []
    for filename in os.listdir(srcdir):
        if (filename.endswith(".f90") or filename.endswith(".f")):
            name = get_name(filename)
            rel_dir = os.path.relpath(srcdir, progdir)
            file_match = [fnmatch.fnmatch(name.lower(), mod)
                          for mod in module_names]
            if(any(file_match)):
                located_modules.append(os.path.join(rel_dir, name))

    return located_modules


# |------------------ Script execution begins here ------------------|


parser = argparse.ArgumentParser(
    description='Generate makefile dependencies for a fortran file'
    ' from the list of included modules')
parser.add_argument('program_dir', default='.', nargs='?',
                    help='Main directory to search for program files')
parser.add_argument('src_dirs', nargs='*',
                    help='Directory list to search for module files')
parser.add_argument('--add', nargs='*', default=[],
                    help='Optional module file to force this script to search for.'
                    ' Note that all extensions should be omitted.')

args = parser.parse_args()
program_dir = args.program_dir
if(args.src_dirs):
    src_dirs = args.src_dirs
else:
    src_dirs = [program_dir]
forced_deps = args.add

# Make a list of the source code found in program_dir
programs = []
for file in os.listdir(program_dir):
    if (fnmatch.fnmatch(file, "*.f90") or fnmatch.fnmatch(file, "*.f")):
        programs.append(os.path.join(program_dir, file))

if(not programs):
    sys.exit("No program files found, exiting.")
depends = os.path.join(program_dir, "depends.mk")


dependencies = {}
for prog in programs:
    prog_name = get_name(prog)
    module_names, is_program = scan_program(prog)
    module_names += forced_deps
    # If there are modules, locate their filenames
    located_modules = []
    missing_modules = []
    if module_names:
        for src in src_dirs:
            located_modules += find_modules(src, program_dir, module_names)

        names_lower = [mod.lower() for mod in module_names]
        located_lower = [os.path.basename(mod.lower()) for mod in located_modules]
        missing_modules = list(set(names_lower)
                               .symmetric_difference(set(located_lower)))
        missing_modules = [mod + ".o" for mod in missing_modules]
        located_modules = [mod + ".o" for mod in located_modules]

    if is_program:
        prog_name = prog_name + ".x"
        dependencies[prog_name] = located_modules
    else:
        prog_name = prog_name + ".o"
        dependencies[prog_name] = located_modules

# Output result to depends.mk
f = open(depends, "w")
for key in dependencies:
    deps = " ".join(dependencies[key])
    f.write(key + ": " + deps + "\n")
f.close()

print("Generated ", depends, "\n")
