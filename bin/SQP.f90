!     The input to this program is the choice of the box to fit in and the
!     Data from James Zanotti's Wilson loops program.
!     This Program calculates the Static Quark Potential, Sommer Scales, e,l, the
!     String tension, the String tension based on a two parameter fit and
!     the physical scale of the lattice based on the Sommer scales and the
!     string tension with Jackknife errors.
!
!     Author Ben Lasscock in Honours 2002.
!     Subroutines complement, effective mass, Jack1, Jack2, Jack3 and constant
!     were written by Derek Leinweber.
!
!     This program calls the subroutines fourfit.f and minf.f
!
!

program SQP
  use Kinds
  use CorrelatorOps
  use Covariance
  use Eigensolve, only: Symmetrise, PerformVarAnalysis, nsmear
  use Strings
  implicit none

  ! I/O variables
  character(len=128) :: file_stub, fmt
  character(len=256) :: cfgBaseName
  character(len=256) :: data_dir, filename, file_list, output_dir, suffix, newSuffix
  character(len=256), dimension(:,:,:), allocatable :: cfg_names
  integer, parameter :: file_unit=100, list_unit=200, fit_unit = 300

  character(len=20) :: startDate, startTime, endDate, endTime
  integer                     :: ncon
  integer                     :: nx2,ny2,nt2 !maximum size of wilson loops
  integer, parameter          :: nd=4 !directions
  integer                     :: ismear, jsmear
  integer                     :: ize,iye
  integer                     :: itb,ite !range of t values to fit to
  integer                     :: icon
  integer                     :: xhat,that !spatial and time directions for wilson loop
  integer                     :: ix,iy,iz,it
  integer                     :: nbins, cfg_per_bin, lb, ub, ibin
  integer, dimension(:), allocatable :: smearSweeps ! Smear sweep variations

  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: avgW_total, avgW, V_win
  real(DP), dimension(:,:,:,:,:,:), allocatable   :: W, gVarJack
  real(DP), dimension(:,:,:,:,:), allocatable     :: g0Jack, V, chisqpdf
  real(DP), dimension(:,:,:,:), allocatable       :: g, C
  real(DP), dimension(:,:,:), allocatable         :: chisqfit, m
  real(DP), dimension(:,:), allocatable           :: emf, gin
  real(DP), dimension(:), allocatable             :: eVals

  real(DP), parameter :: max_chisq = 1e6
  real(DP) :: const, goodfit, chisq
  integer  :: ntb1, ntb2, nte1, nte2, ndf
  integer  :: nrx, nry, nrz, nrt
  integer  :: t0, dt
  integer  :: stat

  print *, "Please enter the data file prefix:"
  read *, file_stub
  print *, "Reading SQP parameters."
  call GetSQPParams(file_stub)
  print *, "Initialising the lattice."

  open(list_unit, file=trim(file_list), form='formatted', status='old')
  read(list_unit,*) ncon
  write(*,*) "Number of configurations: ", ncon

  ize=ny2
  iye=ny2

  print *, "Starting time"
  call date_and_time(date=startDate, time=startTime)

  print *, "trim=", trim(data_dir)

  allocate(W(nx2,0:iye,0:ize,nt2,nd,nd))
  allocate(AvgW_total(nx2,0:iye,0:ize,nt2,ncon,nsmear,nsmear))
  if(nbins > 0) then
    allocate(AvgW(nx2,0:iye,0:ize,nt2,nbins,nsmear,nsmear))
  else
    allocate(AvgW(nx2,0:iye,0:ize,nt2,ncon,nsmear,nsmear))
  end if
  allocate(cfg_names(ncon, nsmear, nsmear))

  ! Create filenames for all variational combinations
  do icon = 1,ncon
    read(list_unit,'(a256)') cfgBaseName
    do jsmear = 1,nsmear
      do ismear = 1,nsmear
        call InsertIntsInStr((/smearSweeps(ismear), smearSweeps(jsmear)/), &
            suffix, newSuffix)
        cfg_names(icon, ismear, jsmear) = trim(cfgBaseName)//trim(newSuffix)
      end do
    end do
  end do
  close(list_unit)

  avgW_total = 0.0d0

  ! Read in the Wilson loops and average over the spatial axes
  do jsmear = 1,nsmear
    do ismear = 1,nsmear
      do icon = 1,ncon
        open(file_unit,file=trim(cfg_names(icon, ismear, jsmear)),&
            form='unformatted', status='old')
        read(file_unit) nrx, nry, nrz, nrt, that
        do xhat = 1,nd-1
          read(file_unit) W(:,:,:,:,xhat,that)
        end do
        close(file_unit)

        do xhat = 1,nd-1
          avgW_total(:,:,:,:,icon,ismear,jsmear) = &
              avgW_total(:,:,:,:,icon,ismear,jsmear) + W(:,:,:,:,xhat,that)
        end do

      end do
    end do
  end do

  avgW_total = avgW_total/real(nd-1, DP)

  ! Bin data to accelerate calculation time
  if(nbins > 0) then
    cfg_per_bin = floor(real(ncon) / real(nbins))
    avgW = 0.0d0
    lb = 1
    ub = cfg_per_bin
    do ibin = 1, nbins
      avgW(:, :, :, :, ibin, :, :) = sum(avgW_total(:, :, :, :, lb:ub, :, :), dim=5) &
          / real(cfg_per_bin, DP)
      lb = ub + 1
      ub = ub + cfg_per_bin
    end do
    ncon = nbins
  else
    avgW = avgW_total
  end if

  allocate(V(nx2,0:iye,0:ize,0:ncon+2,0:ncon+1))
  allocate(gVarJack(nt2,0:ncon+2,0:ncon+1,0:ncon+1,nsmear,nsmear))
  allocate(g0Jack(nt2,0:ncon+2,0:ncon+1,0:ncon+1,nsmear))

  allocate(gin(nt2,ncon))
  allocate(g(nt2,0:ncon+2,0:ncon+1,0:ncon+1))
  allocate(emf(nt2,0:ncon+2))
  allocate(m(1,0:ncon+2,0:ncon+1))
  allocate(C(nt2 - 1, nt2 - 1, 0:ncon, 0:ncon))
  allocate(eVals(nsmear))

  deallocate(W)
  deallocate(avgW_total)


  !cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
  !     The Subroutine Complement generates the Averages.
  !     The Subroutine fitcf takes these in and output's a set of ncon potential datas with ncon errors,
  !     and one set of central potential data with error.
  !     Input the ncon potential datas with ncon errors to fourfit and get a set of ncon parameter values.
  !     Jack1 these and get ONE error for the parameter.
  !     Input the set of central potenial data and with error into fourfit and get a central value for the
  !     parameter.
  !     Thereby getting a parameter value with Jackknife error.
  !     Note: m(1,:,:) has the '1' to conform with the Jack subroutines,
  !     the first dimension has no special significance
  !cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

  allocate(chisqpdf(nx2,0:iye,0:ize,ntb1:ntb2,nte1:nte2))
  allocate(chisqfit(nx2,0:iye,0:ize))
  allocate(V_win(nx2,0:iye,0:ize,0:ncon+2,0:ncon+1,ntb1:ntb2,nte1:nte2))

  write(*,*) "Fitting energies"
  do iz=0,ize
    do iy=0,iye
      do ix=1,nx2
        ! print *,  "Fitting energies", ix,iy,iz

        do jsmear = 1,nsmear
          do ismear = 1,nsmear

            gIn(:,:) = avgW(ix,iy,iz,:,:,ismear,jsmear)
            ! Generates a jackknife ensemble in gVarJack
            ! Repeated for each smearing level
            ! gVarJack(it,0,0,0)      stores the total ensemble average for time it
            ! gVarJack(it,ncon+1,0,0) stores the total jacknife error
            ! gVarJack(it,i,0,0)      stores the i'th jacknife average
            ! gVarJack(it,i,ncon+1,0) stores the i'th jacknife error
            ! Other combinations store higher order jacknifes
            ! See legacy.f90 for more detail
            ! Looks like complement doesn't do the errors, just the ensembles
            call Complement(gIn, gVarJack(:,:,:,:,ismear,jsmear), ncon, nt2)

          end do
        end do

        call Symmetrise(gVarJack(:,:,:,:,:,:), ncon)

        ! Solve the generalised eigenvalue problem and return
        ! the projected energy levels, g0Jack
        if(nsmear > 1) then
          call PerformVarAnalysis(gVarJack, t0, dt, ncon, g0Jack, eVals, stat)
          ! Only consider the lowest energy level
          g = g0Jack(:, :, :, :, 1)
        else
          g = gVarJack(:, :, :, :, 1, 1)
          stat = 0
        end if

        if(stat .ne. 0) then
          write(*,*)
          write(*,*) "!------------------------------------------------------------------------!"
          write(*,*) "Generalised eigensolver failed, terminating"
          write(*,*) "Failed at (x,y,z) = ", ix, iy, iz
          write(*,*) "!------------------------------------------------------------------------!"
          write(*,*)
          stop
        end if

        CALL GetEffMass(g,ncon,nt2)
        ! g is returned as above, but now contains the effective mass
        ! The effective mass is the potential we're looking for
        emf(:,0:ncon+2) = g(:,0:ncon+2,0,0)
        ! emf now stores only the first order jacknife results

        call GetCovarMat(g, C, ncon, nt2 - 1)
        do itb=ntb1,ntb2
          do ite=itb+1,nte2

            ! print *,  "covarconst", itb, ite, ix, iy, iz
            ! call covarconst(emf(:,0:ncon+2),itb,ite,const,chisq,ndf,goodfit)
            ! Fit the emf over the range itb to ite to a constant, const
            ! chisq is the chisquared value of the fit
            ! ndf is the number of required parameters to perform the fit (max 5)
            ! goodfit is the probability that a value at least as large as "chisq"
            ! will be obtained by chance
            ! Want this to not be too small for some reason?
            ! print '(a,7I4)',  "return", ix,iy,iz,ncon,itb,ite,nt2
            ! print '(10I4)',  shape(chisqpdf), lbound(chisqpdf)
            ! print '(10I4)',  shape(g), lbound(g)

            ! ndf = window size (ite - itb + 1) - numparam (1)
            ndf = ite - itb
            call fitcf(g(1:nt2 - 1, :, :, :),C,m,ncon,itb,ite,nt2 - 1, chisq)
            chisqpdf(ix,iy,iz,itb,ite) = chisq/ndf
            ! fitcf also fits the first and second order jacknife ensembles in g
            ! to constants stored in m
            ! Fitting is done via the covariance matrix C
            ! Fitting is done over g(itb:ite,:,:,:)
            ! m is structured as follows:
            ! m(1,0,0)          stores the fit of g(itb:ite,0,0,0)
            ! m(1,ncon+1,0)     stores the first order jackknife error of the above
            ! m(1,icon,0)       stores the fit of g(itb:ite,icon,0,0)
            ! m(1,icon,ncon+1)  stores the jackknife error of the above
            ! m(1,icon,jcon)    stores the fit of g(itb:ite,icon,jcon,0)

            V_win(ix,iy,iz,:,:,itb,ite) = m(1,:,:)

          end do
        end do

      enddo
    enddo
  enddo


  ! Output potential data to file
  write(filename, '(2a,2(i0,a))') trim(output_dir), "/fit_data_t0", t0, "-dt", dt, ".dat"
  open(fit_unit,file=trim(filename),form='formatted',status='replace',action='write')
  write(fit_unit, '(6a9,3a25)') "x", "y", "z", "icon", "t_min", "t_max", "V", "V_err", "chisq/dof"
  fmt = '(6(i8,x),3(f30.16,x))'
  do itb=ntb1,ntb2
    do ite=itb+1,nte2
      do iz=0,ize
        do iy=0,iye
          do ix=1,nx2
            ! Prevent writing of overflowing chisq values
            if(abs(chisqpdf(ix, iy, iz, itb, ite)) < max_chisq) then
              ! if(chisqpdf(ix, iy, iz, itb, ite) > 0) then
              write(fit_unit,fmt) ix, iy, iz, 0, itb, ite, &
                  V_win(ix, iy, iz, 0, 0, itb, ite), &
                  V_win(ix, iy, iz, ncon + 1, 0, itb, ite), chisqpdf(ix, iy, iz, itb, ite)
              do icon = 1, ncon
                write(fit_unit,fmt) ix, iy, iz, icon, itb, ite, &
                    V_win(ix, iy, iz, icon, 0, itb, ite), &
                    V_win(ix, iy, iz, icon, ncon + 1, itb, ite), chisqpdf(ix, iy, iz, itb, ite)
              end do
            end if
          end do
        end do
      end do
    end do
  end do
  close(fit_unit)
  print *, "Done"


  deallocate(V_win)
  deallocate(chisqpdf)
  deallocate(AvgW)
  deallocate(V)
  deallocate(gin)
  deallocate(g)
  deallocate(emf)

  call date_and_time(date=endDate, time=endTime)
  print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
  print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)

contains


  subroutine GetSQPParams(file_stub)
    character(len=*) :: file_stub
    character(len=256) :: propfile
    integer :: file_unit = 301
    propfile = trim(file_stub)//".sqp"
    open(file_unit,file=trim(propfile),action='read')
    read(file_unit,'(a256)') data_dir
    read(file_unit,'(a256)') file_list
    read(file_unit,'(a256)') suffix
    read(file_unit,*) nx2
    read(file_unit,*) ny2
    read(file_unit,*) nt2
    read(file_unit,*) ntb1
    read(file_unit,*) ntb2
    read(file_unit,*) nte2
    read(file_unit,*) nsmear
    allocate(smearSweeps(nsmear))
    read(file_unit,*) smearSweeps
    if(nsmear > 1) then
      read(file_unit, *) t0
      read(file_unit, *) dt
    end if
    read(file_unit,'(a256)') output_dir
    read(file_unit,*) nbins
    close(file_unit)

    print *, "What directory is the data in: "//trim(data_dir)
    write(*,*) "Filename containing list of base config names: "//trim(file_list)
    write(*,*) "Base suffix:"//trim(suffix)
    print *, "On-axis size, nx2 = "//str(nx2,12)
    print *, "Off-axis size, ny2 = "//str(ny2,12)
    print *, "Time size: "//str(nt2,12)
    print *, "min start time: "//str(ntb1,12)
    print *, "max start time: "//str(ntb2,12)
    print *, "max end time: "//str(nte2,12)
    write(*,*) "Number of smearing variations: "//str(nsmear,12)
    write(*,'(a)',advance="no") "Smear sweeps: "
    write(*,*) smearSweeps
    print *, "Variational t0: "//str(t0, 12)
    print *, "Variational dt: "//str(dt, 12)
    print *, "Directory to deposit potential data: "//trim(output_dir)
    print *, "Number of bins to accumulate data: "//str(nbins, 12)
    nte1 = ntb1 +1

  end subroutine GetSQPParams


end program SQP
