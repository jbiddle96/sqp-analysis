program EffectiveMass
  use Kinds
  use Strings
  use CorrelatorOps
  use EigenSolve, only: Symmetrise, Normalise, PerformVarAnalysis, &
      SortEMF, nsmear, analyse_correlators
  implicit none

  integer :: nx2, ny2, nt2                  ! Maximum size of wilson loops
  integer :: nrx, nry, nrz, nrt, xhat, that
  integer :: nye, nze
  integer :: ncon                           ! Number of configurations
  integer :: t0_min, t0_max, dt_min, dt_max ! Variational parameters
  integer, parameter :: nd=4                ! Number of directions
  integer, dimension(:), allocatable :: smearSweeps ! Smear sweep variations
  logical :: sortByEMF, skip_variation

  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: avgW, avgW_total
  real(DP), dimension(:,:,:,:,:,:), allocatable   :: W
  real(DP), dimension(:,:,:,:,:,:), allocatable   :: gVarJack
  real(DP), dimension(:,:,:,:,:), allocatable     :: g0Jack
  real(DP), dimension(:,:,:,:), allocatable       :: C, g_single
  real(DP), dimension(:,:,:), allocatable         :: emf, emfErr, norm_corr_components
  real(DP), dimension(:,:), allocatable           :: gIn
  real(DP), dimension(:), allocatable             :: eVals, corr_mat_ratio

  integer :: icon, jcon, ismear, jsmear
  integer :: ix, iy, iz, it, it0, idt
  integer :: nbins, cfg_per_bin, lb, ub, ibin
  integer :: stat

  ! I/O variables
  character(len=256), dimension(:,:,:), allocatable :: cfgNames
  character(len=256) :: fileStub, fileList, dir, cfgBaseName
  character(len=256) :: emfFilename, potentialDir, cFilename, corrFilename, evalFilename
  character(len=256) :: cmrFilename, comFilename, var_corrFilename
  character(len=64)  :: baseSuffix, newSuffix
  character(len=64)  :: fmt
  integer :: emf_unit, c_unit, ifile, single_unit, corr_unit, eval_unit, cmr_unit, com_unit
  integer, parameter :: file_unit=100, list_unit=200
  integer, parameter :: emf_unit_start=201, c_unit_start=301
  integer, parameter :: single_unit_start=401, corr_unit_start = 501
  integer, parameter :: eval_unit_start = 601, cmr_unit_start = 701
  integer, parameter :: com_unit_start = 801

  !!------------------------- Get parameters -------------------------!!

  write(*,*) "Please enter the data file prefix:"
  read(*,*) fileStub
  write(*,*) "Reading EMF parameters"
  call GetEMFParams(fileStub)

  open(list_unit, file=trim(fileList), form='formatted', status='old')
  read(list_unit,*) ncon
  write(*,*) "Number of configurations: ", ncon
  nze=ny2
  nye=ny2
  allocate(cfgNames(ncon, nsmear, nsmear))
  allocate(W(nx2,0:nye,0:nze,nt2,nd,nd))
  if(nbins > 0) then
    allocate(AvgW(nx2,0:nye,0:nze,nt2,nbins,nsmear,nsmear))
  else
    allocate(AvgW(nx2,0:nye,0:nze,nt2,ncon,nsmear,nsmear))
  end if
  allocate(avgW_total(nx2,0:nye,0:nze,nt2,ncon,nsmear,nsmear))

  !!--------------------------- Read data ---------------------------!!

  ! Create filenames for all variational combinations
  do icon = 1,ncon
    read(list_unit,'(a256)') cfgBaseName
    do jsmear = 1,nsmear
      do ismear = 1,nsmear
        call InsertIntsInStr((/smearSweeps(ismear), smearSweeps(jsmear)/), &
            baseSuffix, newSuffix)
        cfgNames(icon, ismear, jsmear) = trim(cfgBaseName)//trim(newSuffix)
      end do
    end do
    ! write(*,*)
  end do
  close(list_unit)

  avgW_total = 0.0d0

  ! Read in the Wilson loops and average over the spatial axes
  do jsmear = 1,nsmear
    do ismear = 1,nsmear
      do icon = 1,ncon

        ! write(*,*) "Reading ", trim(cfgNames(icon, ismear, jsmear))
        open(file_unit,file=trim(cfgNames(icon, ismear, jsmear)),&
            form='unformatted', status='old')
        read(file_unit) nrx, nry, nrz, nrt, that
        do xhat = 1,nd-1
          read(file_unit) W(:,:,:,:,xhat,that)
        end do
        close(file_unit)

        do xhat = 1,nd-1
          avgW_total(:,:,:,:,icon,ismear,jsmear) = &
              avgW_total(:,:,:,:,icon,ismear,jsmear) + W(:,:,:,:,xhat,that)
        end do

      end do
    end do
  end do

  avgW_total = avgW_total/real(nd-1, DP)

  ! Bin data to accelerate calculation time
  if(nbins > 0) then
    cfg_per_bin = floor(real(ncon) / real(nbins))
    avgW = 0.0d0
    lb = 1
    ub = cfg_per_bin
    do ibin = 1, nbins
      avgW(:, :, :, :, ibin, :, :) = sum(avgW_total(:, :, :, :, lb:ub, :, :), dim=5) &
          / real(cfg_per_bin, DP)
      lb = ub + 1
      ub = ub + cfg_per_bin
    end do
    ncon = nbins
  else
    avgW = avgW_total
  end if

  allocate(gVarJack(nt2,0:ncon+2,0:ncon+1,0:ncon+1,nsmear,nsmear))
  allocate(g_single(nt2,0:ncon+2,0:ncon+1,0:ncon+1))
  allocate(g0Jack(nt2,0:ncon+2,0:ncon+1,0:ncon+1,nsmear))
  allocate(gIn(nt2,ncon))
  allocate(C(nt2-1,nt2-1,0:ncon,nsmear))
  allocate(emf(nt2,0:ncon,nsmear))
  allocate(emfErr(nt2,0:ncon,nsmear))
  allocate(eVals(nsmear))
  allocate(corr_mat_ratio(nt2))
  allocate(norm_corr_components(nt2, nsmear, nsmear))

  deallocate(W)


  !!------------------------ Perform analysis ------------------------!!

  skip_variation = .false.
  call open_files()

  ! Perform variational analysis to extract an optimised emf
  do iz = 0,nze
    do iy = 0,nye
      do ix = 1,nx2

        single_unit = single_unit_start
        corr_unit = corr_unit_start
        do jsmear = 1,nsmear
          do ismear = 1,nsmear
            gIn(:,:) = avgW(ix,iy,iz,:,:,ismear,jsmear)
            call Complement(gIn, gVarJack(:,:,:,:,ismear,jsmear), ncon, nt2)

            ! Output individual emf and correlator data
            g_single = gVarJack(:,:,:,:,ismear,jsmear)

            do it = 1,nt2-1
              write(corr_unit, '(6i4,2f)') ix, iy, iz, it, 1, 0,&
                  g_single(it,0, 0, 0), g_single(it, ncon + 1, 0, 0)
            end do
            corr_unit = corr_unit + 1

            call GetEffMass(g_single, ncon, nt2)
            do it = 1,nt2-1
              write(single_unit, '(6i4,2f)') ix, iy, iz, it, 1, 0,&
                  g_single(it,0, 0, 0), g_single(it, ncon + 1, 0, 0)
            end do
            single_unit = single_unit + 1

          end do
        end do

        ! Use improved estimator g = (g(i,j) + g(j,i))/2
        ! Symmetrise zeroth, first and second order ensembles
        if (nsmear > 1) then
          call analyse_correlators(gVarJack(:,0,0,0,:,:), &
              corr_mat_ratio, norm_corr_components)
          do it = 1, nt2
            write(cmr_unit, '(4i4,f)') ix, iy, iz, it, corr_mat_ratio(it)
            write(fmt, *) '(4i4,2(', nsmear**2, 'f))'
            write(com_unit, fmt) ix, iy, iz, it, &
                norm_corr_components(it, :, :)
          end do
          call Symmetrise(gVarJack(:,:,:,:,:,:), ncon)
          call Normalise(gVarJack(:,:,:,:,:,:), ncon)
        end if
        g0Jack = 0.0d0

        emf_unit = emf_unit_start
        c_unit = c_unit_start
        eval_unit = eval_unit_start

        ! Perform variational analysis
        if( .not. skip_variation) then
          outer: do idt = dt_min,dt_max
            inner: do it0 = t0_min,t0_max

              if(nsmear > 1) then
                call PerformVarAnalysis(gVarJack, it0, idt, ncon, g0Jack, eVals, stat, jack_level=2)
              else
                g0Jack(:,:,:,:,:) = gVarJack(:, :, :, :, :, 1)
                stat = 0
              end if

              if(stat .ne. 0) then
                write(*,*)
                write(*,*) "!------------------------------------------------------------------------!"
                write(*,*) "Generalised eigensolver failed, skipping remainder of variational analysis"
                write(*,*) "Failed at (x,y,z) = ", ix, iy, iz
                write(*,*) "Correlator files will be output to facilitate debugging"
                write(*,*) "!------------------------------------------------------------------------!"
                write(*,*)
                skip_variation = .true.
                exit outer
              end if

              ! Calculate EMF and errors from the jackknife ensemble
              ! This recalculates the jackknife errors
              ! Note that for the emf, values at t=nt2 are garbage and are
              ! omitted from output
              do ismear=1,nsmear
                call GetEffMass(g0Jack(:,:,:,:,ismear), ncon, nt2)
              end do

              ! Sort EMF values from low to high to isolate the lowest energy
              ! state
              ! Note that if errors are large this will likely misclassify points
              if(sortByEMF) call SortEMF(g0Jack(:,:,:,:,:), ncon, nt2)

              ! Generate covariance matrix to facilitate fitting analysis
              do ismear=1,nsmear
                call GetCovarMat(g0Jack(:,:,:,:,ismear), C(:, :, :, ismear), &
                    ncon, nt2-1)
              end do

              ! Write output
              emf(:,:,:) = g0Jack(:,0:ncon,0,0,:)
              emfErr(:,0,:) = g0Jack(:,ncon + 1,0,0,:)
              emfErr(:,1:ncon,:) = g0Jack(:,1:ncon,ncon+1,0,:)

              do it = 1,nt2-1
                do ismear = 1,nsmear
                  do icon = 0,ncon
                    write(emf_unit, '(6i4,2f)') ix, iy, iz, it, ismear, icon,&
                        emf(it,icon,ismear), emfErr(it,icon,ismear)
                  end do
                end do
              end do

              write(fmt, *) '(5i4,', nt2**2, 'f)'
              do ismear = 1,nsmear
                do icon = 0,ncon
                  write(c_unit,fmt) ix, iy, iz, ismear, icon, &
                      C(:,:,icon,ismear)
                end do
              end do

              if(nsmear > 1) then
                write(fmt, *) '(3i4,2(', nsmear, 'f))'
                write(eval_unit, '(3i4,3f)') ix, iy, iz, eVals
              end if

              emf_unit = emf_unit + 1
              c_unit = c_unit + 1
              eval_unit = eval_unit + 1

            end do inner
          end do outer
        end if

      end do
    end do
  end do

  call close_files()

  write(*,*) "Finished"

contains

  subroutine GetEMFParams(fileStub)
    character(len=*) :: fileStub
    character(len=256) :: propfile
    integer :: file_unit = 301
    propfile = trim(fileStub)//".emf"
    open(file_unit,file=trim(propfile),action='read')
    read(file_unit,'(a256)') dir
    read(file_unit,'(a256)') fileList
    read(file_unit,'(a64)')  baseSuffix
    read(file_unit,*) nx2
    read(file_unit,*) ny2
    read(file_unit,*) nt2
    read(file_unit,*) nsmear
    allocate(smearSweeps(nsmear))
    read(file_unit,*) smearSweeps
    if(nsmear > 1) then
      read(file_unit,*) t0_min
      read(file_unit,*) t0_max
      read(file_unit,*) dt_min
      read(file_unit,*) dt_max
      read(file_unit,*) sortByEMF
    else
      t0_min = 1
      t0_max = 1
      dt_min = 1
      dt_max = 1
      sortByEMF = .False.
    end if
    read(file_unit,'(a256)') potentialDir
    read(file_unit,*) nbins
    close(file_unit)

    write(*,*) "What directory is the data in: "//trim(dir)
    write(*,*) "Filename containing list of base config names: "//trim(fileList)
    write(*,*) "Base suffix:"//trim(baseSuffix)
    write(*,*) "On-axis size, nx2 = "//str(nx2,12)
    write(*,*) "Off-axis size, ny2 = "//str(ny2,12)
    write(*,*) "Time size: "//str(nt2,12)
    write(*,*) "Number of smearing variations: "//str(nsmear,12)
    write(*,'(a)',advance="no") "Smear sweeps: "
    write(*,*) smearSweeps
    if (nsmear > 1) then
      write(*,*) "Min variational start time: "//str(t0_min,12)
      write(*,*) "Max variational start time: "//str(t0_max,12)
      write(*,*) "Min variational time step: "//str(dt_min,12)
      write(*,*) "Max variational time step: "//str(dt_max,12)
      if(sortByEMF) then
        write(*,*) "Sorting by EMF values"
      else
        write(*,*) "Not sorting by EMF values"
      end if
    end if
    write(*,*) "Directory to deposit potential data: "//trim(potentialDir)
    write(*,*) "Number of bins to accumulate data: "//str(nbins, 12)
  end subroutine GetEMFParams


  subroutine open_files()
    ! Open output files
    emf_unit = emf_unit_start
    c_unit = c_unit_start
    eval_unit = eval_unit_start
    do idt = dt_min,dt_max
      do it0 = t0_min,t0_max
        if(nsmear > 1) then
          write(emfFilename, '(2a,2(i0,a))') trim(potentialDir), &
              "emf_t0", it0, "dt", idt, ".dat"

          write(cFilename, '(2a,2(i0,a))') trim(potentialDir), &
              "C_t0", it0, "dt", idt, ".dat"

          ! Only write eigenvalues if nsmear > 1
          write(evalFilename, '(2a,2(i0,a))') trim(potentialDir), &
              "eval_t0", it0, "dt", idt, ".dat"
          write(*,*) "Opening: ", trim(evalFilename)
          open(eval_unit, file=evalFilename, form='formatted',&
              action='write', status='replace')
          eval_unit = eval_unit + 1

        else
          write(emfFilename, '(2a)') trim(potentialDir), "emf.dat"
          write(cFilename, '(2a)') trim(potentialDir), "C.dat"
        end if
        write(*,*) "Opening: ", trim(emfFilename)
        open(emf_unit, file=emfFilename, form='formatted',&
            action='write', status='replace')
        emf_unit = emf_unit + 1

        write(*,*) "Opening: ", trim(cFilename)
        open(c_unit, file=cFilename, form='formatted',&
            action='write', status='replace')
        c_unit = c_unit + 1
      end do
    end do

    ! Open individual emf output files
    single_unit = single_unit_start
    corr_unit = corr_unit_start
    do jsmear = 1, nsmear
      do ismear = 1, nsmear

        if(nsmear > 1) then
          write(emfFilename, '(2a,i0,a,i0,a)') trim(potentialDir), &
              "emf_single_i", ismear, "j", jsmear, ".dat"
          write(corrFilename, '(2a,i0,a,i0,a)') trim(potentialDir), &
              "corr_single_i", ismear, "j", jsmear, ".dat"
        else
          write(emfFilename, '(2a)') trim(potentialDir), &
              "emf_single.dat"
          write(corrFilename, '(2a)') trim(potentialDir), &
              "corr_single.dat"
        end if

        write(*,*) "Opening: ", trim(emfFilename)
        open(single_unit, file=emfFilename, form='formatted',&
            action='write', status='replace')

        write(*,*) "Opening: ", trim(corrFilename)
        open(corr_unit, file=corrFilename, form='formatted',&
            action='write', status='replace')
        single_unit = single_unit + 1
        corr_unit = corr_unit + 1
      end do
    end do

    ! Open average off-diagonal correlation matrix files
    if(nsmear > 1) then
      cmr_unit = cmr_unit_start
      cmrFilename = trim(potentialDir)//"avg_corr_off-on_diag_ratio.dat"
      write(*,*) "Opening: ", trim(cmrFilename)
      open(cmr_unit, file=cmrFilename, form='formatted', &
          action='write', status='replace')

      com_unit = com_unit_start
      comFilename = trim(potentialDir)//"norm_corr_components.dat"
      write(*,*) "Opening: ", trim(comFilename)
      open(com_unit, file=comFilename, form='formatted', &
          action='write', status='replace')

    end if
  end subroutine open_files


  subroutine close_files()
    ! Close all output files
    do ifile = emf_unit_start, emf_unit-1
      close(ifile)
    end do

    do ifile = c_unit_start, c_unit-1
      close(ifile)
    end do

    do ifile = single_unit_start, single_unit-1
      close(ifile)
    end do

    do ifile = corr_unit_start, corr_unit-1
      close(ifile)
    end do

    if (nsmear > 1) then
      do ifile = eval_unit_start, eval_unit-1
        close(ifile)
      end do
      close(cmr_unit)
      close(com_unit)
    end if

  end subroutine close_files
end program EffectiveMass
