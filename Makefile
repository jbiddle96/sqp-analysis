BIN = ./bin/
SRC = ./src/

SUBDIRS = ${BIN} ${SRC}

all: depends ${SUBDIRS}

${SUBDIRS}:
	${MAKE} -C $@

# Generate dependency files
depends:
	python3 ./include/mkdeps.py ${BIN} ${SRC}
	python3 ./include/mkdeps.py ${SRC}

${BIN}: ${SRC}

${SRC}:

# Remove .mod .o .x files
clean:
	$(foreach dir, ${BIN} ${SRC}, ${MAKE} -C ${dir} clean;)

.PHONY: clean all depends ${SUBDIRS}
