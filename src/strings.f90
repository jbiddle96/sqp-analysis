
module Strings
  use Kinds

  implicit none
  private

  interface str
     module procedure logical_to_str
     module procedure integer_to_str
     module procedure integer_1_to_str
     module procedure real_to_str_sp
     module procedure real_to_str_dp
     module procedure complex_to_str_sc
     module procedure complex_to_str_dc
  end interface

  public :: str

  public :: char2real
  public :: strip_char
  public :: strip_comment

  public :: get_key_value

  public :: InsertIntsInStr

contains

  pure function logical_to_str(a,width,fmt) result (s)
    logical, intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function logical_to_str

  pure function integer_to_str(a,width,fmt) result (s)
    integer, intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function integer_to_str

  pure function integer_1_to_str(a,width,fmt) result (s)
    integer, dimension(:), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function integer_1_to_str

  pure function real_to_str_sp(a,width,fmt) result (s)
    real(sp), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function real_to_str_sp

  pure function real_to_str_dp(a,width,fmt) result (s)
    real(dp), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function real_to_str_dp

  pure function complex_to_str_sc(a,width,fmt) result (s)
    complex(sc), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if

  end function complex_to_str_sc

  pure function complex_to_str_dc(a,width,fmt) result (s)
    complex(dc), intent(in) :: a
    integer, intent(in) :: width
    character(len=*), intent(in), optional :: fmt

    character(len=width) :: s
    character(len=80) :: s2
    character, parameter :: star='*'

    if ( present(fmt) ) then
       write(s,fmt) a
    else
       s = ' '
       write(s2,*) a
       if ( len(trim(adjustl(s2))) > width ) then
          s = star
       else
          s = trim(adjustl(s2))
       end if
    end if
  end function complex_to_str_dc

  pure function get_key_value(str,key,sep) result (val)
    character(len=*), intent(in) :: str,key,sep
    character(len=:), allocatable :: val

    integer :: ik, ib, ie

    ik = index(str,key)
    if ( ik > 0 ) then
       ib = ik + len(key)
       ie = scan(str(ib:),sep)
       if ( ie > 0 ) then
          ie = ib + ie -2
       else
          ie = len(str)
       end if
       val = str(ib:ie)
    else
       val = "ERR_KEY_NOT_FOUND"
    end if

  end function get_key_value

  !> Removes everything after com from the string str.
  pure function strip_comment( str, com )
    character(len=*), intent(in) :: str
    character(len=*), intent(in) :: com
    character(len=len(str)) :: strip_comment

    integer :: i

    i = index( trim( str ), trim( com ) )

    if (i == 0) then
       strip_comment = str
    else
       strip_comment = str(1:i-1)
    end if

  end function strip_comment

  !> Removes the character char from character string str.
  pure function strip_char( str, char )
    character(len=*), intent(in) :: str
    character(len=1), intent(in) :: char
    character(len=len(str)) :: strip_char
    integer :: i, j

    strip_char = ""
    j = 0
    do i = 1, len( str )
       if (str(i:i) /= char) then
          j = j + 1
          strip_char(j:j) = str(i:i)
       end if
    end do

  end function strip_char

  !> Converts a character string to real(dp). Can only handle
  !! /, * operators and sqrt() function. Does not support brackets or sqrt()
  !! functions inside other sqrt() functions.
  pure recursive real(dp) function char2real( str )
    character(len=*), intent(in) :: str

    character(len=len(str)+1) :: d
    integer :: ii, ib, ie

    char2real = 1.0_dp
    d = strip_char( str, " " )

    ! process for conversion:
    ! - work from right to left, this means 1.0/2.0*3.0 = 1.5 and not 1.0/6.0 = 0.1667
    ! - check to see if we have a sqrt function
    !   .true. => skip to work out what is in brackets
    !   .false. => read string until we hit a / or sqrt function

    ii = len( trim( d ) )
    do
       select case (d(ii:ii))
       case ("/")
          char2real = char2real( d(1:ii-1) )/char2real
          ii = 0
       case ("*")
          char2real = char2real( d(1:ii-1) )*char2real
          ii = 0
       case (")")
          ! need contents of the sqrt function, end is easy
          ie = ii - 1

          ! now find where sqrt begins
          ib = ie - 1
          get_closing_bracket: do
             select case(d(ib:ib))
             case ("(")
                ! found closing bracket
                exit get_closing_bracket
             case default
                ib = ib - 1
             end select
          end do get_closing_bracket

          char2real = sqrt( char2real( d(ib+1:ie) ) )
          ii = ib - 5
       case default
          ! have a number
          ie = ii
          ib = ie + 1

          find_end_of_num: do
             select case(d(ib:ib))
             case ("/", "*", ")")
                exit find_end_of_num
             case default
                ib = ib - 1
             end select

             if (ib == 0) exit find_end_of_num
          end do find_end_of_num

          read(d(ib+1:ie),*) char2real
          ii = ib
       end select

       if (ii <= 0) exit
    end do

  end function char2real

  subroutine InsertIntsInStr(numList, baseStr, finalStr)
    ! Insert an array of integers into locations in baseStr
    ! Locations are denoted by a hyphen, '-'
    integer, dimension(:) :: numList
    character(len=*) :: baseStr, finalStr

    integer :: iNum, nInsertions, baseLength, iChar, jChar
    integer, dimension(:), allocatable :: numLens
    character(len=10), dimension(:), allocatable :: numStrings

    finalStr = ""
    baseLength=len(trim(baseStr))
    nInsertions = size(numList)
    allocate(numStrings(nInsertions))
    allocate(numLens(nInsertions))

    do iChar = 1,nInsertions
      write(numStrings(iChar), '(i0)') numList(iChar)
      numLens(iChar) = len(trim(numStrings(iChar)))
    end do

    iNum = 1
    jChar = 1
    ! Place numbers in string
    ! iChar tracks position in baseStr
    ! jChar tracks position in finalStr
    ! iNum tracks current number to be inserted
    do iChar = 1,baseLength
      if(baseStr(iChar:iChar) == '-') then
        finalStr(jChar:jChar + numLens(iNum)) = trim(numStrings(iNum))
        jChar = jChar + numLens(iNum)
        iNum = iNum + 1
      else
        finalStr(jChar:jChar) = baseStr(iChar:iChar)
        jChar = jChar + 1
      end if
    end do

    deallocate(numStrings)
    deallocate(numLens)

  end subroutine InsertIntsInStr

end module Strings
