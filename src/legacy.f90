module Legacy
  use Kinds
  implicit none

contains

!******** ********* ********* ********* ********* ********* ********* **
!
  SUBROUTINE complement(gin,g,ncon,nt)
    !
    !  Produces averages over all but 0, all but 1, all but 2, all but 3
    !  configurations, arranged according to :
    !    g(0,     0,     0)      average over all configurations
    !    g(ncon+1,0,     0)      will be jackknifed error for g(0,0,0)
    !    g(ncon+2,0,     0)      will be gavg - g(0,0,0)
    !    g(i,     0,     0)      average over all but 'i'th conf.,
    !                              (i=1,ncon)
    !    g(i,     ncon+1,0)      will be jackknifed error for g(i,0,0)
    !    g(i,     j,     0)      average over all but 'i'th and 'j'th conf.
    !                              (i,j=1,ncon), g(i,i,0)=g(i,0,0)
    !    g(i,     j,     ncon+1) will be jackknifed error for g(i,j,0)
    !    g(i,     j,     k)      av. over all but 'i'th, 'j'th, 'k'th conf.
    !                              (i,j,k=1,ncon), (i.ne.j)
    !                              g(i,j,i)=g(i,j,j)=g(i,j,0)
    !
    !******** ********* ********* ********* ********* ********* ********* **
    !
    !      implicit character*1 (a-z)
    !
    integer ::   nt,ncon
    !

    !
    real(dp) ::      gin(nt,ncon)
    real(dp) ::      g  (nt,0:ncon+2,0:ncon+1,0:ncon+1)
    !
    integer ::   it,icon,jcon,kcon
    do it=1,nt
       !
       g(it,0,0,0)=0.0d0
       !
       do icon=1,ncon
          g(it,0,0,0)=g(it,0,0,0)+gin(it,icon)
       end do
       !
       do icon=1,ncon
          g(it,icon,0,0)=g(it,0,0,0)-gin(it,icon)
          do jcon=1,ncon
             g(it,icon,jcon,0)=g(it,icon,0,0)-gin(it,jcon)
             do kcon=1,ncon
                g(it,icon,jcon,kcon)=(g(it,icon,jcon,0)-gin(it,kcon))/(ncon-3)
             end do
          end do
       end do
       !
       !  handling of normalization for first two levels of Jackknife
       !
       do icon=1,ncon
          !
          do jcon=1,ncon
             g(it,icon,jcon,0)=g(it,icon,jcon,0)/(ncon-2)
          end do
          !
          g(it,icon,0,0)=g(it,icon,0,0)/(ncon-1)
          !
       end do
       !
       g(it,0,0,0)=g(it,0,0,0)/ncon
       !
    end do
    !
  end SUBROUTINE complement

  subroutine GetEffMass(g,ncon,nt)
    integer ::    nt,ncon

    real(dp) ::       g(nt,0:ncon+2,0:ncon+1,0:ncon+1)

    !
    !  Create Effective Masses if necessary
    !
    CALL effectiveMass(g,ncon,nt)

    ! *** Why nt-1??? ***
    CALL jack1(g(:,:,0,0),1,nt,1,nt-1,ncon)
    CALL jack2(g(:,:,:,0),1,nt,1,nt-1,ncon)
    CALL jack3(g(:,:,:,:),1,nt,1,nt-1,ncon)

  end subroutine GetEffMass

  SUBROUTINE fitcf(g,m,ncon,itb,ite,nt)
    real(dp) ::       g(nt,0:ncon+2,0:ncon+1,0:ncon+1)
    real(dp) ::       m(1,0:ncon+2,0:ncon+1)
    integer, intent(in) ::    ncon,itb,ite,nt

    integer ::                icon,jcon

    do icon=1,ncon
      do jcon=1,ncon
        if (icon .ne. jcon) then

          CALL constant(g(:,icon,jcon,0),g(:,icon,jcon,ncon+1),itb,ite,m(1,icon,jcon),nt)

        end if
      end do

      CALL constant(g(:,icon,0,0),g(:,icon,ncon+1,0),itb,ite,m(1,icon,0),nt)

      m(1,icon,icon)=m(1,icon,0)
    end do

    CALL constant(g(:,0,0,0),g(:,ncon+1,0,0),itb,ite,m(1,0,0),nt)




    CALL jack2(m(1,0:ncon+2,0:ncon+1),1,1,1,1,ncon)
    CALL jack1(m(1,0:ncon+2,0),1,1,1,1,ncon)

  end subroutine fitcf

  SUBROUTINE effectiveMass(g,ncon,nt)
    !
    !  Converts a correlation function complement set to an effective mass
    !
    integer ::   nt, ncon

    !
    real(dp) ::      g(nt,0:ncon+2,0:ncon+1,0:ncon+1)
    real(dp) ::      em
    integer ::   it, icon, jcon, kcon
    !

    do kcon = 1, ncon
       do jcon = 1, ncon
          do icon = 1, ncon
             do it = 1, nt-1


                g(it,icon,jcon,kcon) = &
                &            sign(1.0d0, g(it,icon,jcon,kcon)) * &
                &            ( log(abs(g(it,icon,jcon,kcon))) -  &
                &              log(abs(g(it+1,icon,jcon,kcon))) )


             end do
          end do
       end do
    end do
    !


    do jcon = 1, ncon
       do icon = 1, ncon
          do it = 1, nt-1
             g(it,icon,jcon,0) = &
             &            sign(1.0d0, g(it,icon,jcon,0)) * &
             &            ( log(abs(g(it,icon,jcon,0))) -  &
             &              log(abs(g(it+1,icon,jcon,0))) )

          end do
       end do
    end do
    !
    do icon = 1, ncon
       do it = 1, nt-1
          g(it,icon,0,0) = &
          &            sign(1.0d0, g(it,icon,0,0)) * &
          &            ( log(abs(g(it,icon,0,0))) -  &
          &              log(abs(g(it+1,icon,0,0))) )

       end do
    end do
    !
    do it = 1, nt-1
       g(it,0,0,0) = &
       &            sign(1.0d0, g(it,0,0,0)) * &
       &            ( log(abs(g(it,0,0,0))) - &
       &              log(abs(g(it+1,0,0,0))) )
    end do

  end SUBROUTINE effectiveMass

  SUBROUTINE jack1(g,nlb,nle,lb,le,ncon)
    !
    ! computes a first-order jackknife
    !
    !implicit character*1 (a-z)
    !
    integer ::    ncon
    !

    !
    integer ::    nlb,nle,lb,le,l,icon
    !
    real(dp) ::       g(nlb:nle,0:ncon+2), gavg
    !
    !*VDIR: prefer scalar
    !

    do l=lb,le
       g(l,ncon+1)=0.0d0
       gavg=0.0d0
       do icon=1,ncon
          gavg=gavg+g(l,icon)
       end do
       gavg=gavg/ncon
       do icon=1,ncon
          g(l,ncon+1)=g(l,ncon+1)+(g(l,icon)-gavg)**2
       end do
       g(l,ncon+1)=sqrt(g(l,ncon+1)*(ncon-1)/ncon)
       g(l,ncon+2)=gavg - g(l,0)
    end do
  end SUBROUTINE jack1

  SUBROUTINE jack2(g,nlb,nle,lb,le,ncon)
    !
    ! computes a second-order jackknife
    !
    !implicit character*1 (a-z)
    !
    integer ::    ncon
    !

    !
    integer ::    nlb,nle,lb,le,l,icon,jcon
    !
    real(dp) ::       g(nlb:nle,0:ncon+2,0:ncon+1), gavg
    !
    !*VDIR: prefer scalar
    !
    do l=lb,le
       do icon=1,ncon
          g(l,icon,ncon+1)=0.0d0
          gavg=0.0d0
          do jcon=1,ncon
             if (jcon.ne.icon) then
                gavg=gavg+g(l,icon,jcon)
             end if
          end do
          gavg=gavg/(ncon-1)
          do jcon=1,ncon
             if (jcon.ne.icon) then
                g(l,icon,ncon+1)=g(l,icon,ncon+1) + (g(l,icon,jcon)-gavg)**2
             end if
          end do
          !
          g(l,icon,ncon+1)=sqrt(g(l,icon,ncon+1)*(ncon-2)/(ncon-1))
          !
       end do
    end do
  end SUBROUTINE jack2

  SUBROUTINE jack3(g,nlb,nle,lb,le,ncon)
    !
    ! computes a third-order jackknife
    !
    !implicit character*1 (a-z)
    !
    integer ::    ncon

    !

    !
    integer ::    nlb,nle,lb,le,l,icon,jcon,kcon
    !
    real(dp) ::       g(nlb:nle,0:ncon+2,0:ncon+1,0:ncon+1), gavg
    !
    !*VDIR: prefer scalar
    !
    do l=lb,le
       do icon=1,ncon
          do jcon=1,ncon
             if (jcon.ne.icon) then
                g(l,icon,jcon,ncon+1)=0.0d0
                gavg=0.0d0
                do kcon=1,ncon
                   if (kcon.ne.icon.and.kcon.ne.jcon) then
                      gavg=gavg+g(l,icon,jcon,kcon)
                   end if
                end do
                gavg=gavg/(ncon-2)
                do kcon=1,ncon
                   if (kcon.ne.icon.and.kcon.ne.jcon) then
                      g(l,icon,jcon,ncon+1)=g(l,icon,jcon,ncon+1) + (g(l,icon,jcon,kcon)-gavg)**2
                   end if
                end do
                g(l,icon,jcon,ncon+1) = sqrt(g(l,icon,jcon,ncon+1)*(ncon-3)/(ncon-2))
             end if
          end do
       end do
    end do
  end SUBROUTINE jack3

  SUBROUTINE constant(g,gerr,itb,ite,const,nt)
    !
    ! Least-squares fit of g to a constant
    !
    !  Solution by use of the Normal Equations.
    !
    !    y_i=a_1
    !
    !    y_i=g_i
    !    x_i=it
    !
    !    R_1=1
    !
    !  See Numerical Receipes Pages 509 to 511 for reference.
    !      Note: r==alpha ; b == beta.
    !
    !******** ********* ********* ********* ********* ********* ********* **
    !
    !implicit character*1 (a-z)
    !
    integer ::    nt
    !

    !
    real(dp) ::       g(nt),gerr(nt),const
    real(dp) ::       r,b,varinv,x,y
    !
    integer ::    it,itb,ite
    !
    r=0.0d0
    b=0.0d0
    !
    do it=itb,ite
       varinv=(1.0d0/gerr(it))**2
       x=it
       y=g(it)
       r=r+varinv
       b=b+y*varinv
    end do
    !
    ! solution is y=a=b * r^-1
    !
    const=b/r

  end SUBROUTINE constant

end module Legacy
