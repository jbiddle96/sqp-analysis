      module covariance

      private

      public :: covarconst,COVARMASS,output_header,output_data

c     The global parameters such as lattice spacing, ncon etc are included here
c     using the parameters module

c     These parameters where declared by a whole bunch of the covariance subroutines
c     But are easier to keep track of is declared here


      integer,parameter :: maxparam = 5
      integer :: nt,ncon

      contains

      subroutine output_header(token,header,file_unit_number,ika,ikappa,ist,istart,ish,ishift)
      character(len=*),intent(in)          :: header,token
      integer,intent(in)                   :: file_unit_number
      character(len=*),optional,intent(in) :: ika,ist,ish
      integer,optional,intent(in)          :: istart,ishift,ikappa

      write(file_unit_number,*) trim(token)
      if( present(ika) .and. present(ikappa) ) write(file_unit_number,*) trim(ika),ikappa
      if( present(ist) .and. present(istart) ) write(file_unit_number,*) trim(ist),istart
      if( present(ish) .and. present(ishift) ) write(file_unit_number,*) trim(ish),ishift
      write(file_unit_number,*) trim(header)
      return
      end subroutine

      subroutine output_data(m,itb,ite,file_unit_number,chisq,ndf,GdFt)
      double precision,dimension(0:),intent(in) :: m
      integer,intent(in)                        :: itb,ite,file_unit_number
      double precision,intent(in),optional      :: chisq,GdFt
      integer,intent(in),optional               :: ndf
      integer :: ncon

      ncon = size(m) - 3
      if( present(chisq) .and. present(Ndf) .and. present(GdFt) ) then
         write(file_unit_number,'(2i3,4f12.6,i3,2f12.6)') itb,ite,m(0),m(ncon+1),m(ncon+2),ChiSq,Ndf,ChiSq/max(Ndf,1),GdFt
      else
         write(file_unit_number,'(2i3,3f12.6)') itb,ite,m(0),m(ncon+1),m(ncon+2)
      endif

      return
      end subroutine

      SUBROUTINE COVARCONST(g,itb,ite,const,chisq,ndegfree,goodfit)
c
c----------------- --------- --------- --------- --------- --------- --
c
c     Covariance fit to a constant in time.
c     "const" is the fitted constant.
c     "chisq" is the chi-square.
c     "ndegfree" is the number of degrees of freedom.
c     "goodfit" is the probability of a higher chi-square,
c     and should not be too small.
c
c----------------- --------- --------- --------- --------- --------- --
c
c     Altered: -
c     Input:   g(nt,0:ncon+2), itb, ite
c     Output:  const, chisq, ndegfree, goodfit
c     Scratch: xind(maxindep),y(maxindep,ncon),
c     yavg(nt), yens(nt),
c     ycov(nt,nt),
c     a(maxparam), va(maxparam,maxparam)
c
c----------------- --------- --------- --------- --------- --------- --
c
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      IMPLICIT INTEGER (I-N)


c
c     I change the bounds of g
      DOUBLE PRECISION,DIMENSION(:,0:),intent(in) :: g
      DOUBLE PRECISION                            :: deg2,chisq2,const,chisq,goodfit
c
      DOUBLE PRECISION,DIMENSION(:),allocatable     :: xind,yavg,yens
      DOUBLE PRECISION,DIMENSION(:,:),allocatable   :: y,ycov
      DOUBLE PRECISION,DIMENSION(maxparam,maxparam) :: va
      DOUBLE PRECISION,DIMENSION(maxparam)          :: a

      nt = size(g,dim=1)
      ncon = size(g,dim=2) - 3
      allocate(xind(nt))
      allocate(yavg(nt))
      allocate(yens(nt))
      allocate(y(nt,ncon))
      allocate(ycov(nt,nt))

c
c----------------- --------- --------- --------- --------- --------- --
c
      numdata=ncon
      numindep=ite-itb+1
c
c     Put the independent variable (time slice) in "xind".
c     Put the dependent variable ("g") in "y".
c
      DO indep=1, numindep
         xind(indep)=indep+itb-1
         DO idata=1, numdata
            y(indep,idata)=g(indep+itb-1,idata)
         END DO
c
c     The ensemble average.
c
         yens(indep)=g(indep+itb-1,0)
      END DO
c
c     Compute the jackknife estimate of the data covariance matrix.
c
      CALL JACOVAR(y,numdata,ncon,numindep,nt,.true.,
     &     yavg,ycov)
c
c     Linear least squares fit to a constant.
c     Condition number.
c
      conmaxinv=1.0e-06
c
c     A constant is a polynomial with one parameter.
c
      numparam=1
c
c     Do not output the matrix decomposition.
c
      iounit=0
c
c     "a" is the fitted constant.
c     "va" is the covariance matrix for "a".
c
      CALL LLSQFIT(xind,yens,ycov,
     &     conmaxinv,numindep,numparam,iounit,
     &     a,va,cmconinv,dmconinv,chisqdp,ndegfree)
c
      chisq=chisqdp
      const=a(1)
c
c     Evaluate goodness of fit (probability that a value at least as large
c     as "chisq" will be obtained by chance if the model is correct,
c     and if the input data is normally distributed.
c
      deg2=ndegfree/2.
      chisq2=chisq/2.
      goodfit=GAMMQ(deg2,chisq2)


      RETURN
      END subroutine
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c

      SUBROUTINE JACOVAR(x,numdata,ncon,numvar,maxvar,jac, xavg,xcov)
c
c----------------- --------- --------- --------- --------- --------- --
c
c     Calculates the means and covariances of means of an array of data.
c     If the logical "jac" equals .false.,
c     the covariance matrix corresponds to the square of the naive
c     "standard error of the mean"; if .true. then
c     it corresponds to its jackknife estimate.
c     (The two differ by a factor of (numdata-1)**2).
c
c----------------- --------- --------- --------- --------- --------- --
c
c     Altered: -
c     Input:   x(maxvar,ncon), numdata, ncon, numvar, maxvar, jac
c     Output:  xavg(maxvar), xcov(maxvar,maxvar)
c     Scratch: -
c
c----------------- --------- --------- --------- --------- --------- --
c
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      IMPLICIT INTEGER (I-N)
c
      DOUBLE PRECISION
     &     accum,
     &     x(maxvar,ncon),
     &     xavg(maxvar), xcov(maxvar,maxvar)
c
      LOGICAL
     &     jac
c
c----------------- --------- --------- --------- --------- --------- --
c
c     for a given time slice calculate the average of jackknife values
c
      DO i=1, numvar
         accum=0.0d0
         DO k=1, numdata
            accum=accum + x(i,k)
         END DO
         xavg(i)=accum / dble(numdata)
      END DO
c
c     calculate the covariance matrix xcov(t,t')
c
      DO i=1, numvar
         DO j=1, numvar
            accum=0.0d0
            DO k=1, numdata
               accum=accum + x(i,k) * x(j,k)
            END DO
            IF (jac) THEN
               xcov(i,j)=( accum / dble(numdata) -
     &              xavg(i) * xavg(j) )
     &              * ( numdata - 1 )
            ELSE
               xcov(i,j)=( accum / dble(numdata) -
     &              xavg(i) * xavg(j) )
     &              / ( numdata - 1 )
            ENDIF
         END DO
      END DO
c
      RETURN
      END SUBROUTINE
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      FUNCTION GAMMQ(A,X)
      implicit double precision (a-h,o-z)
      implicit integer (i-n)
c
c From "Numerical Recipes" FORTRAN diskette (1985)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Returns the complement Q(A,X)=1 - P(A,X)
c   of the incomplete gamma function P(A,X).
c $Q(\chi^{2}|\nu)=Q(\nu/2,\chi^{2}/2)$ is the probability
c   that the observed chi-square will exceed the value $\chi^{2}$
c   by chance even for a correct model.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Subroutines called:
c   GSER            ; returns the incomplete gamma function P
c   GCF             ; returns the incomplete gamma function Q
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      IF(X.LT.0..OR.A.LE.0.)write(*,*)'PAUSE in gammq'
      IF(X.LT.A+1.)THEN
        CALL GSER(GAMSER,A,X,GLN)
        GAMMQ=1.-GAMSER
      ELSE
        CALL GCF(GAMMCF,A,X,GLN)
        GAMMQ=GAMMCF
      ENDIF
      RETURN
      END FUNCTION
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      SUBROUTINE GCF(GAMMCF,A,X,GLN)
      implicit double precision (a-h,o-z)
      implicit integer (i-n)
c
c From "Numerical Recipes" FORTRAN diskette (1985)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Returns the incomplete Gamma function,
c   evaluated by its continued fraction representation, as "gammcf"
c   Also returns $ln \Gamma(a)$ as "gln".
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Functions called:
c   GAMMLN        ; returns the log of the Gamma function
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      double precision x
      PARAMETER (ITMAX=100,EPS=3.E-7)
      GLN=GAMMLN(A)
      GOLD=0.
      A0=1.
      A1=X
      B0=0.
      B1=1.
      FAC=1.
      DO 11 N=1,ITMAX
        AN=FLOAT(N)
        ANA=AN-A
        A0=(A1+A0*ANA)*FAC
        B0=(B1+B0*ANA)*FAC
        ANF=AN*FAC
        A1=X*A0+ANF*A1
        B1=X*B0+ANF*B1
        IF(A1.NE.0.)THEN
          FAC=1./A1
          G=B1*FAC
          IF(ABS((G-GOLD)/G).LT.EPS)GO TO 1
          GOLD=G
        ENDIF
11    CONTINUE
      write(*,*) 'A too large, ITMAX too small'
1     GAMMCF=EXP(-X+A*LOG(X)-GLN)*G
      RETURN
      END SUBROUTINE
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      SUBROUTINE GSER(GAMSER,A,X,GLN)
      implicit double precision (a-h,o-z)
      implicit integer (i-n)
c
c From "Numerical Recipes" FORTRAN diskette (1985)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Returns the incomplete Gamma function P(a,x)
c   evaluated by its series representation, as "gamser"
c   Also returns $ln \Gamma(a)$ as "gln".
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Functions called:
c   GAMMLN        ; returns the log of the Gamma function
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      PARAMETER (ITMAX=100,EPS=3.E-7)
      GLN=GAMMLN(A)
      IF(X.LE.0.)THEN
        IF(X.LT.0.)write(*,*)'PAUSE in gser'
        GAMSER=0.
        RETURN
      ENDIF
      AP=A
      SUM=1./A
      DEL=SUM
      DO 11 N=1,ITMAX
        AP=AP+1.
        DEL=DEL*X/AP
        SUM=SUM+DEL
        IF(ABS(DEL).LT.ABS(SUM)*EPS)GO TO 1
11    CONTINUE
      write(*,*) 'A too large, ITMAX too small'
1     GAMSER=SUM*EXP(-X+A*LOG(X)-GLN)
      RETURN
      END SUBROUTINE
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      FUNCTION GAMMLN(XX)
      implicit double precision (a-h,o-z)
      implicit integer (i-n)
c
c From "Numerical Recipes" FORTRAN diskette (1985)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Returns the log of the Gamma function.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      REAL*8 COF(6),STP,HALF,ONE,FPF,X,TMP,SER
      DATA COF,STP/76.18009173D0,-86.50532033D0,24.01409822D0,
     *    -1.231739516D0,.120858003D-2,-.536382D-5,2.50662827465D0/
      DATA HALF,ONE,FPF/0.5D0,1.0D0,5.5D0/
      X=XX-ONE
      TMP=X+FPF
      TMP=(X+HALF)*LOG(TMP)-TMP
      SER=ONE
      DO 11 J=1,6
        X=X+ONE
        SER=SER+COF(J)/X
11    CONTINUE
      GAMMLN=TMP+LOG(STP*SER)
      RETURN
      END FUNCTION
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      SUBROUTINE FUNCPOLY(x,np, p)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Generates values of "np" basis functions "p(i)=x**(i-1)"
c   evaluated at "x"
c   for a polynomial fit $y=\sum_{i}^{np} a_{i} x^{i-1}$.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Altered: -
c Input:   x, np
c Output:  p(np)
c Scratch: -
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      implicit integer (i-n)
      IMPLICIT CHARACTER*1 (A-H,O-Z)
c
      DOUBLE PRECISION
     &  p(np), x
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      p(1)=1.0d0
      IF (np .EQ. 1) RETURN
      DO j=2, np
        p(j)=p(j-1) * x
      END DO
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      RETURN
      END SUBROUTINE
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      SUBROUTINE LLSQFIT(x,y,vy,conmaxinv,numindep,numparam,iounit,
     &                   a,va,cmconinv,dmconinv,chisq,ndegfree)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Linear least squares fit.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Altered: -
c Input:   "func", x(nt),
c          y(nt), vy(nt,nt),
c          conmaxinv, numindep, numparam, iounit
c Output:  a(maxparam), va(maxparam,maxparam),
c          cmconinv, dmconinv, chisq, ndegfree
c Scratch: -
c Altered: 29-5-03 By Ben
c          I removed func as a parameter to LLSQFIT because it is obselete
c          inside the module
c-------- --------- --------- --------- --------- --------- --------- --
c
c "y" is the (length "numindep") vector of sample means.
c "vy" is the covariance matrix $V_{y}$ for $y$.
c   $V_{ij}=<(y_i-<y_i>)(y_j-<y_j>)>$.
c "a" is the (length "numparam") vector of optimal fit parameters.
c "va" is the covariance matrix $V_{a}$ for $a$.
c A least squares best fit is made to the form
c   $y(x)=\sum_{ip=1}^{numparam} f_{ip}(x) a_{ip}$ (or $y=F a$).
c "ft(ip,id)" is $F(id,ip)=f_{ip}(x_{id})$.
c  here x_{id} are time slices numbered id
c       f_{ip} are polynomial functions
c "func" is the name of the subroutine which returns ft(ip,id),
c   for fixed id.
c "chisq" is $\chi^{2}$
c
c The normal equation to be solved for $a$ is $M a=b$
c   where $M=F^{T} V_{y}^{-1} F$ and $b=F^{T} V_{y}^{-1} y$.
c First $V_{y}$ is factored as $U_{V_{y}} w_{V_{y}} U_{V_{y}}^{T}$
c   so its inverse is $U_{V_{y}} w_{V_{y}}^{-1} U_{V_{y}}^{T}$.
c Then $M$ and $b$ are constructed.
c Then $M$ is decomposed as $U_{M} w_{M} U_{M}^{T}$
c   so its inverse is $M^{-1}=U_{M} w_{M}^{-1} U_{M}^{T}$.
c This inverse is $V_{a}=M^{-1}$, the covariance matrix of the
c   fitted parameters $a$.
c Then $a=V_{a} b$.
c Once the solution $a$ is determined, compute the chi-square $\chi^{2}$
c
c Minimum permissible inverse condition number "conmaxinv"
c   (set to, say, 1.0e-06).
c "cmnuminv" is the inverse condition no. of the data covariance matrix.
c "dmnuminv" is the inverse condition no. of the design matrix (squared)
c
c "iounit" will be passed to subroutine SVDINV.
c   Set it to 0 if no output is desired, to the unit number otherwise.
c
c The estimated variance of an interpolated or extrapolated value
c   is $\sigma^{2}(y(x))=( F V_{a} F^{T} )_{xx}$
c
c ***IF*** the input data is normally distributed, then
c   a) $\chi^{2}$ is distributed as a chi-square distribution with
c      "ndegfree" degrees of freedom, and can be used to determine
c      the goodness of fit.
c   b) A single element $a_{i}$ will be distributed normally,
c      with an "error" $\delta a_{i}$
c      $=\pm \sqrt{ \Delta \chi^{2}_{1} } \sqrt { (V_{a})_{ii} }$
c      that is, the standard (one-sigma, 68.3%) error is
c      $\delta a_{i}=\pm \sqrt { (V_{a})_{ii} }$
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Subroutines called:
c   SVDINV          ; Singular value decomposition, returning inverse.
c   "FUNC"          ; Returns basis functions.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      IMPLICIT INTEGER (I-N)
c
c Set these parameters as desired.
c


c
      DOUBLE PRECISION
     &  ft(maxparam,nt), x(nt),
     &  y(nt), vy(nt,nt),
     &  a(maxparam),
     &  b(maxparam), s(maxparam,nt), d(nt),
     &  m(maxparam,maxparam),
     &  va(maxparam,maxparam),
     &  accum, um(maxparam,maxparam), wim(maxparam),
     &  uvy(nt,nt), wivy(nt),
     &  scra1(nt,nt), scra2(maxparam,maxparam)
c

c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Singular value decomposition of the covariance matrix as
c   $V_{y}=U_{V_{y}} w_{V_{y}} U_{V_{y}}^{T}$
c   so $V_{y}^{-1}=U_{V_{y}} w_{V_{y}}^{-1} U_{V_{y}}^{T}$.
c
      numindepnew=numindep
      IF (iounit .NE. 0 )
     &  WRITE(iounit,*) ' Input data covariance matrix:'
      CALL SVDINV(vy,conmaxinv,numindep,nt,iounit,
     &            uvy,wivy,cmconinv,numzero, scra1)
c
c If there is any degeneracy,
c   the effective number of points is reduced.
c
      numindepnew=numindepnew - numzero
c
c Compute the (transpose of the) fitting matrix $F$.
c   $ft(ip,id)=f_{ip}(x_{id})$, for fixed $x_{id}$
c
      DO id=1, numindep
        CALL FUNCPOLY(x(id),numparam, ft(1,id))
      END DO
c
c Define $S=F^{T} U_{V_{y}} w_{V_{y}}^{-1} U_{V_{y}}^{T}$
c
      DO ip=1, numparam
      DO id=1, numindep
        accum=0.0d0
        DO jd=1, numindep
        DO kd=1, numindep
          accum=accum
     &            + ft(ip,jd) * uvy(jd,kd) * wivy(kd)   * uvy(id,kd)
        END DO
        END DO
        s(ip,id)=accum
      END DO
      END DO
c
c $b=S y$ and $M=S F$
c
      DO ip=1, numparam
        accum=0.0d0
        DO id=1, numindep
          accum=accum + s(ip,id) * y(id)
        END DO
        b(ip)=accum
        DO jp=1, numparam
          accum=0.0d0
          DO id=1, numindep
            accum=accum + s(jp,id) * ft(ip,id)
          END DO
          m(jp,ip)=accum
        END DO
      END DO
c
c $M=U_{M} w_{M} U_{M}^{T}$ so $M^{-1}=U_{M} w_{M}^{-1} U_{M}^{T}$
c
      numparamnew=numparam
      IF (iounit .NE. 0 )
     &  WRITE(iounit,*) ' (Square of) design matrix:'
      CALL SVDINV(m,conmaxinv,numparam,maxparam,iounit,
     &            um,wim,dmconinv,numzero, scra2)
c
c If there is any degeneracy,
c  the effective number of parameters is reduced.
c
      numparamnew=numparamnew - numzero
c
c Effective number of degrees of freedom.
c
      ndegfree=numindepnew - numparamnew
c
c Compute the inverse $M^{-1}=U_{M} * w_{M}^{-1} * U_{M}^{T}$
c This equals the covariance matrix $V_{a}$ of the fitted parameters $a$
c
      DO ip=1, numparam
      DO jp=1, numparam
        accum=0.0d0
        DO kp=1, numparam
          accum=accum + um(ip,kp) * wim(kp) * um(jp,kp)
        END DO
        va(ip,jp)=accum
      END DO
      END DO
c
c Compute the solution vector of parameters $a$.
c $M a=b$ so $a=M^{-1} b=V_{a} b$
c
      DO ip=1, numparam
        accum=0.0d0
        DO jp=1, numparam
          accum=accum + va(ip,jp) * b(jp)
        END DO
        a(ip)=accum
      END DO
c
c Now that we've got the solution vector "a", compute the chi-square.
c
c First, compute the difference $y - F a$.
c
      DO id=1, numindep
        accum=y(id)
        DO ip=1, numparam
          accum=accum - ft(ip,id) * a(ip)
        END DO
        d(id)=accum
      END DO
c
c Then compute $(y - F a)^{T} V_{y}^{-1} (y - F a)$
c  =$d^{T} U_{V_{y}} w_{V_{y}}^{-1} U_{V_{y}}^{T}
c   using the already computed SVD of $V_{y}$.
c
      accum=0.0d0
      DO id=1, numindep
      DO jd=1, numindep
      DO kd=1, numindep
        accum=accum
     &        + d(id) * uvy(id,jd) * wivy(jd) * uvy(kd,jd) * d(kd)
      END DO
      END DO
      END DO
      chisq=accum
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      RETURN
      END SUBROUTINE
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      SUBROUTINE SVDINV(a,conmaxinv,n,np,iounit,
     &                  u,w,connuminv,numzero, t)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c SVDINV will factor the n x n (stored as np x np) symmetric matrix "A"
c   as $A=UwU^{T}, with $w$ diagonal.
c It will then compute $w^{-1}$, setting to zero
c   any values which are less than the threshold "conmaxinv*MAX(w)".
c The "w" returned is actually the inverse.
c "iounit" will be passed to subroutine SVDDISP.
c   Set it to 0 if no output is desired, to the unit number otherwise.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Altered: -
c Input:   a(np,np), conmaxinv, n, np, iounit
c Output:  u(np,np), w(np), connuminv, numzero
c Scratch: t(np,np)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Subroutines called:
c   SVDDISP         ; Singular value decomposition.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      IMPLICIT INTEGER (I-N)
c
      DOUBLE PRECISION
     &  t(np,np),
     &  u(np,np), w(np),
     &  a(np,np)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Save the original matrix A.
c
      DO i=1, n
      DO j=1, n
        t(i,j)=a(i,j)
      END DO
      END DO
c
c T is changed.
c
      CALL SVDDISP(a,n,np,iounit, u,w, t)
c
      numzero=0
      wmax=0.
      DO i=1, n
        IF (w(i) .GT. wmax) wmax=w(i)
      END DO
      wmin=wmax
      DO i=1, n
        IF (w(i) .LT. wmin) wmin=w(i)
      END DO
      connuminv=wmin / wmax
      wmin=wmax * conmaxinv
      DO i=1, n
        IF (w(i) .LT. wmin) THEN
c
c This is actually the inverse.
c
          w(i)=0.
          numzero=numzero + 1
        ELSE
c
c This is actually the inverse.
c
          w(i)=1. / w(i)
        END IF
      END DO
c
      RETURN
      END SUBROUTINE
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      SUBROUTINE SVDDISP(a,n,np,iounit, u,w, t)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c SVD will factor the n x n (stored as np x np) symmetric matrix "A"
c    as A=UwU^{T}.  The decomposition and some checks will be output.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Altered: -
c Input:   a(np,np), n, np, iounit
c Output:  u(np,np), w(np),
c Scratch: t(np,np)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Subroutines called:
c   SVDCMP          ; Singular value decomposition.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      IMPLICIT INTEGER (I-N)
c
      DOUBLE PRECISION
     &  t(np,np), accum,
     &  u(np,np), w(np),
     &  a(np,np)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Save the original matrix A.
c
      DO i=1, n
      DO j=1, n
        t(i,j)=a(i,j)
      END DO
      END DO
c
c SVDCMP changes T.
c
      CALL SVDCMP(t, n,n,np,np, w,u)
c
      IF (iounit .EQ. 0 ) RETURN
c
c      WRITE(iounit,9002) ((a(i,j),j=1,n),i=1,n),((u(i,j),j=1,n),i=1,n),
c     .                   (w(i),i=1,n),(SQRT(w(i)),i=1,n)
c 9002 FORMAT(/,' Singular value decomposition',
c     .       /,' of matrix'         ,/,<n>(<n>(e14.7,2x),/),
c     .       /,' as UwU^{T} with U=',/,<n>(<n>(e14.7,2x),/),
c     .       /,' and w=diag()'      ,/,<n>(e14.7,2x),/,
c     .       /,' with SQRT(w)='     ,/,<n>(e14.7,2x),/)
c
c Verify that A=UwU^{T}
c
      DO i=1,n
      DO j=1,n
        accum=0.0d0
        DO l=1,n
          accum=accum + u(i,l) * w(l) * u(j,l)
        END DO
        t(i,j)=accum
      END DO
      END DO
c
c      WRITE(iounit,9003) ((t(i,j),j=1,n),i=1,n)
c 9003 FORMAT(' Check: UwU^{T}=',/,<n>(<n>(e14.7,2x),/))
c
c Verify that U is orthogonal.
c
      DO i=1,n
      DO j=1,n
        accum=0.0d0
        DO l=1,n
          accum=accum + u(i,l) * u(j,l)
        END DO
        t(i,j)=accum
      END DO
      END DO
c
c      WRITE(iounit,9004) ((t(i,j),j=1,n),i=1,n)
c 9004 FORMAT(' Check: UU^{T}=',/,<n>(<n>(e14.7,2x),/))
c
      RETURN
      END SUBROUTINE
c
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c******** ********* ********* ********* ********* ********* ********* **
c
      SUBROUTINE SVDCMP(A,M,N,MP,NP,W,V)
c
c From "Numerical Recipes" FORTRAN diskette (1985)
c
c-------- --------- --------- --------- --------- --------- --------- --
c
c Given a Matrix "A",
c   with logical dimensions "M" by "N"
c   and physical dimensions "MP" by "NP",
c   this routine computes its singular value decomposition, "A=UWV^{T}".
c The matrix "U" replaces "A" on output.
c The diagonal matrix of singular values "W" is output as a vector "W".
c The matrix "V" (not its transpose) is output as "V".
c "M" must be greater than or equal to "N"; if it is smaller,
c   then "A" should be filled up to square with zero rows.
c
c-------- --------- --------- --------- --------- --------- --------- --
c
      IMPLICIT CHARACTER*1 (A-H,O-Z)
      implicit integer (i-n)
      PARAMETER (NMAX=100)
      DOUBLE PRECISION
     &  W(NP),V(NP,NP),S,SCALE,A(MP,NP),RV1(NMAX),
     &  G,ANORM,F,H,C,Y,Z,X
c
      G=0.0D0
      SCALE=0.0D0
      ANORM=0.0D0
      DO 25 I=1,N
        L=I+1
        RV1(I)=SCALE*G
        G=0.0D0
        S=0.0D0
        SCALE=0.0D0
        IF (I.LE.M) THEN
          DO 11 K=I,M
            SCALE=SCALE+ABS(A(K,I))
11        CONTINUE
          IF (SCALE.NE.0.0D0) THEN
            DO 12 K=I,M
              A(K,I)=A(K,I)/SCALE
              S=S+A(K,I)*A(K,I)
12          CONTINUE
            F=A(I,I)
            G=SIGN(SQRT(S),F)*(-1.0d0)
            H=F*G-S
            A(I,I)=F-G
            IF (I.NE.N) THEN
              DO 15 J=L,N
                S=0.0D0
                DO 13 K=I,M
                  S=S+A(K,I)*A(K,J)
13              CONTINUE
                F=S/H
                DO 14 K=I,M
                  A(K,J)=A(K,J)+F*A(K,I)
14              CONTINUE
15            CONTINUE
            ENDIF
            DO 16 K=I,M
              A(K,I)=SCALE*A(K,I)
16          CONTINUE
          ENDIF
        ENDIF
        W(I)=SCALE *G
        G=0.0D0
        S=0.0D0
        SCALE=0.0D0
        IF ((I.LE.M).AND.(I.NE.N)) THEN
          DO 17 K=L,N
            SCALE=SCALE+ABS(A(I,K))
17        CONTINUE
          IF (SCALE.NE.0.0D0) THEN
            DO 18 K=L,N
              A(I,K)=A(I,K)/SCALE
              S=S+A(I,K)*A(I,K)
18          CONTINUE
            F=A(I,L)
            G=SIGN(SQRT(S),F)*(-1.0d0)
            H=F*G-S
            A(I,L)=F-G
            DO 19 K=L,N
              RV1(K)=A(I,K)/H
19          CONTINUE
            IF (I.NE.M) THEN
              DO 23 J=L,M
                S=0.0D0
                DO 21 K=L,N
                  S=S+A(J,K)*A(I,K)
21              CONTINUE
                DO 22 K=L,N
                  A(J,K)=A(J,K)+S*RV1(K)
22              CONTINUE
23            CONTINUE
            ENDIF
            DO 24 K=L,N
              A(I,K)=SCALE*A(I,K)
24          CONTINUE
          ENDIF
        ENDIF
        ANORM=MAX(ANORM,(ABS(W(I))+ABS(RV1(I))))
25    CONTINUE
      DO 32 I=N,1,-1
        IF (I.LT.N) THEN
          IF (G.NE.0.0D0) THEN
            DO 26 J=L,N
              V(J,I)=(A(I,J)/A(I,L))/G
26          CONTINUE
            DO 29 J=L,N
              S=0.0D0
              DO 27 K=L,N
                S=S+A(I,K)*V(K,J)
27            CONTINUE
              DO 28 K=L,N
                V(K,J)=V(K,J)+S*V(K,I)
28            CONTINUE
29          CONTINUE
          ENDIF
          DO 31 J=L,N
            V(I,J)=0.0D0
            V(J,I)=0.0D0
31        CONTINUE
        ENDIF
        V(I,I)=1.0D0
        G=RV1(I)
        L=I
32    CONTINUE
      DO 39 I=N,1,-1
        L=I+1
        G=W(I)
        IF (I.LT.N) THEN
          DO 33 J=L,N
            A(I,J)=0.0D0
33        CONTINUE
        ENDIF
        IF (G.NE.0.0D0) THEN
          G=1.0D0/G
          IF (I.NE.N) THEN
            DO 36 J=L,N
              S=0.0D0
              DO 34 K=L,M
                S=S+A(K,I)*A(K,J)
34            CONTINUE
              F=(S/A(I,I))*G
              DO 35 K=I,M
                A(K,J)=A(K,J)+F*A(K,I)
35            CONTINUE
36          CONTINUE
          ENDIF
          DO 37 J=I,M
            A(J,I)=A(J,I)*G
37        CONTINUE
        ELSE
          DO 38 J=I,M
            A(J,I)=0.0D0
38        CONTINUE
        ENDIF
        A(I,I)=A(I,I)+1.0D0
39    CONTINUE
      DO 49 K=N,1,-1
        DO 48 ITS=1,30
          DO 41 L=K,1,-1
            NM=L-1
            IF ((ABS(RV1(L))+ANORM).EQ.ANORM)  GO TO 2
            IF ((ABS(W(NM))+ANORM).EQ.ANORM)  GO TO 1
41        CONTINUE
1         C=0.0D0
          S=1.0D0
          DO 43 I=L,K
            F=S*RV1(I)
            IF ((ABS(F)+ANORM).NE.ANORM) THEN
              G=W(I)
              H=SQRT(F*F+G*G)
              W(I)=H
              H=1.0D0/H
              C=(G*H)
              S=-(F*H)
              DO 42 J=1,M
                Y=A(J,NM)
                Z=A(J,I)
                A(J,NM)=(Y*C)+(Z*S)
                A(J,I)=-(Y*S)+(Z*C)
42            CONTINUE
            ENDIF
43        CONTINUE
2         Z=W(K)
          IF (L.EQ.K) THEN
            IF (Z.LT.0.0D0) THEN
              W(K)=-Z
              DO 44 J=1,N
                V(J,K)=-V(J,K)
44            CONTINUE
            ENDIF
            GO TO 3
          ENDIF
          IF (ITS.EQ.30) write(*,*) 'No convergence in 30 iterations'
          X=W(L)
          NM=K-1
          Y=W(NM)
          G=RV1(NM)
          H=RV1(K)
          F=((Y-Z)*(Y+Z)+(G-H)*(G+H))/(2.0D0*H*Y)
          G=SQRT(F*F+1.0D0)
          F=((X-Z)*(X+Z)+H*((Y/(F+SIGN(G,F)))-H))/X
          C=1.0D0
          S=1.0D0
          DO 47 J=L,NM
            I=J+1
            G=RV1(I)
            Y=W(I)
            H=S*G
            G=C*G
            Z=SQRT(F*F+H*H)
            RV1(J)=Z
            C=F/Z
            S=H/Z
            F=(X*C)+(G*S)
            G=-(X*S)+(G*C)
            H=Y*S
            Y=Y*C
            DO 45 NM=1,N
              X=V(NM,J)
              Z=V(NM,I)
              V(NM,J)=(X*C)+(Z*S)
              V(NM,I)=-(X*S)+(Z*C)
45          CONTINUE
            Z=SQRT(F*F+H*H)
            W(J)=Z
            IF (Z.NE.0.0D0) THEN
              Z=1.0D0/Z
              C=F*Z
              S=H*Z
            ENDIF
            F=(C*G)+(S*Y)
            X=-(S*G)+(C*Y)
            DO 46 NM=1,M
              Y=A(NM,J)
              Z=A(NM,I)
              A(NM,J)=(Y*C)+(Z*S)
              A(NM,I)=-(Y*S)+(Z*C)
46          CONTINUE
47        CONTINUE
          RV1(L)=0.0D0
          RV1(K)=F
          W(K)=X
48      CONTINUE
3       CONTINUE
49    CONTINUE
      RETURN
      END SUBROUTINE

      end module covariance
