module MathsOps
  use kinds
  use IFPORT
  use, intrinsic :: iso_c_binding, only: c_size_t
  implicit none

  interface mean
     module procedure mean_SP
     module procedure mean_DP
  end interface mean

  interface median
     module procedure median_SP
     module procedure median_DP
  end interface median

  interface std
     module procedure std_SP
     module procedure std_DP
  end interface std

  interface sort
     module procedure sort_SP
     module procedure sort_DP
  end interface sort


contains

  ! Mean functions
  real(SP) function mean_SP(array)
    real(SP), dimension(:), intent(in) :: array

    mean_SP = sum(array)/real(size(array), SP)
  end function mean_SP


  real(DP) function mean_DP(array)
    real(DP), dimension(:), intent(in) :: array

    mean_DP = sum(array)/real(size(array), DP)
  end function mean_DP


  ! Standard deviation functions
  real(SP) function std_SP(array)
    real(SP), dimension(:), intent(in) :: array

    ! Local vars
    real(SP) :: mean

    mean = mean_SP(array)
    std_SP = sqrt(sum((array(:) - mean)**2)/real(size(array(:)), SP))
  end function std_SP


  real(DP) function std_DP(array)
    real(DP), dimension(:), intent(in) :: array

    ! Local vars
    real(DP) :: mean

    mean = mean_DP(array)
    std_DP = sqrt(sum((array(:) - mean)**2)/real(size(array(:)), DP))
  end function std_DP


  real(DP) function median_DP(array)
    real(DP), dimension(:), intent(in) :: array

    ! Local vars
    real(DP), dimension(size(array)) :: array_copy
    integer :: array_size, midpoint

    array_size = size(array)

    array_copy = array
    call sort(array_copy)

    if(mod(array_size, 2) == 1) then
      midpoint = (array_size / 2) + 1
      median_DP = array_copy(midpoint)
    else
      midpoint = array_size / 2
      median_DP = (array_copy(midpoint) + array_copy(midpoint + 1)) / 2.0d0
    end if

  end function median_DP

  real(SP) function median_SP(array)
    real(SP), dimension(:), intent(in) :: array

    ! Local vars
    real(SP), dimension(size(array)) :: array_copy
    integer :: array_size, midpoint

    array_size = size(array)

    array_copy = array
    call sort(array_copy)

    if(mod(array_size, 2) == 1) then
      midpoint = (array_size / 2) + 1
      median_SP = array_copy(midpoint)
    else
      midpoint = array_size / 2
      median_SP = (array_copy(midpoint) + array_copy(midpoint + 1)) / 2.0d0
    end if

  end function median_SP

  subroutine find_missing_positive(A, x)
    ! Find the first missing positive integer x in array A
    integer, dimension(:, :), value :: A
    integer, intent(out) :: x

    ! Local vars
    integer, dimension(:), allocatable :: B
    integer :: i, N

    allocate(B(size(A)))

    B = reshape(A, [size(A)])
    call segregate_array(B, N)

    do i = 1, N
      if(B(i) < N) then
        B(B(i)) = -B(B(i))
      end if
    end do

    write(*,*) "B = "
    write(*,*) B

    x = findloc((B > 0), .true., dim=1)

  end subroutine find_missing_positive

  subroutine segregate_array(A, max_idx)
    ! Function to push all non-positive numbers to the back of an array
    integer, dimension(:), intent(inout) :: A
    integer, intent(out) :: max_idx

    integer :: i, j, N, temp

    N = size(A)
    j = 1

    do i = 1, N
      if(A(i) > 0) then
        temp = A(i)
        A(i) = A(j)
        A(j) = temp
        j = j + 1
      end if
    end do

    max_idx = j - 1
  end subroutine segregate_array


  subroutine sort_DP(array, order)
    real(DP), dimension(:), intent(inout) :: array
    character(len=3), optional :: order

    ! Local vars
    integer(C_SIZE_T) :: array_len, array_size
    character(len=3) :: order_

    if(present(order)) then
      order_ = order
    else
      order_ = 'inc'
    end if

    array_len = size(array)
    array_size = DP

    if(order_ == 'inc') then
      call qsort(array, array_len, array_size, order_inc_DP)
    else if(order_ == 'dec') then
      call qsort(array, array_len, array_size, order_dec_DP)
    else
      write(*,*) "Invalid sort order, aborting"
      stop
    end if

  end subroutine sort_DP


  subroutine sort_SP(array, order)
    real(SP), dimension(:), intent(inout) :: array
    character(len=3), optional :: order

    ! Local vars
    integer(C_SIZE_T) :: array_len, array_size
    character(len=3) :: order_

    if(present(order)) then
      order_ = order
    else
      order_ = 'inc'
    end if

    array_len = size(array)
    array_size = SP

    if(order_ == 'inc') then
      call qsort(array, array_len, array_size, order_inc_SP)
    else if(order_ == 'dec') then
      call qsort(array, array_len, array_size, order_dec_SP)
    else
      write(*,*) "Invalid sort order, aborting"
      stop
    end if

  end subroutine sort_SP


  integer(2) function order_inc_DP(a1, a2)
    real(DP) :: a1,a2

    if(a1 < a2) then
      order_inc_DP = -1
    else if(a1 > a2) then
      order_inc_DP = 1
    else if(a1 == a2) then
      order_inc_DP = 0
    end if
  end function order_inc_DP

  integer(2) function order_dec_DP(a1, a2)
    real(DP) :: a1,a2

    if(a1 > a2) then
      order_dec_DP = -1
    else if(a1 < a2) then
      order_dec_DP = 1
    else if(a1 == a2) then
      order_dec_DP = 0
    end if

  end function order_dec_DP


  integer(2) function order_inc_SP(a1, a2)
    real(SP) :: a1,a2

    if(a1 < a2) then
      order_inc_SP = -1
    else if(a1 > a2) then
      order_inc_SP = 1
    else if(a1 == a2) then
      order_inc_SP = 0
    end if
  end function order_inc_SP


  integer(2) function order_dec_SP(a1, a2)
    real(SP) :: a1,a2

    if(a1 > a2) then
      order_dec_SP = -1
    else if(a1 < a2) then
      order_dec_SP = 1
    else if(a1 == a2) then
      order_dec_SP = 0
    end if

  end function order_dec_SP

end module MathsOps
