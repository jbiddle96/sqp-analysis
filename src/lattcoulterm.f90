
program LattCoulTerm

  use Kinds
  use MPIInterface
  use Strings
  use TextIO
  implicit none

  integer, parameter :: rmax = 10, rmax_off = rmax/2, rmax3 = rmax*(rmax_off+1)**2
  integer, parameter :: div_r(3) = (/ 2,5,10 /), div_off(3) = (/ 2,3,6 /) ! divisors
  real(dp), dimension(:,:,:), allocatable :: GluonProp128,GluonProp256,GluonPropExt,GluonProp,GluonBuff

  character(len=20) :: startDate, startTime, endDate, endTime
  integer :: nrx, nry,nrz
  integer :: irx,iry,irz,jrx,jry,jrz,ix,iy,iz
  integer :: mpierror, irank, mpi_status(MPI_STATUS_SIZE)
  integer :: procx, procy, procz
  integer :: isize, idiv

  real(dp) :: c_plaq, c_rect
  real(dp) :: dyondx, x256,x128
  character(len=128) :: outputfile
  character(len=11) :: formatstr(0:1) = (/ "unformatted", "formatted  " /)
  integer :: iaction, iformat

  call InitialiseMPI

  call SetProcessTopology

  call Put("This program calculates the infinite-volume extrapolated lattice Coulomb term.")
  call Put("Enter the gluon action type (0=Wilson,1=Two-Loop,2=IMP,3=Iwasaki,42=DBW2):")
  call Get(iaction)
  call Put("Enter the output filename:")
  call Get(outputfile)
  call Put("Select a format (0=unformatted,1=formatted):")
  call Get(iformat)

  if ( iaction == 0 ) then
     c_plaq = 1.0d0
     c_rect = 0.0d0
  else if ( iaction == 1 ) then
     c_plaq = 5.0d0/3.0d0
     c_rect=-1.0d0/12.0d0
  else if ( iaction == 2 ) then
     c_plaq = 5.0d0/3.0d0
     c_rect=-1.0d0/12.0d0
  else if ( iaction == 3 ) then ! Iwasaki glue.
     c_plaq =  3.648d0
     c_rect = -0.331d0
  else if ( iaction == 42 ) then ! DBW2 glue.
     c_plaq = 1.0d0
     c_rect = -1.4067d0/12.2536d0
  end if

  call GetSubspace(mpi_rank,irx,jrx,iry,jry,irz,jrz)

  allocate(GluonProp128(irx:jrx,iry:jry,irz:jrz))
  allocate(GluonProp256(irx:jrx,iry:jry,irz:jrz))
  allocate(GluonPropExt(irx:jrx,iry:jry,irz:jrz))

  call date_and_time(date=startDate, time=startTime)
  !call Put("Starting time: "//trim(startTime))

  call Coulomb(128,c_plaq,c_rect,GluonProp128)
  call Coulomb(256,c_plaq,c_rect,GluonProp256)

  x128 = 1.0d0/128.0d0
  x256 = 1.0d0/256.0d0

  do iz=irz,jrz
     do iy=iry,jry
        do ix=irx,jrx
           dyondx = (GluonProp256(ix,iy,iz) - GluonProp128(ix,iy,iz))/(x256-x128)
           GluonPropExt(ix,iy,iz) = GluonProp256(ix,iy,iz) - dyondx*x256
        end do
     end do
  end do

  allocate(GluonBuff(nrx,nry,nrz))
  GluonBuff = GluonPropExt
  if ( mpi_rank == 0 ) then
     allocate(GluonProp(1:rmax,0:rmax_off,0:rmax_off))
     GluonProp(irx:jrx,iry:jry,irz:jrz) = GluonBuff

     do irank=1,mpi_size-1
        call MPI_Recv(GluonBuff,nrx*nry*nrz,mpi_double_precision,irank,irank,mpi_comm,mpi_status,mpierror)
        call GetSubspace(irank,irx,jrx,iry,jry,irz,jrz)
        GluonProp(irx:jrx,iry:jry,irz:jrz) = GluonBuff
     end do

     open(unit=100,file=outputfile,action="write",status="unknown",form=trim(formatstr(iformat)) )
     if ( iformat == 0) then
        write(100) rmax, rmax_off
        write(100) GluonProp
     else
        do iz=0,rmax_off
           do iy=0,rmax_off
              do ix=1,rmax
                 write(100,'(3I4,F20.15)') ix,iy,iz,GluonProp(ix,iy,iz)
              end do
           end do
        end do
     end if
     close(100)

     deallocate(GluonProp)
  else
     call MPI_Send(GluonBuff,nrx*nry*nrz,mpi_double_precision,0,mpi_rank,mpi_comm,mpierror)
  end if

  call MPI_Barrier(mpi_comm,mpierror)

  deallocate(GluonBuff)
  deallocate(GluonProp128)
  deallocate(GluonProp256)
  deallocate(GluonPropExt)

  call date_and_time(date=endDate, time=endTime)
  if ( i_am_root ) then
     print '(a,a8,a,a10)', "Started on ", trim(startDate), " at ", trim(startTime)
     print '(a,a8,a,a10)', "Finished on ", trim(endDate), " at ", trim(endTime)
  end if

  call FinaliseMPI

contains

  subroutine Coulomb(nL,c_plaq,c_rect,GluonProp)
    ! Derived from Urs Hellers' code that calculates the tree-level gluon propagator. 

    integer :: nL
    real(dp) :: c_plaq,c_rect
    real(dp) :: GluonProp(irx:jrx,iry:jry,irz:jrz)

    integer :: l,lh,lh1

    integer k1,k2,k3,i1,i2,i3
    real(dp) :: cf0,cf1,cf2,cf3,pil,p,volm1,s2(0:nL/2),c2(0:nL/2),c(0:nL)
    real(dp) :: gl,abst,sum,cx1,cx12,sum1,sum2
    real(dp) :: cc, ss, sc, sss, ccc, scsc
    real(dp) :: cf03p, cf1p, cf23p, d44

    l = nL
    lh = l/2
    lh1=lh-1

    cc    = c_plaq + 8.0d0*c_rect
    cf0   = c_plaq / cc
    cf1   = c_rect / cc

    pil   = pi/dble(l)
    volm1 = 1.0d0/dble(l*l*l)

    do k1 = 0,lh
       p      = pil*dble(k1)
       s2(k1) = dsin(p)**2
       c2(k1) = dcos(p)**2
       c(k1)  = dcos(2.0d0*p)
    enddo
    do k1   = lh+1,l
       p     = pil*dble(k1)
       c(k1) = dcos(2.0d0*p)
    enddo

    cf03p = 4.0d0 * cf0
    cf1p  = 16.0d0 * cf1

    do i3=irz,jrz
       do i2=iry,jry
          do i1=irx,jrx

             sum   = 0.0d0
             do k1 = 0,lh
                sum1  = 0.0d0
                cx1   = c(mod(i1*k1,l))
                do k2 = 0,lh
                   cx12   = cx1 * c(mod(i2*k2,l))
                   ss     = s2(k1) + s2(k2)
                   sc     = s2(k1)*c2(k1) + s2(k2)*c2(k2)
                   sss    = ss + 1.0d0
                   d44    = cf03p*sss + cf1p*(sc+sss)
                   sum2   = cx12 * c(mod(i3*lh,l)) / d44
                   if ( k1+k2 .ne. 0 ) then
                      d44  = cf03p*ss + cf1p*(sc+ss)
                      sum2 = sum2 + cx12/d44
                   end if
                   do k3 = 1,lh1
                      sss  = ss + s2(k3)
                      scsc = sc + s2(k3)*c2(k3)
                      d44  = cf03p*sss + cf1p*(scsc+sss)
                      sum2 = sum2 + 2.0d0*cx12*c(mod(i3*k3,l))/d44
                   end do
                   if ( mod(k2,lh) .eq. 0 ) then
                      sum1 = sum1 + sum2
                   else
                      sum1 = sum1 + 2.0d0*sum2
                   endif
                end do
                if ( mod(k1,lh) .eq. 0 ) then
                   sum    = sum + sum1
                else
                   sum    = sum + 2.0d0*sum1
                end if
             end do

             gl         = 4.0d0*pi*sum*volm1
             GluonProp(i1,i2,i3) = gl
          end do
       end do
    end do
  end subroutine Coulomb

  function rank_to_coords(rank) result (coords)
    integer :: rank

    integer, dimension(3) :: coords

    coords(3) = mod(rank,procz)
    coords(2) = mod(rank/procz,procy)
    coords(1) = rank/(procz*procy)


  end function rank_to_coords

  function coords_to_rank(coords) result (rank)
    integer, dimension(3) :: coords

    integer :: rank

    rank = coords(3) + procz*coords(2) + procz*procy*coords(1)

  end function coords_to_rank

  subroutine GetSubspace(irank,irx,jrx,iry,jry,irz,jrz)
    integer :: irank,irx,jrx,iry,jry,irz,jrz

    integer, dimension(3) :: coords

    coords = rank_to_coords(irank)

    irx = nrx*coords(1)+ 1
    jrx = nrx*(coords(1) + 1)

    iry = nry*coords(2)
    jry = nry*(coords(2)+1)-1

    irz = nrz*coords(3)
    jrz = nrz*(coords(3)+1)-1


  end subroutine GetSubspace

  subroutine SetProcessTopology

    isize = mpi_size

    procz = 1
    do idiv=1,size(div_off)
       if ( ( isize / div_off(idiv) > 0 ) .and. ( mod(isize,div_off(idiv)) == 0 ) ) then
          procz = div_off(idiv)
       end if
    end do
    isize = isize / procz

    procy = 1
    do idiv=1,size(div_off)
       if ( ( isize / div_off(idiv) > 0 ) .and. ( mod(isize,div_off(idiv)) == 0 ) ) then
          procy = div_off(idiv)
       end if
    end do
    isize = isize / procy

    procx = 1
    do idiv=1,size(div_r)
       if ( ( isize / div_r(idiv) > 0 ) .and. ( mod(isize,div_r(idiv)) == 0 ) ) then
          procx = div_r(idiv)
       end if
    end do
    isize = isize / procx

    nrz = (rmax_off+1) / procz
    nry = (rmax_off+1) / procy
    nrx = rmax / procx

    if ( procx*procy*procz /= mpi_size ) call AbortMPI("Invalid processor count: (px,py,pz) = "//str(procx,12)//str(procy,12)//str(procz,12))

  end subroutine SetProcessTopology

end program LattCoulTerm

