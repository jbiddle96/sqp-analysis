include 'lapack.f90'

module Eigensolve
  use Kinds
  use CorrelatorOps
  use MathsOps
  use lapack95, only: ggev
  implicit none
  integer :: nsmear

contains

  subroutine Symmetrise(gVarJack, ncon)
    ! Symmetrise last 2 indices of gVarJack
    ! Also symmetrises the higher order jackknife ensembles
    real(DP), dimension(:,0:,0:,0:,:,:), intent(inout) :: gVarJack
    integer, intent(in) :: ncon

    integer :: icon, jcon, kcon, i, j

    do j=1,nsmear-1
      do i=j+1,nsmear
        ! Zeroth order
        gVarJack(:,0,0,0,i,j) = (gVarJack(:,0,0,0,i,j)&
            + gVarJack(:,0,0,0,j,i))/2.0d0
        gVarJack(:,0,0,0,j,i) = gVarJack(:,0,0,0,i,j)

        ! First order
        do icon=1,ncon
          gVarJack(:,icon,0,0,i,j) = (gVarJack(:,icon,0,0,i,j)&
              + gVarJack(:,icon,0,0,j,i))/2.0d0
          gVarJack(:,icon,0,0,j,i) = gVarJack(:,icon,0,0,i,j)

          ! Second order
          do jcon = 1,ncon
            gVarJack(:,icon,jcon,0,i,j) = (gVarJack(:,icon,jcon,0,i,j)&
                + gVarJack(:,icon,jcon,0,j,i))/2.0d0
            gVarJack(:,icon,jcon,0,j,i) = gVarJack(:,icon,jcon,0,i,j)

            ! Third order
            do kcon = 1, ncon
              gVarJack(:,icon,jcon,kcon,i,j) = (gVarJack(:,icon,jcon,kcon,i,j)&
                  + gVarJack(:,icon,jcon,kcon,j,i))/2.0d0
              gVarJack(:,icon,jcon,kcon,j,i) = gVarJack(:,icon,jcon,kcon,i,j)
            end do

          end do
        end do
      end do
    end do

  end subroutine Symmetrise


  subroutine Normalise(gVarJack, ncon)
    implicit None
    ! Normalise the 2 smearing indices of gVarJack
    ! Also normalises the first and second order jackknife ensembles
    ! Normalisation is done by imposing g_ij -> g_ij/(sqrt(g_ii(0)) * sqrt(g_jj(0)))
    real(DP), dimension(:,0:,0:,0:,:,:), intent(inout) :: gVarJack
    integer, intent(in) :: ncon

    real(DP), dimension(nsmear) :: g_diag_sqrt
    integer :: icon, jcon, kcon, i, j, k
    character(len=32) :: fmt

    ! 0th order jackknife
    do i=1,nsmear
      g_diag_sqrt(i) = sqrt(gVarJack(1,0,0,0,i,i))
    end do
    ! write(*,*) 'g_diag_sqrt:'
    ! write(*,*) g_diag_sqrt(:)
    ! write(fmt,'(a,i0,a)') '(',nsmear, '(f0.5,2x))'
    ! write(*,*)
    ! write(*,*) "Before:"
    ! write(*,fmt) gVarJack(3,0,0,0,:,:)
    do j=1,nsmear
      do i=1,nsmear
        gVarJack(:,0,0,0,i,j) = gVarJack(:,0,0,0,i,j) &
            / (g_diag_sqrt(i) * g_diag_sqrt(j))
      end do
    end do
    if(any(gVarJack(:,0,0,0,:,:) > HUGE(g_diag_sqrt(1)))) then
      write(*,*) 'Infinite values found in gVarJack'
      call exit
    end if

    ! write(*,*) "After:"
    ! write(*,fmt) gVarJack(3,0,0,0,:,:)

    ! write(*,*) "finished 0"

    ! 1st order jackknife
    do icon=1,ncon
      do k=1,nsmear
        g_diag_sqrt(k) = sqrt(gVarJack(1,icon,0,0,k,k))
      end do

      do j=1,nsmear
        do i=1,nsmear
          gVarJack(:,icon,0,0,i,j) = gVarJack(:,icon,0,0,i,j) &
              / (g_diag_sqrt(i) * g_diag_sqrt(j))
        end do
      end do
    end do
    ! write(*,*) "finished 1"

    ! 2nd order jackknife
    do jcon=1,ncon
      do icon=1,ncon
        do k=1,nsmear
          g_diag_sqrt(k) = sqrt(gVarJack(1,icon,jcon,0,k,k))
        end do

        do j=1,nsmear
          do i=1,nsmear
            gVarJack(:,icon,jcon,0,i,j) = gVarJack(:,icon,jcon,0,i,j) &
                / (g_diag_sqrt(i) * g_diag_sqrt(j))
          end do
        end do

      end do
    end do

    ! 3rd order jackknife
    do kcon = 1, ncon
      do jcon = 1, ncon
        do icon = 1, ncon
          do k=1,nsmear
            g_diag_sqrt(k) = sqrt(gVarJack(1,icon,jcon,kcon,k,k))
          end do

          do j=1,nsmear
            do i=1,nsmear
              gVarJack(:,icon,jcon,0,i,j) = gVarJack(:,icon,jcon,kcon,i,j) &
                  / (g_diag_sqrt(i) * g_diag_sqrt(j))
            end do
          end do

        end do
      end do
    end do

  end subroutine Normalise


  subroutine analyse_correlators(g, avg_ratio, corr_components)
    real(DP), dimension(:, :, :) :: g
    real(DP), dimension(:) :: avg_ratio
    real(DP), dimension(:, :, :) :: corr_components

    ! Local vars
    real(DP), dimension(:), allocatable :: off_diag_avg, diag_avg
    integer :: nt, i, j

    nt = size(avg_ratio)
    ! Calculate the ratio of the off diagonal correlators to the diagonal
    allocate(off_diag_avg(nt))
    allocate(diag_avg(nt))
    off_diag_avg = 0.0d0
    diag_avg = 0.0d0
    do j = 1, nsmear - 1
      do i = j + 1, nsmear
        off_diag_avg = off_diag_avg + g(:, i, j)
      end do
    end do
    off_diag_avg(:) = off_diag_avg(:) / (real(nsmear * (nsmear - 1), DP) / 2.0d0)

    do i = 1, nsmear
      diag_avg = diag_avg + g(:, i, i)
    end do
    diag_avg(:) = diag_avg(:) / real(nsmear, DP)

    avg_ratio = off_diag_avg / diag_avg

    do j = 1, nsmear
      do i = 1, nsmear
        corr_components(:, i, j) = g(:, i, j) / sqrt(abs(g(:, i, i) * g(:, j, j)))
      end do
    end do

  end subroutine analyse_correlators


  subroutine SortEMF(g0Jack, ncon, nt)
    ! Sort the EMF values by high to low in the variational (last) index
    real(DP), dimension(:,0:,0:,0:,:), intent(inout) :: g0Jack
    integer, intent(in) :: ncon, nt

    real(DP) :: temp
    integer  :: icon, jcon, it, i, j

    do it = 1,nt
      do i = 1,nsmear - 1
        ! Sort by average values
        j = minloc(g0Jack(it,0,0,0,i:nsmear), dim=1) + i - 1

        if(i .NE. j) then
          call Swap(g0Jack(it,0,0,0,:), i, j)

          do icon = 1,ncon + 1
            ! Sort the first-order jackknife subensembles and errors accordingly
            call Swap(g0Jack(it,icon,0,0,:), i, j)
            ! Sort the second-order jackknife subensembles and errors
            do jcon = 1,ncon + 1
              call Swap(g0Jack(it,icon,jcon,0,:), i, j)
            end do
          end do

        end if
      end do
    end do

  end subroutine SortEMF

  subroutine SolveGEP(gVar, t0, dt, eigenVals, vl, vr, stat)
    real(DP), dimension(:,:,:), intent(in) :: gVar
    integer, intent(in) :: t0, dt
    real(DP), dimension(:), intent(out) :: eigenVals
    real(DP), dimension(:,:), intent(out) :: vl, vr
    integer :: stat

    ! Local variables
    real(DP), dimension(nsmear,nsmear) :: gt0, gt0pdt
    real(DP), dimension(nsmear) :: alphar, alphai, beta
    real(DP), parameter :: eps = 10d-10
    integer :: i
    character(len=32) :: fmt

    write(fmt, '(a,i0,a)')'(', nsmear, 'f)'

    gt0(:,:) = gVar(t0,:,:)
    gt0pdt(:,:) = gVar(t0 + dt,:,:)

    ! Solve the generalised eigenvalue problem
    ! Stores eigenvalues as (alphar + alphai)/beta
    ! Stores left and right eigenvectors in the
    ! columns of vl and vr respectively
    !! Could potentially use positive symmetric solver !!
    !! Need to ensure positivity though !!
    call ggev(gt0pdt, gt0, alphar, alphai, beta, vl, vr)
    ! Check for complex eigenvalues
    if(any(abs(alphai) > eps)) then
      stat = 1
      write(*,*) "Complex eigenvalues found at ", "t0 = " , t0, " dt = ", dt
      write(*,*) "Eigenvalues:"
      write(*,*) "real part"
      write(*,fmt) alphar
      write(*,*) "imaginary part"
      write(*,fmt) alphai
      write(*,*) "normalising factor"
      write(*,fmt) beta
      write(*,*) "g(t0)"
      write(*,fmt) gVar(t0,:,:)
      write(*,*) "g(t0 + dt)"
      write(*,fmt) gVar(t0 + dt,:,:)
      write(*,*) "Exiting with error code", stat
      return
    end if

    do i = 1,nsmear
      ! Check for zero eigenvalues
      if(abs(beta(i)) > eps) then
        eigenVals(i) = alphar(i)/beta(i)
      else
        stat = 2
        eigenVals(i) = 0.0d0
        write(*,*) "Zero eigenvalues found at ", "t0 = " , t0, " dt = ", dt
        write(*,*) "alpha real"
        write(*,*) alphar(i)
        write(*,*) "beta"
        write(*,*) beta(i)
        write(*,*) "g(t0) = "
        write(*,fmt) gVar(t0,:,:)
        write(*,*) "g(t0 + dt) = "
        write(*,fmt) gVar(t0 + dt,:,:)
        write(*,*) "Exiting with error code", stat
        write(*,*)
        return
      end if
    end do
    stat = 0
  end subroutine SolveGEP

  subroutine SortEVals(eVals, vl, vr)
    ! Sort the eigenvalues and eigenvectors such that
    ! they are in descending order by the eigenvalues
    ! This is because the largest eigenvalue corresponds to the lowest energy level
    real(DP), dimension(:,:), intent(inout) :: vl, vr
    real(DP), dimension(:), intent(inout) :: eVals

    real(DP), dimension(nsmear) :: tempVec
    real(DP) :: temp
    integer :: i, j

    do i = 1,nsmear - 1
      ! Find maximum index in remaining array elements
      j = maxloc(eVals(i:nsmear), dim=1) + i - 1
      if(i .NE. j) then
        ! Swap eigenvalues
        temp = eVals(i)
        eVals(i) = eVals(j)
        eVals(j) = temp

        ! Swap eigenvectors
        tempVec(:) = vl(:,i)
        vl(:,i) = vl(:,j)
        vl(:,j) = tempVec

        tempVec(:) = vr(:,i)
        vr(:,i) = vr(:,j)
        vr(:,j) = tempVec
      end if
    end do

  end subroutine SortEVals

  subroutine Projectg(g, vl, gVar, vr)
    real(DP), dimension(:,:), intent(out) :: g
    real(DP), dimension(:,:,:), intent(in):: gVar
    real(DP), dimension(:,:), intent(in) :: vl, vr

    integer :: i, j, nt

    nt = size(g, dim=1)

    do j = 1,nsmear
      do i = 1,nt
        g(i,j) = dot_product(vl(:,j), matmul(gVar(i, :, :), vr(:,j)))
      end do
    end do

  end subroutine Projectg

  subroutine PerformVarAnalysis(gVarJack, t0, dt, ncon, g0Jack, eVals, stat, jack_level)
    ! Perform the full variational analysis and return the projected
    ! eigenvector, g0Jack.

    real(DP), dimension(:,0:,0:,0:,:,:) :: gVarJack
    integer, intent(in) :: t0, dt, ncon
    real(DP), dimension(:,0:,0:,0:,:) :: g0Jack
    integer :: stat
    integer, optional, intent(in) :: jack_level

    ! Local vars
    integer :: icon, jcon, kcon
    integer :: jack_level_

    ! Generalised eigenvalue solver variables
    real(DP), dimension(nsmear,nsmear) :: vr, vl
    real(DP), dimension(nsmear), intent(inout) :: eVals

    if(present(jack_level)) then
      jack_level_ = jack_level
    else
      jack_level_ = 3
    end if

    call SolveGEP(gVarJack(:,0,0,0,:,:), t0, dt, eVals, vl, vr, stat)
    if(stat .ne. 0) return
    call SortEVals(eVals, vl, vr)
    ! Project onto g_a using the eigenvectors
    call Projectg(g0Jack(:,0,0,0,:), vl(:,:), gVarJack(:,0,0,0,:,:), vr(:,:))

    if(jack_level_ > 0) then
      ! Repeat variational analysis for first-order jackknife ensemble
      do icon = 1,ncon
        call SolveGEP(gVarJack(:,icon,0,0,:,:), t0, dt, eVals, vl, vr, stat)
        if(stat .ne. 0) return
        call SortEVals(eVals, vl, vr)
        call Projectg(g0Jack(:,icon,0,0,:), vl(:,:), &
            gVarJack(:,icon,0,0,:,:), vr(:,:))

        if(jack_level_ > 1) then
          ! Second order
          do jcon = 1,ncon
            call SolveGEP(gVarJack(:,icon,jcon,0,:,:), t0, dt, eVals, vl, vr, stat)
            if(stat .ne. 0) return
            call SortEVals(eVals, vl, vr)
            call Projectg(g0Jack(:,icon,jcon,0,:), vl(:,:), &
                gVarJack(:,icon,jcon,0,:,:), vr(:,:))

            if(jack_level_ > 2) then
              ! Third order
              do kcon = 1,ncon
                call SolveGEP(gVarJack(:,icon,jcon,kcon,:,:), t0, dt, eVals, vl, vr, stat)
                if(stat .ne. 0) return
                call SortEVals(eVals, vl, vr)
                call Projectg(g0Jack(:,icon,jcon,kcon,:), vl(:,:), &
                    gVarJack(:,icon,jcon,kcon,:,:), vr(:,:))
              end do
            end if
          end do
        end if
      end do
    end if
  end subroutine PerformVarAnalysis

end module Eigensolve
