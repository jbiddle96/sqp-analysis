module CorrelatorOps
  use Kinds
  use lapack95, only: sytrf, sytri
  implicit none

  interface fitcf
    module procedure fitcf, fitcf_cov
  end interface fitcf

  interface constant
    module procedure constant_1d, constant_2d
  end interface constant

  interface GetCovarMat
    module procedure GetCovarMat_1, GetCovarMat_2
  end interface GetCovarMat

contains

  !******** ********* ********* ********* ********* ********* ********* **
  !
  SUBROUTINE complement(gin,g,ncon,nt)
    !
    !  Produces averages over all but 0, all but 1, all but 2, all but 3
    !  configurations, arranged according to :
    !    g(0,     0,     0)      average over all configurations
    !    g(ncon+1,0,     0)      will be jackknifed error for g(0,0,0)
    !    g(ncon+2,0,     0)      will be gavg - g(0,0,0)
    !    g(i,     0,     0)      average over all but 'i'th conf.,
    !                              (i=1,ncon)
    !    g(i,     ncon+1,0)      will be jackknifed error for g(i,0,0)
    !    g(i,     j,     0)      average over all but 'i'th and 'j'th conf.
    !                              (i,j=1,ncon), g(i,i,0)=g(i,0,0)
    !    g(i,     j,     ncon+1) will be jackknifed error for g(i,j,0)
    !    g(i,     j,     k)      av. over all but 'i'th, 'j'th, 'k'th conf.
    !                              (i,j,k=1,ncon), (i.ne.j)
    !                              g(i,j,i)=g(i,j,j)=g(i,j,0)
    !
    !******** ********* ********* ********* ********* ********* ********* **
    !
    !      implicit character*1 (a-z)
    !
    integer ::   nt,ncon
    !

    !
    real(dp) ::      gin(nt,ncon)
    real(dp) ::      g  (nt,0:ncon+2,0:ncon+1,0:ncon+1)
    !
    integer ::   it,icon,jcon,kcon
    do it=1,nt
      !
      g(it,0,0,0)=0.0d0
      !
      do icon=1,ncon
        g(it,0,0,0)=g(it,0,0,0)+gin(it,icon)
      end do
      !
      do icon=1,ncon
        g(it,icon,0,0)=g(it,0,0,0)-gin(it,icon)
        do jcon=1,ncon
          g(it,icon,jcon,0)=g(it,icon,0,0)-gin(it,jcon)
          do kcon=1,ncon
            g(it,icon,jcon,kcon)=(g(it,icon,jcon,0)-gin(it,kcon))/(ncon-3)
          end do
        end do
      end do
      !
      !  handling of normalization for first two levels of Jackknife
      !
      do icon=1,ncon
        !
        do jcon=1,ncon
          g(it,icon,jcon,0)=g(it,icon,jcon,0)/(ncon-2)
        end do
        !
        g(it,icon,0,0)=g(it,icon,0,0)/(ncon-1)
        !
      end do
      !
      g(it,0,0,0)=g(it,0,0,0)/ncon
      !
    end do

    CALL jack1(g(:,:,0,0),1,nt,1,nt,ncon)
    CALL jack2(g(:,:,:,0),1,nt,1,nt,ncon)
    CALL jack3(g(:,:,:,:),1,nt,1,nt,ncon)

  end SUBROUTINE complement

  subroutine GetEffMass(g,ncon,nt)
    integer ::    nt,ncon

    real(dp) ::       g(nt,0:ncon+2,0:ncon+1,0:ncon+1)

    !
    !  Create Effective Masses
    !
    CALL effectiveMass(g,ncon,nt)

    CALL jack1(g(:,:,0,0),1,nt,1,nt-1,ncon)
    CALL jack2(g(:,:,:,0),1,nt,1,nt-1,ncon)
    CALL jack3(g(:,:,:,:),1,nt,1,nt-1,ncon)

  end subroutine GetEffMass

  SUBROUTINE fitcf(g,m,ncon,itb,ite,nt, chisq)
    real(dp) ::       g(nt,0:ncon+2,0:ncon+1,0:ncon+1)
    real(dp) ::       m(1,0:ncon+2,0:ncon+1)
    real(DP) :: chisq
    integer, intent(in) ::    ncon,itb,ite,nt

    real(DP), dimension(nt, nt, 0:ncon, 0:ncon) :: C
    integer :: icon,jcon

    do icon=1,ncon
      do jcon=1,ncon
        if (icon .ne. jcon) then

          CALL constant(g(:,icon,jcon,0),g(:,icon,jcon,ncon+1),itb,ite,m(1,icon,jcon),nt, chisq)

        end if
      end do

      CALL constant(g(:,icon,0,0),g(:,icon,ncon+1,0),itb,ite,m(1,icon,0),nt, chisq)

      m(1,icon,icon)=m(1,icon,0)
    end do

    CALL constant(g(:,0,0,0),g(:,ncon+1,0,0),itb,ite,m(1,0,0),nt, chisq)

    CALL jack2(m(1,0:ncon+2,0:ncon+1),1,1,1,1,ncon)
    CALL jack1(m(1,0:ncon+2,0),1,1,1,1,ncon)

  end subroutine fitcf

  SUBROUTINE fitcf_cov(g,C,m,ncon,itb,ite,nt, chisq)
    real(dp) ::       g(nt,0:ncon+2,0:ncon+1,0:ncon+1)
    real(dp) ::       m(1,0:ncon+2,0:ncon+1)
    real(DP) :: chisq
    integer, intent(in) ::    ncon,itb,ite,nt

    real(DP), dimension(nt, nt, 0:ncon, 0:ncon) :: C
    integer :: icon,jcon

    do icon=1,ncon
      do jcon=1,ncon
        if (icon .ne. jcon) then

          CALL constant(g(:,icon,jcon,0),C(:,:,icon,jcon),itb,ite,m(1,icon,jcon),nt, chisq)

        end if
      end do

      CALL constant(g(:,icon,0,0),C(:,:,icon,0),itb,ite,m(1,icon,0),nt, chisq)

      m(1,icon,icon)=m(1,icon,0)
    end do

    CALL constant(g(:,0,0,0),C(:,:,0, 0),itb,ite,m(1,0,0),nt, chisq)

    CALL jack2(m(1,0:ncon+2,0:ncon+1),1,1,1,1,ncon)
    CALL jack1(m(1,0:ncon+2,0),1,1,1,1,ncon)

  end subroutine fitcf_cov

  SUBROUTINE effectiveMass(g,ncon,nt)
    !
    !  Converts a correlation function complement set to an effective mass
    !
    integer ::   nt, ncon

    !
    real(dp) ::      g(nt,0:ncon+2,0:ncon+1,0:ncon+1)
    real(dp) ::      em
    integer ::   it, icon, jcon, kcon
    !

    do kcon = 1, ncon
      do jcon = 1, ncon
        do icon = 1, ncon
          do it = 1, nt-1


            g(it,icon,jcon,kcon) = &
                &            sign(1.0d0, g(it,icon,jcon,kcon)) * &
                &            ( log(abs(g(it,icon,jcon,kcon))) -  &
                &              log(abs(g(it+1,icon,jcon,kcon))) )


          end do
        end do
      end do
    end do
    !


    do jcon = 1, ncon
      do icon = 1, ncon
        do it = 1, nt-1
          g(it,icon,jcon,0) = &
              &            sign(1.0d0, g(it,icon,jcon,0)) * &
              &            ( log(abs(g(it,icon,jcon,0))) -  &
              &              log(abs(g(it+1,icon,jcon,0))) )

        end do
      end do
    end do
    !
    do icon = 1, ncon
      do it = 1, nt-1
        g(it,icon,0,0) = &
            &            sign(1.0d0, g(it,icon,0,0)) * &
            &            ( log(abs(g(it,icon,0,0))) -  &
            &              log(abs(g(it+1,icon,0,0))) )

      end do
    end do
    !
    do it = 1, nt-1
      g(it,0,0,0) = &
          &            sign(1.0d0, g(it,0,0,0)) * &
          &            ( log(abs(g(it,0,0,0))) - &
          &              log(abs(g(it+1,0,0,0))) )
    end do

  end SUBROUTINE effectiveMass

  SUBROUTINE jack1(g,nlb,nle,lb,le,ncon)
    !
    ! computes a first-order jackknife
    !
    !implicit character*1 (a-z)
    !
    integer ::    ncon
    !

    !
    integer ::    nlb,nle,lb,le,l,icon
    !
    real(dp) ::    g(nlb:nle,0:ncon+2), gavg
    !
    !*VDIR: prefer scalar
    !
    do l=lb,le
      g(l,ncon+1)=0.0d0
      gavg=0.0d0
      do icon=1,ncon
        gavg=gavg+g(l,icon)
      end do
      gavg=gavg/ncon
      do icon=1,ncon
        g(l,ncon+1)=g(l,ncon+1)+(g(l,icon)-gavg)**2
      end do
      g(l,ncon+1)=sqrt(g(l,ncon+1)*(ncon-1)/ncon)
      g(l,ncon+2)=gavg - g(l,0)
    end do
  end SUBROUTINE jack1

  SUBROUTINE jack2(g,nlb,nle,lb,le,ncon)
    !
    ! computes a second-order jackknife
    !
    !implicit character*1 (a-z)
    !
    integer ::    ncon
    !

    !
    integer ::    nlb,nle,lb,le,l,icon,jcon
    !
    real(dp) ::       g(nlb:nle,0:ncon+2,0:ncon+1), gavg
    !
    !*VDIR: prefer scalar
    !
    do l=lb,le
      do icon=1,ncon
        g(l,icon,ncon+1)=0.0d0
        gavg=0.0d0
        do jcon=1,ncon
          if (jcon.ne.icon) then
            gavg=gavg+g(l,icon,jcon)
          end if
        end do
        gavg=gavg/(ncon-1)
        do jcon=1,ncon
          if (jcon.ne.icon) then
            g(l,icon,ncon+1)=g(l,icon,ncon+1) + (g(l,icon,jcon)-gavg)**2
          end if
        end do
        !
        g(l,icon,ncon+1)=sqrt(g(l,icon,ncon+1)*(ncon-2)/(ncon-1))
        !
      end do
    end do
  end SUBROUTINE jack2

  SUBROUTINE jack3(g,nlb,nle,lb,le,ncon)
    !
    ! computes a third-order jackknife
    !
    !implicit character*1 (a-z)
    !
    integer ::    ncon

    !

    !
    integer ::    nlb,nle,lb,le,l,icon,jcon,kcon
    !
    real(dp) ::       g(nlb:nle,0:ncon+2,0:ncon+1,0:ncon+1), gavg
    !
    !*VDIR: prefer scalar
    !
    do l=lb,le
      do icon=1,ncon
        do jcon=1,ncon
          if (jcon.ne.icon) then
            g(l,icon,jcon,ncon+1)=0.0d0
            gavg=0.0d0
            do kcon=1,ncon
              if (kcon.ne.icon.and.kcon.ne.jcon) then
                gavg=gavg+g(l,icon,jcon,kcon)
              end if
            end do
            gavg=gavg/(ncon-2)
            do kcon=1,ncon
              if (kcon.ne.icon.and.kcon.ne.jcon) then
                g(l,icon,jcon,ncon+1)=g(l,icon,jcon,ncon+1) + (g(l,icon,jcon,kcon)-gavg)**2
              end if
            end do
            g(l,icon,jcon,ncon+1) = sqrt(g(l,icon,jcon,ncon+1)*(ncon-3)/(ncon-2))
          end if
        end do
      end do
    end do
  end SUBROUTINE jack3

  subroutine JackErr(sigma, zJack)
    ! Computes a simple first-order jackknife error, sigma
    ! for nt jackknife ensembles
    real(DP), dimension(:), intent(out)  :: sigma
    real(DP), dimension(:,:), intent(in) :: zJack

    real(DP), dimension(size(sigma)) :: zAvg
    real(DP) :: norm
    integer :: i, nt, ncon

    ncon = size(zJack, dim=2)
    nt = size(sigma)
    zAvg = sum(zJack, dim=2)/real(ncon, DP)

    norm = real(ncon - 1, DP)/real(ncon, DP)
    do i = 1,nt
      sigma(i) = sqrt(norm * sum((zJack(i,:) - zAvg(i))**2))
    end do

  end subroutine JackErr

  subroutine GetCovarMat_1(g0Jack, C, ncon, nt)
    ! Construct a covariance matrix for the 0th and 1st order jackknife ensembles
    real(DP), dimension(:,0:,0:,0:), intent(in) :: g0Jack
    integer, intent(in) :: ncon, nt
    real(DP), dimension(:,:,0:), intent(out) :: C

    real(DP), dimension(nt,ncon,0:ncon) :: jack
    real(DP), dimension(nt,0:ncon) :: jackAvg
    integer :: it, jt, icon, jcon

    jack(:,:,:) = g0Jack(:,1:ncon,0:ncon,0)
    jackAvg(:,0) = sum(g0Jack(:,1:ncon,0,0), dim=2)/real(ncon, DP)
    jackAvg(:,1:ncon) = sum(g0Jack(:,1:ncon,1:ncon,0), dim=3)/real(ncon, DP)

    C = 0.0d0

    do jt = 1,nt
      do it = jt,nt
        ! Calculate covariance from first order jackknife ensemble
        C(it,jt,0) = sum((jack(it,:,0) - jackAvg(it,0)) &
            * (jack(jt,:,0) - jackAvg(jt,0)))
        C(jt,it,0) = C(it,jt,0)
        do icon=1,ncon
          ! Calculate covariance for each first order jackknife subensemble
          ! using the second order jackknife ensembles
          C(it,jt,icon) = sum((jack(it,icon,:) - jackAvg(it,icon))&
              * (jack(jt,icon,:) - jackAvg(jt,icon)))
          C(jt,it,icon) = C(it,jt,icon)

        end do
      end do
    end do

    C(:,:,0) = real(ncon - 1, DP) * C(:,:,0)/real(ncon, DP)
    C(:,:,1:ncon) = real(ncon - 2, DP) * C(:,:,1:ncon)/real(ncon - 1, DP)

  end subroutine GetCovarMat_1

  subroutine GetCovarMat_2(g0Jack, C, ncon, nt)
    ! Construct a covariance matrix for the 0th, 1st and 2nd order jackknife ensembles
    real(DP), dimension(:,0:,0:,0:), intent(in) :: g0Jack
    integer, intent(in) :: ncon, nt
    real(DP), dimension(:,:,0:, 0:), intent(out) :: C

    real(DP), dimension(nt, 0:ncon, 0:ncon) :: jackAvg
    integer :: it, jt, icon, jcon

    jackAvg(:, 0, 0) = sum(g0Jack(:,1:ncon,0,0), dim=2)/real(ncon, DP)
    jackAvg(:, 1:ncon, 0) = sum(g0Jack(:,1:ncon,1:ncon,0), dim=3)/real(ncon, DP)
    jackAvg(:, 1:ncon, 1:ncon) = sum(g0Jack(:,1:ncon,1:ncon,1:ncon), dim=4)/real(ncon, DP)

    C = 0.0d0

    do jt = 1,nt
      do it = jt,nt
        ! Calculate covariance from first order jackknife ensemble
        C(it,jt,0, 0) = sum((g0Jack(it,1:ncon,0, 0) - jackAvg(it,0, 0)) &
            * (g0Jack(jt,1:ncon,0, 0) - jackAvg(jt,0, 0)))
        C(jt,it,0, 0) = C(it,jt,0, 0)

        do icon=1,ncon
          ! Calculate covariance for each first order jackknife subensemble
          ! using the second order jackknife ensembles
          C(it,jt,icon, 0) = sum((g0Jack(it,icon,1:ncon, 0) - jackAvg(it,icon, 0))&
              * (g0Jack(jt,icon,1:ncon, 0) - jackAvg(jt,icon, 0)))
          C(jt,it,icon, 0) = C(it,jt,icon, 0)

          do jcon = 1, ncon
            ! Calculate covariance for each second order jackknife subensemble
            ! using the third order jackknife ensembles
            C(it,jt,icon, jcon) = sum((g0Jack(it,icon,jcon, 1:ncon) - jackAvg(it,icon, jcon))&
                * (g0Jack(jt,icon,jcon, 1:ncon) - jackAvg(jt,icon, jcon)))
            C(jt,it,icon, jcon) = C(it,jt,icon, jcon)
          end do

        end do
      end do
    end do

    C(:,:,0, 0) = real(ncon - 1, DP) * C(:,:,0, 0)/real(ncon, DP)
    C(:,:,1:ncon, 0) = real(ncon - 2, DP) * C(:,:,1:ncon, 0)/real(ncon - 1, DP)
    C(:,:,1:ncon, 1:ncon) = real(ncon - 3, DP) * C(:,:,1:ncon, 1:ncon)/real(ncon - 2, DP)

  end subroutine GetCovarMat_2

  SUBROUTINE constant_1d(g,gerr,itb,ite,const,nt, chisq)
    !
    ! Least-squares fit of g to a constant
    !
    !  Solution by use of the Normal Equations.
    !
    !    y_i=a_1
    !
    !    y_i=g_i
    !    x_i=it
    !
    !    R_1=1
    !
    !  See Numerical Receipes Pages 509 to 511 for reference.
    !      Note: r==alpha ; b == beta.
    !
    !******** ********* ********* ********* ********* ********* ********* **
    !
    !implicit character*1 (a-z)
    !
    integer ::    nt
    !

    !
    real(dp) ::       g(nt),gerr(nt),const
    real(dp) ::       r,b,varinv,x,y, chisq
    !
    integer ::    it,itb,ite
    !
    r=0.0d0
    b=0.0d0
    !
    do it=itb,ite
      varinv=(1.0d0/gerr(it))**2
      x=it
      y=g(it)
      r=r+varinv
      b=b+y*varinv
    end do
    !
    ! solution is y=a=b * r^-1
    !
    const=b/r

    chisq = sum((g(itb:ite) - const)**2 * (1.0d0/gerr(itb:ite))**2 )

  end SUBROUTINE constant_1d


  SUBROUTINE constant_2d(g,cov,itb,ite,const, nt, chisq)
    !
    ! Least-squares fit of g to a constant via covariance matrix
    ! Uses closed form solution obtained by minimising
    ! chisq = R^T C^{-1} R
    ! Where R is the vector of residuals and C is the covariance matrix
    !
    !******** ********* ********* ********* ********* ********* ********* **
    !
    !
    real(dp), dimension(:), intent(in) :: g
    real(DP), DIMENSION(:, :), intent(in):: cov
    integer, intent(in):: itb, ite, nt

    real(DP), dimension(:, :), allocatable :: inv_cov
    real(DP), intent(out) :: const

    real(dp) :: r, chisq
    integer  :: it, n

    n = ite - itb + 1
    allocate(inv_cov(n, n))
    call inv_sym(cov(itb:ite, itb:ite), inv_cov)

    r = sum(inv_cov)

    const = sum(matmul(inv_cov, g(itb:ite))) / r

    chisq = sum((g(itb:ite) - const) * matmul(inv_cov, (g(itb:ite) - const)))

  end SUBROUTINE constant_2d


  subroutine Swap(vec, i, j)
    real(DP), dimension(:), intent(inout) :: vec
    integer, intent(in) :: i, j

    real(DP) :: temp

    temp = vec(i)
    vec(i) = vec(j)
    vec(j) = temp

  end subroutine Swap

  subroutine inv_sym(A, invA)
    ! Calculate the inverse of a nxn symmetric matrix A
    real(DP), dimension(:, :), intent(in) :: A
    real(DP), dimension(:, :), intent(out) :: invA
    integer :: i, j, n
    integer, dimension(size(A, 1)) :: ipiv
    character(20) :: fmt

    n = size(A, 1)
    invA = A

    call sytrf(invA, ipiv=ipiv)
    call sytri(invA, ipiv)

    do j = 1, n - 1
      do i = j + 1, n
        invA(i, j) = invA(j, i)
      end do
    end do

  end subroutine inv_sym

end module CorrelatorOps
