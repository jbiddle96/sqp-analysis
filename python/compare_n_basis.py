"""
Plot a ratio of two choices of differing variational basis
to observe differences
"""
import numpy as np
import argparse
import importlib
import os
import sqp_helper as sqp
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

importlib.reload(sqp)


def unique_mask(a):
    mask = (a[:-1] == a[1:])
    mask = np.logical_not(np.insert(mask, 0, False))
    return mask


def calc_moving_average(V, V_jack, r_init=5.0,
                        window_width=3.0, window_increment=0.2):

    V_fit = V.copy()
    V_fit_jack = V_jack.copy()

    r_max = np.amax(V['r'])
    n_windows = int(np.floor((r_max - r_init - window_width) / window_increment) ) + 1

    r_avg_list = []
    sigma_list = []
    # sigma_jack = np.zeros((n_windows, n_jack))
    sigma_jack = []
    fit = []

    r_start = r_init
    r_end = r_init + window_width
    # Loop through every window
    for iw in range(n_windows):
        mask = (V_fit['r'] >= r_start) & (V_fit['r'] <= r_end)
        r_window = V_fit['r'][mask]
        if(len(r_window) < 2):
            print(f'< 2 points in window [{r_start}, {r_end}]')
            break
        r_avg = r_start + window_width / 2.0
        r_avg_list.append(r_avg)
        V_window = V_fit['V'][mask]
        V_err_window = V_fit['V_err'][mask]
        fit, pcov = curve_fit(
            sqp.f_linear, r_window, V_window, sigma=V_err_window, absolute_sigma=True
        )
        sigma_list.append(fit[0])

        sigma_jack.append([])
        for ij, V_j in enumerate(V_fit_jack):
            V_jack_window = V_j['V'][mask]
            V_jack_err_window = V_j['V_err'][mask]
            fit, pcov = curve_fit(
                sqp.f_linear,
                r_window,
                V_jack_window,
                sigma=V_jack_err_window,
                absolute_sigma=True,
            )
            sigma_jack[-1].append(fit[0])

        r_start += window_increment
        r_end += window_increment

    return sigma_list, sigma_jack, r_avg_list


def moving_average_ratio(data1, data2):
    """
    Fit all windows of width window_width starting from r_init
    to a linear function. This gives a moving estimate of sigma
    as r increases.

    If a cornell fit has been done, can subtract off
    the coulomb term to give a better linear estimate
    """

    V_fit1 = data1[0].copy()
    V_fit_jack1 = data1[1].copy()

    V_fit2 = data2[0].copy()
    V_fit_jack2 = data2[1].copy()

    sigma1, sigma_jack1, r_avg = calc_moving_average(V_fit1, V_fit_jack1)
    print(f"ravg1 = {len(r_avg)}")
    sigma2, sigma_jack2, r_avg = calc_moving_average(V_fit2, V_fit_jack2)
    print(f"ravg2 = {len(r_avg)}")

    moving_avg1 = {}
    moving_avg1['r_avg'] = r_avg
    moving_avg1['sigma'] = sigma1
    moving_avg1['sigma_err'] = sqp.calc_jack_err(sigma_jack1, axis=1)

    moving_avg2 = {}
    moving_avg2['r_avg'] = r_avg
    moving_avg2['sigma'] = sigma2
    moving_avg2['sigma_err'] = sqp.calc_jack_err(sigma_jack2, axis=1)

    ratio = np.array(sigma1) / np.array(sigma2)
    ratio_jack = [np.array(Vj1) / np.array(Vj2)
                  for Vj1, Vj2 in zip(sigma_jack1, sigma_jack2)]

    ratio_err_list = sqp.calc_jack_err(ratio_jack, axis=1)

    ratio_average = {}
    ratio_average['r_avg'] = r_avg
    ratio_average['ratio'] = ratio
    ratio_average['ratio_err'] = ratio_err_list

    return moving_avg1, moving_avg2, ratio_average


parser = argparse.ArgumentParser(
    description='Compare variational methods.')
parser.add_argument('config_files', default='.', nargs=2,
                    help='Config files specifying where to load data from')

args = parser.parse_args()
config_files = args.config_files

base_data_dir = "fitted_points/"
out_dir = "./comparison_plots/"
if not os.path.exists(out_dir):
    os.makedirs(out_dir)

datasets = []

for filename in config_files:
    base_dir = os.path.dirname(filename)
    general, config = sqp.load_config(filename)
    for cfg in config:
        if(cfg['label'] == 'VO'):
            V, V_jack = sqp.load_f90_V_data(cfg['sqpdir'],
                                            cfg['fit_params'],
                                            cfg['t_fit'][0],
                                            cfg['t_fit'][1],
                                            x_max=cfg['x_max'],
                                            max_chisqpdf=cfg['max_chisqpdf'])

            print(np.shape(V))
            print(np.shape(V_jack))
            data_dir = os.path.join(base_dir, base_data_dir)
            filename = f"V_{cfg['label']}.dat"
            cfg['data'] = sqp.load_V_data(data_dir, filename)

            cfg['raw_data'] = (V, V_jack)
            datasets.append(cfg)

# VO_datasets = (datasets[0][2], datasets[1][2])
default_width = 6.4
default_height = 4.8
colours = ['#1b9e77', '#d95f02', '#7570b3']

fig, ax = plt.subplots(
    nrows=3,
    constrained_layout=True,
    num="single",
    figsize=[default_width, 3 * default_height],
    )

cfg1 = datasets[0]
cfg2 = datasets[1]

ma1, ma2, ratio_data = moving_average_ratio(cfg1['raw_data'], cfg2['raw_data'])

ax[0].errorbar(
    ratio_data['r_avg'],
    ratio_data['ratio'],
    ratio_data['ratio_err'],
    marker=".",
    markersize=2.5,
    elinewidth=1,
    capsize=2,
    linewidth=0,
    color=colours[2]
)
ax[1].errorbar(
    cfg1['data']['r'],
    cfg1['data']['V'],
    cfg1['data']['V_err'],
    marker=".",
    markersize=2.5,
    elinewidth=1,
    capsize=2,
    linewidth=0,
    color=colours[0],
    label=f"{cfg1['nsmear']} params"
)
ax[1].errorbar(
    cfg2['data']['r'],
    cfg2['data']['V'],
    cfg2['data']['V_err'],
    marker=".",
    markersize=2.5,
    elinewidth=1,
    capsize=2,
    linewidth=0,
    color=colours[1],
    label=f"{cfg2['nsmear']} params"
)
ax[2].errorbar(
    ma1['r_avg'],
    ma1['sigma'],
    ma1['sigma_err'],
    marker=".",
    markersize=2.5,
    elinewidth=1,
    capsize=2,
    linewidth=0,
    color=colours[0],
    label=f"{cfg1['nsmear']} params"
)
ax[2].errorbar(
    ma2['r_avg'],
    ma2['sigma'],
    ma2['sigma_err'],
    marker=".",
    markersize=2.5,
    elinewidth=1,
    capsize=2,
    linewidth=0,
    color=colours[1],
    label=f"{cfg2['nsmear']} params"
)

r_max = np.amax(ratio_data['r_avg'])
ax[0].axhline(y=1.0, xmax=r_max, color="black", ls="--")

ax[0].set_xlabel(r"$r$")
ax[0].set_ylabel(r"$\sigma$")
ax[0].set_title(f"{cfg1['nsmear']} params / {cfg2['nsmear']} params")

ax[0].set_xlabel(r"$r$")
ax[0].set_ylabel(r"$V$")
ax[1].legend()
ax[2].legend()
filename = os.path.join(out_dir, "moving_avg_ratios.pdf")
plt.savefig(filename)
print("Wrote ", filename)
plt.close("all")
print("Finished")
