"""Plot the emf results from fit_plateau.py"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import os
import sqp_helper as sqp
import importlib
from cache import load_cache

importlib.reload(sqp)


def plot_lowest_energy(
    fig,
    ax,
    fits,
    emf,
    filename,
    ax_title=None,
    ymax=None
):
    """
    Plot the effective mass and the corresponding plateau fits

    :param fig: Figure object
    :param ax: Axis object to plot on
    :param fits: Dictionary of on-axis fits
    :param emf: Dictionary of on-axis emf values
    :param filename: Name of the output file
    :param ax_title: Optional, default = None. Axis title

    """
    cmap = plt.get_cmap("jet")
    r_max = np.amax(list(fits.keys()))
    colours = cmap(np.linspace(0, 1.0, int(r_max)))
    offset = 0.05

    for i, (r, c) in enumerate(zip(emf, colours)):

        this_offset = i * offset

        if fits[r] is not None:
            t_min = fits[r]['t_min']
            t_max = fits[r]['t_max']
            ax.hlines(
                fits[r]['fit'],
                xmin=t_min + this_offset,
                xmax=t_max + this_offset,
                linewidth=1,
                color=c,
                linestyle="dashed",
            )
            err_max = fits[r]['fit'] + fits[r]['fit_err']
            err_min = fits[r]['fit'] - fits[r]['fit_err']
            t = [t_min + this_offset, t_max + this_offset]
            ax.fill_between(t, err_min, err_max, color=c, alpha=0.5)

            label = r"$r:{}, \tilde{{\chi}}^2 = {:.2f}$".format(r, fits[r]['chisqpdf'])
        else:
            label = "$r:{}$, No valid fits".format(r)

        t = emf[r]['t'].copy()
        emf_plot = emf[r]['emf'].copy()
        emf_plot_err = emf[r]['emf_err'].copy()
        # Remove points that have relative error > 0.5
        max_rel_err = 0.5
        rel_err = np.abs(emf_plot_err / emf_plot)
        mask = error_mask(rel_err, max_rel_err)
        t = t[mask]
        emf_plot = emf_plot[mask]
        emf_plot_err = emf_plot_err[mask]

        t = t + this_offset
        ax.set_xlabel(r"$t$")
        ax.set_ylabel(r"$aV(r,t)$")
        ax.errorbar(
            t,
            emf_plot,
            emf_plot_err,
            marker=".",
            markersize=2.5,
            elinewidth=1,
            capsize=2,
            linewidth=0,
            label=label,
            color=c,
        )

    ax.set_ylim(-0.1, ymax)
    # Pick an arbitrary dictionary element and get t_max from it
    t_max = np.amax(list(emf.values())[0]['t'])
    max_tick_labels = 20
    if t_max >= max_tick_labels:
        # ax.set_xticks(np.arange(1, t_max + 1, 2.0))
        ax.xaxis.set_major_locator(MultipleLocator(2))
        ax.xaxis.set_minor_locator(MultipleLocator(1))
    else:
        # ax.set_xticks(np.arange(1, t_max + 1, 1.0))
        ax.xaxis.set_major_locator(MultipleLocator(1))

    ax.legend(bbox_to_anchor=(1.05, 1), borderaxespad=0.,
              loc='upper left', prop={'size': 12})

    if ax_title is not None:
        ax.set_title(ax_title)

    if filename:
        print("Writing {}".format(filename))
        plt.savefig(filename)


def error_mask(rel_err, max_err):
    """Generate a mask from the relative error

    Mask is True up until the first value in which rel_err > max_err,
    then is False

    :param rel_err: Array of relative errors
    :param max_err: Max error value
    :returns: Boolean mask

    """

    idx = np.where(rel_err > max_err)[0]
    n_elements = len(rel_err)
    mask = np.full(n_elements, True)
    if idx.size > 0:
        idx = idx[0]
        mask[idx:] = False

    return mask


def plot_t_windows(
    axes, windows, filename, ax_title=None
):
    """Plot optimal t-windows for all r values.

    :param axes: Axes on which to plot.
    :param windows: Dictionary of fit windows for each r value
    :param filename: Name of figure to be output
    :param ax_title: (Optional, default = None) Title for the axis

    """

    r_values = np.array(list(windows.keys()))
    r_values = np.linalg.norm(r_values, axis=1)
    unique_r_values = np.unique(r_values)
    min_vals = np.array([windows[key]['t_min']
                         if windows[key] is not None else np.NaN
                         for key in windows])
    max_vals = np.array([windows[key]['t_max']
                         if windows[key] is not None else np.NaN
                         for key in windows])

    best_min = []
    worst_min = []
    best_max = []
    worst_max = []
    for r in unique_r_values:
        mask = (r_values == r)
        t_window_min_r = min_vals[mask]
        t_window_max_r = max_vals[mask]

        min_best = np.amin(t_window_min_r)
        max_best = np.amax(t_window_max_r)
        if min_best != max_best:
            best_min.append(min_best)
            best_max.append(max_best)
        else:
            best_min.append(np.nan)
            best_max.append(np.nan)

        min_worst = np.amax(t_window_min_r)
        max_worst = np.amin(t_window_max_r)
        if min_worst != max_worst:
            worst_min.append(min_worst)
            worst_max.append(max_worst)
        else:
            worst_min.append(np.nan)
            worst_max.append(np.nan)

    # Plot best windows
    r = unique_r_values
    t_min_plot = best_min
    t_max_plot = best_max
    colours = plt.rcParams["axes.prop_cycle"].by_key()["color"][0:3]
    axes[0].vlines(
        r,
        t_min_plot,
        t_max_plot,
        color=colours[0],
        linestyle="dashed",
    )
    axes[0].plot(
        r, t_min_plot, marker=".", markersize=4, color=colours[1]
    )
    axes[0].plot(
        r, t_max_plot, marker=".", markersize=4, color=colours[2]
    )

    # Plot worst windows
    t_min_plot = worst_min
    t_max_plot = worst_max

    axes[1].vlines(
        r,
        t_min_plot,
        t_max_plot,
        color=colours[0],
        linestyle="dashed",
    )
    axes[1].plot(
        r, t_min_plot, marker=".", markersize=4, color=colours[1]
    )
    axes[1].plot(
        r, t_max_plot, marker=".", markersize=4, color=colours[2]
    )

    r_max = np.ceil(np.amax(r))
    for ax in axes:
        ax.set_xticks(np.arange(1, r_max + 1, 1.0))
        ax.set_xlabel(r"$r$")
        ax.set_ylabel(r"$t$")
    if ax_title:
        axes[0].set_title(ax_title + " - Best window")
        axes[1].set_title(ax_title + " - Worst window")

    print("Writing {}".format(filename))
    plt.savefig(filename)


if __name__ == '__main__':
    # Load config info
    config = sqp.load_config("config.yml")
    # Setup output directories
    clean = config[0].get('clean', False)
    E0_dir = sqp.assign_dir("./EMF_plots/", clean=clean)
    fit_window_dir = sqp.assign_dir("./fit_window_plots/", clean=clean)
    cache_dir = sqp.assign_dir("./cache/")

    # Set font size
    plt.rcParams.update({'font.size': 13})

    DEFAULT_WIDTH = 6.4
    DEFAULT_HEIGHT = 4.8

    for i, cfg in enumerate(config):
        cfg_type = cfg["label"]
        options = cfg['options']
        emf_ymax = cfg.get('emf_ymax', None)
        t0, dt = cfg['fit_params']

        for prefer, window_method in options:
            suffix = '{}_t0{}dt{}_p{}_{}'.format(cfg_type, t0, dt,
                                                 prefer, window_method)

            fit_cache_name = f"plot_fits_{suffix}.pickle"
            fit_cache_name = os.path.join(cache_dir, fit_cache_name)
            fit_plot_data = load_cache(fit_cache_name)

            emf_cache_name = f"plot_emf_{suffix}.pickle"
            emf_cache_name = os.path.join(cache_dir, emf_cache_name)
            emf_plot_data = load_cache(emf_cache_name)

            cache_name = 'fits_{}.pickle'.format(suffix)
            cache_name = os.path.join(cache_dir, cache_name)
            fits = load_cache(cache_name)

            # Make EMF plots
            fig, axes = plt.subplots(
                sharex=False,
                constrained_layout=True,
                figsize=[1.5 * DEFAULT_WIDTH, DEFAULT_HEIGHT],
            )

            filename = "E0_{}.pdf".format(suffix)
            filename = os.path.join(E0_dir, filename)
            plot_lowest_energy(
                fig,
                axes,
                fit_plot_data,
                emf_plot_data,
                filename=filename,
                ymax=emf_ymax
            )

            # Make fit window plots
            fig, axes = plt.subplots(
                ncols=2,
                sharex=False,
                constrained_layout=True,
                figsize=[4 * DEFAULT_WIDTH, 2 * DEFAULT_HEIGHT],
            )
            filename = "fitwin_{}.pdf".format(suffix)
            filename = os.path.join(fit_window_dir, filename)
            plot_t_windows(
                axes,
                fits,
                filename=filename,
                ax_title=f"{cfg_type}"
            )
            plt.close("all")

    print("Finished")
