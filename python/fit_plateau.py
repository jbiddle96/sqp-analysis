"""
Script to fit the EMF to a plateau
Produces a plot of the on-axis plateau fits
and a plot of the optimal fit windows as a function of r
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import sqp_helper as sqp
import importlib
import itertools as it
from cache import cache, load_cache

importlib.reload(sqp)


@cache
def find_t_windows(emf_data, C_data, t_fit_min, chisqmax,
                   prefer='min', method='fixed', logfile=None):
    """Fit all the emf data to time windows.

    :param emf_data: Effective mass data at given t0, dt
    :param C_data: Covariance matrix data at given t0, dt
    :param t0: Window fit minimum
    :param chisqmax: Maximum allowable chisq/dof
    :param prefer: (Optional, default='min') Fit preference method
    :param method: (Optional, default='fixed') Window selection method
    :param logfile: (Optional, default = None) Logfile to output fit results
    :returns fits: Dictionary containing the fits for each x

    """
    emf = emf_data[emf_data["smear"] == 1]
    C = C_data[C_data["smear"] == 1]

    if logfile:
        f = open(logfile, "w")
    else:
        f = None

    if method == 'variable':
        fits = find_variable_windows(emf, C, chisqmax, t_fit_min, prefer, log=f)
    elif method == 'fixed':
        fits = find_fixed_windows(emf, C, chisqmax, t_fit_min, prefer, log=f)

    if logfile:
        f.close()

    return fits


def find_fixed_windows(emf, C, chisqmax, t_fit_min, prefer='min', log=None):
    """Find all fit optimal fit windows, independent of other windows

    :param emf: Lowest energy emf
    :param C: Covariance matrix corresponding to emf
    :param chisqmax: Max chisquared to fit
    :param t_fit_min: Minimum initial fit for on_axis points
    :param prefer: (Optional, default='min') Fit preference method
    :param log: (Optional, default=None) Opened file to write results to
    :returns fits: Dictionary of fit results
    """

    x_values = np.unique(emf[["x", "y", "z"]])
    fits = {}
    for x in x_values:
        emf_x = emf[emf[["x", "y", "z"]] == x]
        sigma_x = C[C[["x", "y", "z"]] == x]
        key = tuple(x)
        if logfile:
            log.write(f"\n x = {x}\n")

        # if x[0] >= 14:
        #     t_fit_min = 8

        fits[key] = find_fit_window(
            emf_x, sigma_x, t_fit_min, chisqmax=chisqmax,
            prefer=prefer, logfile=log
        )

    return fits


def find_variable_windows(emf, C, chisqmax, t_fit_min, prefer='min', log=None):
    """Find variable fit windows such that on-axis windows monotonically decrease

    off-axis windows are restricted to have the same t_min as their nearest lower
    on-axis point.

    e.g. if x=(3,0,0) has fit window [5, 9], then x=(3,1,1) must have t_min=5

    :param emf: Lowest energy emf
    :param C: Covariance matrix corresponding to emf
    :param chisqmax: Max chisquared to fit
    :param t_fit_min: Minimum initial fit for on_axis points
    :param prefer: (Optional, default='min') Fit preference method
    :param log: (Optional, default=None) Opened file to write results to
    :returns fits: Dictionary of fit results

    """

    # Find the on-axis fits first, enforcing monotonically decreasing bounds
    onaxis = (emf['y'] == 0) & (emf['z'] == 0)
    emf_onaxis = emf[onaxis]

    onaxis = (C['y'] == 0) & (C['z'] == 0)
    C_onaxis = C[onaxis]

    x_onaxis = np.unique(emf_onaxis[["x", "y", "z"]])
    prev_win = None
    fits = {}

    for x in x_onaxis:
        emf_x = emf_onaxis[emf_onaxis[["x", "y", "z"]] == x]
        sigma_x = C_onaxis[C_onaxis[["x", "y", "z"]] == x]
        key = tuple(x)

        if log is not None:
            log.write(f"\n x = {x}\n")

        fits[key] = find_fit_window(
            emf_x, sigma_x, t_fit_min, chisqmax=chisqmax,
            prefer=prefer, prev_win=prev_win, logfile=log
        )

        if fits[key] is not None:
            prev_win = [fits[key]['t_min'], fits[key]['t_max']]

    # Find the off-axis fits
    offaxis = (emf['y'] > 0) | (emf['z'] > 0)
    emf_offaxis = emf[offaxis]

    offaxis = (C['y'] > 0) | (C['z'] > 0)
    C_offaxis = C[offaxis]

    x_offaxis = np.unique(emf_offaxis[["x", "y", "z"]])

    for x in x_offaxis:
        emf_x = emf_offaxis[emf_offaxis[["x", "y", "z"]] == x]
        sigma_x = C_offaxis[C_offaxis[["x", "y", "z"]] == x]
        key = tuple(x)

        r = np.floor(np.sqrt(x[0] ** 2
                             + x[1] ** 2
                             + x[2] ** 2))

        # Handle the case of r overshooting the largest on-axis r
        r = int(min(r, depends['x_max']))

        # Find the nearest lower on-axis r fit
        while r > 0:
            if fits[(r, 0, 0)] is not None:
                t_min_init = fits[(r, 0, 0)]['t_min']
                break
            r -= 1
        else:
            t_min_init = t_fit_min

        if log is not None:
            log.write(f"\n x = {x}\n")

        fits[key] = find_fit_window(
            emf_x, sigma_x, t_min_init,
            chisqmax=chisqmax, max_t_min=t_min_init,
            prefer=prefer, logfile=log
        )

    return fits


def find_optimal_window(fits, logfile=None):
    """Find an optimal window that overlaps with
    as many fitted windows as possible.

    :param fits: The fit windows as created by find_t_windows
    :param logfile: (Optional, default = None) Logfile to output results to
    :returns (t_min, t_max): The optimal window

    """
    min_vals = np.array([fits[key]['t_min']
                         if fits[key] is not None else np.NaN
                         for key in fits])
    max_vals = np.array([fits[key]['t_max']
                         if fits[key] is not None else np.NaN
                         for key in fits])
    if np.isnan(min_vals).all() or np.isnan(max_vals).all():
        return None, None

    min_window_size = 3

    t_min_init = int(np.nanmin(min_vals))
    t_max_init = int(np.nanmax(max_vals))

    combinations = list(it.combinations(range(t_min_init, t_max_init + 1), 2))

    window_counts = {}
    for c in combinations:
        lb = c[0]
        ub = c[1]
        if ub - lb + 1 < min_window_size:
            continue
        window_counts[lb, ub] = 0
        for x in fits:
            if fits[x] is not None:
                if (fits[x]['t_min'] <= lb) and (fits[x]['t_max'] >= ub):
                    window_counts[lb, ub] += 1

    # Resolve ties for optimal windows
    max_value = max(window_counts.values())
    optimal_windows = [key for key in window_counts
                       if window_counts[key] == max_value]
    diffs = [ub - lb for ub, lb in optimal_windows]
    # Find the max width from the tied windows
    # Set the optimal to have the max width
    # If there are multiple tied widths,
    # this will select the window starting at the earliest time
    idx = np.argmax(diffs)
    optimal_window = optimal_windows[idx]

    if logfile is not None:
        with open(logfile, 'w+') as f:
            for (t_min, t_max) in window_counts:
                f.write(f"({t_min}, {t_max}): {window_counts[t_min, t_max]}\n")
            f.write(f'\n Optimal window: {optimal_window}')

    return optimal_window


def get_emf_plot_data(emf, fits):
    """Return EMF data ready for plotting

    :param emf: On-axis effective mass values
    :param fits: Fit dictionaries as returned by find_t_windows
    :returns (fit_results, emf_results): Dictionaries indexed by the on-axis r values.
    Each fit_results[r] is a fit as returned by find_fit_window.
    Each emf_results[r] contains 't', 'emf' and 'emf_err'.

    """

    emf_results = {}
    fit_results = {}

    # Get on-axis values
    emf_on_axis = emf[(emf['y'] == 0) & (emf['z'] == 0)]

    unique_r_values = np.unique(emf_on_axis["x"])
    for i, r in enumerate(unique_r_values):
        emf_r = emf_on_axis[(emf_on_axis["x"] == r) & (emf_on_axis["smear"] == 1)]
        fit_results[r] = fits[(r, 0, 0)]

        emf_results[r] = {}
        emf_results[r]['t'] = emf_r[emf_r["jack"] == 0]["t"]
        emf_results[r]['emf'] = emf_r[emf_r["jack"] == 0]["emf"]
        emf_results[r]['emf_err'] = emf_r[emf_r["jack"] == 0]["emf_err"]

    return fit_results, emf_results


def find_fit_window(
        emf, sigma, t_min_init, chisqmax, prefer='min',
        prev_win=None, max_t_min=None,
        find_fit_error=True, logfile=None
):
    """Fit data at fixed coordinate to a constant using the covariance matrix, sigma.

    Follow the following window selection method:
    - Set t_min = t0, t_max = t
      where t is chosen as the largest t that emf0_err(t) < emf(t).
    - Reduce t_max until chisq/dof < chisqmax is found
    - If not found, increase t0 and repeat.

    :param emf: Effective mass at a fixed x-value
    :param sigma: Corresponding covariance matrix
    :param t_min_init: Initial fitting time
    :param chisqmax: Maximum chisq/dof to accept for a fit
    :param prefer: (Optional, default='min') Toggle whether to prefer keeping
    t_min ('min') or t_max ('max') fixed when incrementing the fit window
    :param prev_win: (Optional, default=None) Previous window that the current window
    must monotonically decrease from.
    :param max_t_min: (Optional, default=None) Specify a fixed maximum for t_min
    :param find_fit_error: (Optional, default=True) Toggle whether to find the jackknife error
    :param logfile: (Optional, default = None) Logfile to output data to
    :returns result: Dictionary of fit results containing:
    t_min, t_max, fit, fit_err, chisq and chisqpdf

    """
    this_x = emf[["x", "y", "z"]][0]
    t = emf[emf["jack"] == 0]["t"]
    emf0 = emf[emf["jack"] == 0]["emf"]
    sigma0 = sigma[sigma["jack"] == 0]["C"][0]
    emf0_err = np.diagonal(sigma0)

    jack_range = sigma["jack"][1:]

    min_window_size = 3

    # Find initial t_max
    if prev_win is not None:
        t_max_init = prev_win[1]
    else:
        t_max_init = t[-1]

    iterator = zip(reversed(t[:t_max_init]),
                   reversed(emf0[:t_max_init]),
                   reversed(emf0_err[:t_max_init]))
    for jt, e, e_err in iterator:
        if abs(e_err) < 0.5 * abs(e):
            t_max_init = jt
            break
    else:
        logfile.write("Cannot find initial t_max for plateau fitting at x = {}\n".format(this_x))
        return None

    if (prev_win is not None) and (max_t_min is not None):
        print('Cannot specify previous window and max_t_min')
        sys.exit()
    elif prev_win is not None:
        max_t_min = prev_win[0]
    elif max_t_min is None:
        max_t_min = np.amax(t)

    t_min = t_min_init
    t_max = t_max_init
    # Check for invalid start ranges
    if (t_max_init <= t_min_init) or (t_max_init - t_min_init + 1 < min_window_size):
        if logfile:
            logfile.write(f"Cannot find initial range for plateau fitting at x = {this_x}\n")
        return None

    chisqpdf = chisqmax + 100  # Some number larger than the max
    while chisqpdf > chisqmax:
        t_window = t[t_min - 1: t_max]
        try:
            # Fit to the current window
            fit, chisq, chisqpdf = sqp.fit_t_window(emf0, sigma0, t_min, t_max)
        except Exception:
            # If the fitting fails, increment preferred direction and try again
            if logfile:
                # Output the chisqpdf for each window
                logfile.write("Window: {}-{}\n".format(t_window[0], t_window[-1]))
                logfile.write("Fit failed \n")
            win = increment(t_max, t_min,
                            t_max_init, t_min_init,
                            min_window_size, prefer,
                            max_t_min)
            if win is None:
                if logfile:
                    logfile.write(f"Could not find valid chisq/dof for x = {this_x}\n")
                return None
            else:
                t_min, t_max = win

            continue

        if logfile:
            # Output the chisqpdf for each window
            logfile.write("Window: {}-{}\n".format(t_window[0], t_window[-1]))
            logfile.write("Chisq/dof: {}\n".format(chisqpdf))
        if (chisqpdf > chisqmax) or (chisqpdf < 0):
            # If a suitable window hasn't been found, shrink increment and try again
            if chisqpdf < 0:
                chisqpdf = chisqmax + 100
            win = increment(t_max, t_min,
                            t_max_init, t_min_init,
                            min_window_size, prefer, max_t_min)
            if win is None:
                logfile.write("Could not find valid chisq/dof for x = {}\n".format(this_x))
                return None
            else:
                t_min, t_max = win

    # Once a suitable window has been found,
    # repeat fitting for jackknife subensembles
    if find_fit_error:
        jack_fits = []
        for j in jack_range:
            emf_jack = emf[emf["jack"] == j]["emf"]
            sigma_jack = sigma[sigma["jack"] == j]["C"][0]
            try:
                fit_results = sqp.fit_t_window(emf_jack, sigma_jack, t_min, t_max)
                fit_jack, chisq_jack, chisqpdf_jack = fit_results
            except Exception as e:
                print(f'Jackknife fitting returned exception {e}\n')
                return None

            jack_fits.append(fit_jack)

        fit_err = sqp.calc_jack_err(jack_fits)
    else:
        fit_err = 0.0

    result = {
        't_min': t_min,
        't_max': t_max,
        'fit': fit,
        'fit_err': fit_err,
        'chisq': chisq,
        'chisqpdf': chisqpdf
    }
    return result


def increment(t_max, t_min, t_max_init, t_min_init,
              min_size, prefer, max_t_min):
    """Increment the fit window as required by find_fit_window

    :param t_max: Current t max
    :param t_min: Current t min
    :param t_max_init: Initial t max
    :param t_min_init: Initial t min
    :param min_size: Minimum window size
    :param prefer: Specifies whether to prefer keeping t_max ('max')
    or t_min ('min') fixed when incrementing
    :param max_t_min: Maximum value for t_min
    :returns t_min, t_max: The new min and max.

    """

    if prefer == 'min':
        t_max -= 1
        if t_max - t_min + 1 < min_size:
            t_min += 1
            t_max = t_max_init
        if t_max_init - t_min + 1 < min_size:
            return None
        if t_min > max_t_min:
            return None
    elif prefer == 'max':
        t_min += 1
        if (t_max - t_min + 1 < min_size) or (t_min > max_t_min):
            t_max -= 1
            t_min = t_min_init
        if t_max - t_min_init + 1 < min_size:
            return None
    return t_min, t_max


# |--------------------- Execution begins here ----------------------|

if __name__ == '__main__':
    # Load config info
    config = sqp.load_config("config.yml")

    # Setup output directories
    clean = config[0].get('clean', False)
    fit_log_dir = sqp.assign_dir("./fit_window_logs/", clean=clean)
    cache_dir = sqp.assign_dir("./cache/")

    # Keys for configuration dependencies
    keys = ['emfdir', 'nsmear', 'smearValues',
            'var_params', 'x_max']
    for i, cfg in enumerate(config):
        cfg_type = cfg["label"]
        options = cfg['options']
        chisqmax = cfg.get('max_chisqpdf', np.inf)
        depends = {key: cfg[key] for key in keys}

        all_optimal_windows = {p: {} for p, w in options}
        for t0, dt in cfg["var_params"]:

            # Load emf
            cache_name = f'emf_{cfg["label"]}_t0{t0}dt{dt}.pickle'
            cache_name = os.path.join(cache_dir, cache_name)
            source = sqp.find_files(cfg['emfdir'], 'emf*.dat')
            emf = sqp.load_EMF_data(cfg, (t0, dt))
            t_max = np.amax(emf["t"])

            # Load covariance matrix
            cache_name = f'C_{cfg["label"]}_t0{t0}dt{dt}.pickle'
            cache_name = os.path.join(cache_dir, cache_name)
            source = sqp.find_files(cfg['emfdir'], 'C*.dat')
            try:
                C = load_cache(cache_name,
                               depends=depends,
                               source=source)
            except (AssertionError, OSError) as e:
                print(f'Cache failed to load due to the following exception: {e}')
                print('Loading from source.')
                C = sqp.load_C_data(cfg, (t0, dt), t_max,
                                    cache_name=cache_name,
                                    cache_meta=depends)

            depends['max_chisqpdf'] = chisqmax

            for prefer, window_method in options:

                # Get all window fits
                suffix = '{}_t0{}dt{}_p{}_{}'.format(cfg_type, t0, dt,
                                                     prefer, window_method)
                cache_name = 'fits_{}.pickle'.format(suffix)
                cache_name = os.path.join(cache_dir, cache_name)
                logfile = "fit_{}.log".format(suffix)
                logfile = os.path.join(fit_log_dir, logfile)
                t_fit_min = cfg.get('window_min', t0 + 2)
                fits = find_t_windows(emf, C, t_fit_min,
                                      chisqmax, logfile=logfile,
                                      prefer=prefer,
                                      method=window_method,
                                      cache_name=cache_name,
                                      cache_meta=depends)

                # If using fixed windows, find the optimal window
                if window_method == 'fixed':
                    logfile = "windows_{}.log".format(suffix)
                    logfile = os.path.join(fit_log_dir, logfile)
                    t_min, t_max = find_optimal_window(fits, logfile)
                    all_optimal_windows[prefer][t0, dt] = t_min, t_max

                # Get EMF plot data (plotting done with plot_emf.py)
                fit_plot_data, emf_plot_data = get_emf_plot_data(emf, fits)

                fit_cache_name = f"plot_fits_{suffix}.pickle"
                fit_cache_name = os.path.join(cache_dir, fit_cache_name)
                cache(fit_plot_data, cache_name=fit_cache_name)

                emf_cache_name = f"plot_emf_{suffix}.pickle"
                emf_cache_name = os.path.join(cache_dir, emf_cache_name)
                cache(emf_plot_data, cache_name=emf_cache_name)

        for prefer in all_optimal_windows:
            # Save the optimal windows for later use
            filename = "optimal_windows_{}_p{}.pickle".format(cfg_type, prefer)
            filename = os.path.join(cache_dir, filename)
            cache(all_optimal_windows[prefer], filename, cache_meta=depends)

    print("Finished")
    ##
