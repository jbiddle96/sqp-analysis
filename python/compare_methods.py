"""
Plot the effective mass to compare the effectiveness of
variational analysis
OLD, NEEDS UPDATING
"""

import sqp_helper as sqp
import numpy as np
import matplotlib.pyplot as plt
import importlib
import os
importlib.reload(sqp)


def load_data(datafile):
    """
    Load non-variational data
    """
    print("Reading", datafile)
    data = np.loadtxt(datafile, dtype=[('x', int),
                                       ('y', int),
                                       ('z', int),
                                       ('t', int),
                                       ('emf', float),
                                       ('emf_err', float)])

    tp = np.dtype([('r', float), ('t', int),
                   ('emf', float), ('emf_err', float)])

    # Split data into on and off axis
    onaxis = (data['y'] == 0) & (data['z'] == 0)
    points_on_axis = np.count_nonzero(onaxis)
    on_axis_data = np.zeros(points_on_axis, dtype=tp)
    on_axis_data['r'] = data['x'][onaxis]
    on_axis_data['t'] = data['t'][onaxis]
    on_axis_data['emf'] = data['emf'][onaxis]
    on_axis_data['emf_err'] = data['emf_err'][onaxis]
    t_max = np.amax(on_axis_data['t'])
    on_axis_data = on_axis_data[on_axis_data['t'] != t_max]

    offaxis = (data['y'] > 0) | (data['z'] > 0)
    points_off_axis = np.count_nonzero(offaxis)
    off_axis_data = np.zeros(points_off_axis, dtype=tp)
    off_axis_data['r'] = np.sqrt(data['x'][offaxis]**2
                                 + data['y'][offaxis]**2
                                 + data['z'][offaxis]**2)
    off_axis_data['t'] = data['t'][offaxis]
    off_axis_data['emf'] = data['emf'][offaxis]
    off_axis_data['emf_err'] = data['emf_err'][offaxis]
    t_max = np.amax(off_axis_data['t'])
    off_axis_data = off_axis_data[off_axis_data['t'] != t_max]

    data_dict = {"OnAxis": on_axis_data,
                 "OffAxis": off_axis_data}

    return data_dict


# Load config info
general, UT_cfg, VO_cfg, VR_cfg = sqp.load_config('config.yml')
ncon = 200

# Load data
fname_UT_novar = "/g/data/e31/jb3708/data/StaticQuarkPotential/UT/32x64_PG/SQP-nape10/wilson.dat"
fname_VR_novar = "/g/data/e31/jb3708/data/StaticQuarkPotential/UT-MCG-VR-Rand/32x64_PG/SQP-nape10/wilson.dat"
fname_VO_novar = "/g/data/e31/jb3708/data/VarStaticQuarkPotential/UT-MCG-VO/32x64_PG"

emf_data_UT_var = sqp.load_EMF_data(UT_cfg['dir'], UT_cfg['fit_params'],
                                    UT_cfg['ncon'])[UT_cfg['fit_params']]['OnAxis']
emf_data_UT_novar = load_data(fname_UT_novar)['OnAxis']

emf_data_VR_var = sqp.load_EMF_data(VR_cfg['dir'], VR_cfg['fit_params'],
                                    VR_cfg['ncon'])[VR_cfg['fit_params']]['OnAxis']
emf_data_VR_novar = load_data(fname_VR_novar)['OnAxis']

emf_data_VO_var = sqp.load_EMF_data(VO_cfg['dir'], VO_cfg['fit_params'],
                                    VO_cfg['ncon'])[VO_cfg['fit_params']]['OnAxis']
emf_data_VO_novar = sqp.load_EMF_data(fname_VO_novar, (2, 2),
                                      VO_cfg['ncon'])[(2, 2)]['OnAxis']

emf_data_UT_var = emf_data_UT_var[emf_data_UT_var['jack'] == 0]
emf_data_UT_var = emf_data_UT_var[emf_data_UT_var['smear'] == 1]
emf_data_VR_var = emf_data_VR_var[emf_data_VR_var['jack'] == 0]
emf_data_VR_var = emf_data_VR_var[emf_data_VR_var['smear'] == 1]
emf_data_VO_var = emf_data_VO_var[emf_data_VO_var['jack'] == 0]
emf_data_VO_var = emf_data_VO_var[emf_data_VO_var['smear'] == 1]

emf_data_VO_novar = emf_data_VO_novar[emf_data_VO_novar['jack'] == 0]
emf_data_VO_novar = emf_data_VO_novar[emf_data_VO_novar['smear'] == 1]
emf_data_VO_novar.dtype.names = ('r', 'y', 'z', 't', 'smear',
                                 'jack', 'emf', 'emf_err')

r_max = int(np.amax(emf_data_UT_novar['r']))
r_range = range(1, r_max + 1)

default_width = 6.4
default_height = 4.8
out_dir = "comparison_plots"


def make_plot(data_var, data_novar, filename):
    fig, axes = plt.subplots(nrows=2, ncols=4, constrained_layout=True,
                             figsize=[3 * default_width, default_height])

    for ax, r in zip(axes.flatten(), r_range):
        emf_var_r = data_var[data_var['x'] == r]
        t = emf_var_r['t']
        emf = emf_var_r['emf']
        emf_err = emf_var_r['emf_err']
        ax.errorbar(t, emf, emf_err,
                    marker=".", markersize=2.5,
                    elinewidth=1, capsize=2,
                    linewidth=0, label="Variation")

        emf_novar_r = data_novar[data_novar['r'] == r]
        t = emf_novar_r['t']
        emf = emf_novar_r['emf']
        emf_err = emf_novar_r['emf_err']
        ax.errorbar(t, emf, emf_err,
                    marker=".", markersize=2.5,
                    elinewidth=1, capsize=2,
                    linewidth=0, label="No Variation")

        ax.set_title(f'{r = }'.format(r))

    axes.flatten()[0].legend()
    filename = os.path.join(out_dir, filename)
    plt.savefig(filename)


make_plot(emf_data_UT_var, emf_data_UT_novar, "UT_comparison.pdf")
make_plot(emf_data_VR_var, emf_data_VR_novar, "VR_comparison.pdf")
make_plot(emf_data_VO_var, emf_data_VO_novar, "VO_comparison.pdf")
plt.close('all')
