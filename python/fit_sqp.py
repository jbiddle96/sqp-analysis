import os
import sys
import numpy as np
from scipy.optimize import curve_fit
import importlib
from tabulate import tabulate
from cache import cache, load_cache
import sqp_helper as sqp
import fit_functions as ff

importlib.reload(sqp)


def fit_sqp(fit_func_key, V, V_jack, cfg, logfile=None):
    """Perform a fit of V to the specified function
    Returns a dictionary of fits (and jackknife errors) to the function parameters,
    as well as additional parameters if possible.

    :param fit_func_key: Function key to fit to
    :param V: The potential data
    :param V_jack: The 1st order jackknife ensemble
    :param cfg: Configuration metadata
    :param logfile: (Optional, default = None) Logfile to output a human-readable summary to
    :returns report, plot_data: Dictionary of the parameter fits
    and an array containing the fit function evaluated using the fitted parameters.

    """
    print(f'Fitting function {fit_func_key} to {cfg["label"]} data')
    # Set the fit function
    fit_func = ff.set_fit_function(fit_func_key)
    if 'fixed_params' in cfg:
        fixed_params = cfg['fixed_params'].get(fit_func_key, {})
        for p, val in fixed_params.items():
            print(f'Fixing parameter {p} to value {val}')
            fit_func.fix_parameter(p, val)

    param_names = fit_func.parameters('free')
    # Treat a parameter labelled 'R' as referring to a radius within the range
    if 'R' in param_names:
        print("""
        Parameter with label 'R' present.
        Interpreting as a distance and setting bounds accordingly.
        """)
        r_min = np.amin(V['r'])
        r_max = np.amax(V['r'])
        fit_func.set_bounds([r_min, r_max], param='R')

    # Find the optimal fit range that meets the chisq criteria
    fit_report = find_fit_range(V, fit_func, cfg)
    fit_params = fit_report['fit_params']
    n_points = fit_report['n_points']
    r_min = fit_report['r_min']
    r_max = fit_report['r_max']
    chisqpdf = fit_report['chisqpdf']
    ignore_errors = cfg.get('ignore_errors', False)

    # Fit the jackknife ensembles to obtain the error
    n_jack = len(V_jack)
    tp = np.dtype(
        [
            ('r', float, (n_points)),
            ('V', float, (n_points)),
            ('V_err', float, (n_points)),
        ]
    )
    V_jack_fit = np.zeros(n_jack, dtype=tp)
    # Cut the fitting region down to the selected region
    for V_jf, V_j in zip(V_jack_fit, V_jack):
        mask = (V_j['r'] >= r_min) & (V_j['r'] <= r_max)
        V_jf['r'] = V_j['r'][mask]
        V_jf['V'] = V_j['V'][mask]
        V_jf['V_err'] = V_j['V_err'][mask]

    jack_mean = []
    for ij in range(n_jack):
        if ignore_errors:
            # If ignoring errors, assum 5% error
            err = 1.05 * V_jack_fit[ij]['V']
            popt, pcov = curve_fit(
                fit_func,
                V_jack_fit[ij]['r'],
                V_jack_fit[ij]['V'],
                p0=fit_params,
                bounds=fit_func.bounds,
                sigma=err,
                absolute_sigma=True
            )

        else:
            popt, pcov = curve_fit(
                fit_func,
                V_jack_fit[ij]['r'],
                V_jack_fit[ij]['V'],
                p0=fit_params,
                bounds=fit_func.bounds,
                sigma=V_jack_fit[ij]['V_err'],
                absolute_sigma=True
            )
        jack_mean.append(popt)

    jack_mean = np.array(jack_mean)

    # Analyse the jackknife results to get plot error bounds
    plot_min = np.amin(V['r']) if cfg.get('plot_min') is None else cfg.get('plot_min')
    plot_max = np.amax(V['r']) if cfg.get('plot_max') is None else cfg.get('plot_max')

    r_plot = np.linspace(plot_min, plot_max, num=100)
    samples = np.array([fit_func(r_plot, *sub) for sub in jack_mean])

    # Use the parameter means in case of non-linear ansatz
    mean = fit_func(r_plot, *fit_params)
    dev = np.array([sqp.calc_jack_err(samples[:, i])
                    for i in range(len(r_plot))])
    upper = mean + dev
    lower = mean - dev
    plot_values = np.array([r_plot, mean, upper, lower]).T

    # Make the final report
    report = {'fitted':
              {'fit_func': fit_func_key,
               'chisqpdf': chisqpdf,
               'r_min': r_min,
               'r_max': r_max}
              }

    report = analyse_parameters(jack_mean, report, fit_params, param_names)

    # Add in the fixed parameters
    fixed_params = fit_func.fixed_parameters()
    param_names = fit_func.parameters()
    for p, val in fixed_params.items():
        report['fitted'][p] = val
        report['fitted'][p + "_err"] = 0

    # Output fit parameters to a human-readable logfile
    if logfile:
        save_params(logfile, report, param_names)

    return report, plot_values


def save_params(logfile, report, param_names):
    """Save parameters to a formatted log file

    :param logfile: Filename to save to
    :param report: Report of fitted parameters
    :param param_names: Names of parameters to save

    """
    fit_func_key = report['fitted']['fit_func']
    r_min = report['fitted']['r_min']
    r_max = report['fitted']['r_max']
    chisqpdf = report['fitted']['chisqpdf']

    additional_params = ('additional' in report)
    fit_table = []
    add_table = []
    headers = ['Parameter', 'Fit value', 'Jackknife error']
    for p in param_names:
        fit_table.append([p, report['fitted'][p], report['fitted'][p + '_err']])

    if additional_params:
        for p in report['additional']:
            if not p.endswith('_err'):
                add_table.append([p,
                                  report['additional'][p],
                                  report['additional'][p + '_err']])
    with open(logfile, 'a+') as f:
        f.write(f'Function: {fit_func_key} \n')
        if chisqpdf > cfg.get('max_chisqpdf', np.inf):
            f.write('MAX CHISQ/DOF EXCEEDED \n')
            f.write('MINIMUM CHISQ/DOF FIT RANGE RETURNED INSTEAD\n')
        f.write('Chisq_pdf = {}\n'.format(chisqpdf))
        f.write(f'[r_min, r_max] = [{r_min:.2f}, {r_max:.2f}] \n')
        f.write(tabulate(fit_table, headers=headers))
        if additional_params:
            f.write('\n\n')
            f.write('Additional parameters \n')
            f.write(tabulate(add_table, headers=headers))
        f.write('\n\n')


def find_fit_range(V, fit_function, cfg):
    """Finds the fit range with which to fit the potential data

    :param V: Data to fit
    :param fit_function: FitFunction object
    :param cfg: Configuration metadata
    :returns report: A dictionary containing the parameter fits,
    the range used for fitting, the number of points included in the fit and the chisq/dof

    """
    r_max_init = np.amax(V['r']) if cfg.get('fit_max') is None else cfg.get('fit_max')
    r_max_init = min(r_max_init, np.amax(V['r']))

    r_min_init = np.amin(V['r']) if cfg.get('fit_min') is None else cfg.get('fit_min')
    r_min_init = max(r_min_init, np.amin(V['r']))

    ignore_errors = cfg.get('ignore_errors', False)
    chisqmax = cfg.get('max_chisqpdf', np.inf)

    r_step = 0.1
    r_max = r_max_init
    r_min = r_min_init
    min_width = 3

    print(f'Using max chisq = {chisqmax}')
    print(f'Fitting over initial range {r_min}, {r_max}')
    if ignore_errors:
        print('Ignoring errors')

    report_list = []
    while True:
        V_fit = V[(V['r'] >= r_min) & (V['r'] <= r_max)]
        n_points = len(V_fit)
        dof = fit_function.get_dof(n_points)
        if dof > 1:
            try:
                # Fit the mean values
                if ignore_errors:
                    err = 1.1 * V_fit['V']
                else:
                    err = V_fit['V_err']

                fit_params, pcov = curve_fit(
                    fit_function, V_fit['r'], V_fit['V'],
                    p0=fit_function.p0,
                    bounds=fit_function.bounds,
                    sigma=err,
                    absolute_sigma=True)
                chisqpdf = fit_function.get_chisqpdf(V_fit['r'],
                                                     V_fit['V'],
                                                     *fit_params,
                                                     y_err=err
                                                     )

                report = {'fit_params': np.array(fit_params),
                          'r_min': r_min,
                          'r_max': r_max,
                          'chisqpdf': chisqpdf,
                          'n_points': n_points}
                report_list.append(report)
            except RuntimeError as e:
                print(e)
                chisqpdf = 100 + chisqmax
        else:
            chisqpdf = 100 + chisqmax
        # If desired chisquared is reached, break
        if chisqpdf <= chisqmax:
            print('------')
            print(f'Found fit range with chisq/dof = {chisqpdf}')
            print(f'Using fit range {r_min} - {r_max}')
            print('------\n')
            return report
        # Else decrease r_max
        else:
            r_max -= r_step

        # If the difference between r_min and r_max is less than min_width,
        # reset r_max and increase r_min
        if r_max - r_min <= min_width:
            r_max = r_max_init
            r_min += r_step

        # If r_min is larger than the initial r_max,
        # abort and return the smallest chisq report that was found
        if r_min + min_width >= r_max_init:
            chisq_list = [report['chisqpdf'] for report in report_list]
            min_idx = np.argmin(chisq_list)
            min_chisq = chisq_list[min_idx]
            report = report_list[min_idx]
            print('------')
            print(f"Fit range with chisq/dof < {chisqmax} could not be found")
            print(f"Returning fit with minimum chisq/dof of {min_chisq}")
            print('------ \n')
            return report


def analyse_parameters(jack_samples, report, fit_params, param_names):
    """Analyse the jackknife results and caculate parameter estimates

    :param jack_samples: Array of jackknife ensemble values for each parameter
    :param report: Dictionary in which to place the parameter calculations
    :param fit_params: The mean values of the parameters.
    This is deliberately separate from the jackknife samples in case of non-linear
    functional forms
    :param param_names: Names of the fitted parameters
    :returns report: The updated report

    """
    param_err = sqp.calc_jack_err(jack_samples)

    for i, p in enumerate(param_names):
        report['fitted'][p] = fit_params[i]
        report['fitted'][p + "_err"] = param_err[i]

    # Check if the given parameters contain alpha and sigma
    calculate_quantities = all(x in report['fitted'] for x in ['sigma', 'alpha'])
    if calculate_quantities:

        # Return immediately if sigma < 0
        if report['fitted']['sigma'] < 0:
            return report

        sigma_idx = param_names.index('sigma')
        alpha_idx = param_names.index('alpha')
        sigma_jack = jack_samples[:, sigma_idx]
        alpha_jack = jack_samples[:, alpha_idx]

        alpha = report['fitted']['alpha']
        sigma = report['fitted']['sigma']
        r0 = np.sqrt((1.65 - alpha) / sigma)
        r1 = np.sqrt((1.0 - alpha) / sigma)
        a0 = 0.49 / r0
        a1 = 0.35 / r1
        a_sigma = (np.sqrt(sigma) / 0.440) * 0.1973

        r0_jack = np.sqrt((1.65 - alpha_jack) / sigma_jack)
        r0_err = sqp.calc_jack_err(r0_jack)

        r1_jack = np.sqrt((1.0 - alpha_jack) / sigma_jack)
        r1_err = sqp.calc_jack_err(r1_jack)

        a0_jack = 0.49 / r0_jack
        a0_err = sqp.calc_jack_err(a0_jack)

        a1_jack = 0.35 / r1_jack
        a1_err = sqp.calc_jack_err(a1_jack)

        a_sigma_jack = (np.sqrt(sigma_jack) / 0.440) * 0.1973
        a_sigma_err = sqp.calc_jack_err(a_sigma_jack)

        additional_vars = ['r0', 'r1', 'a0', 'a1', 'a_sigma']

        report['additional'] = {}
        for key in additional_vars:
            report['additional'][key] = eval(key)
            report['additional'][key + '_err'] = eval(key + '_err')

    return report


def get_windows(r_min, r_max, width, step):
    """Generate the window edges needed for the local slope

    :param r_min: Start window value
    :param r_max: Maximum end value
    :param width: Width of each window
    :param step: Increment between windows
    :returns: List of tuples containing the lower and upper bounds of each window

    """
    n_windows = int(np.floor((r_max - r_min - width) / step)) + 1
    lb = [r_min + i * step for i in range(n_windows)]
    ub = [r_min + width + i * step for i in range(n_windows)]
    return list(zip(lb, ub))


def sqp_moving_average_width(V, V_jack, cfg,
                             window_width=3.0,
                             window_increment=0.2,
                             subtract_coulomb=False,
                             V0=None,
                             alpha=None):
    """Fit all windows of width window_width starting from r_init to a linear function.

    This gives a moving estimate of sigma
    as r increases. Windows with less than 10 data points within them are ignored.

    If a cornell fit has been done, can subtract off
    the coulomb term to give a better linear estimate

    :param V: Potential data to fit
    :param V_jack: 1st order jackknife data
    :param cfg: Configuration metadata
    :param window_width: (default 3.0)
    Width of the sliding window the linear fits are performed over. Given in lattice units
    :param window_increment: (default 0.2) How far the window slides each increment
    :param subtract_coulomb: (default True) Toggle whether to subtract a Coulomb term.
    If so, must supply V0 and alpha.
    :param V0: V0 of the Coulomb term to be subtracted
    :param alpha: alpha of the Coulomb term to be subtracted
    :returns moving_average: Dictionary of moving average data

    """
    V_fit = V.copy()
    V_fit_jack = V_jack.copy()
    fit_func = ff.FitFunction(ff.f_linear, ['sigma', 'b'])

    r_init = cfg.get('moving_avg_init', 5.0)

    if subtract_coulomb:
        V_fit['V'] = V['V'] - ff.f_coulomb(V['r'], V0, alpha)
        for V_j in V_fit_jack:
            V_j['V'] = V_j['V'] - ff.f_coulomb(V_j['r'], V0, alpha)

    # Determine the number of windows
    r_max = np.amax(V['r'])
    bounds = get_windows(r_init, r_max, window_width, window_increment)
    if not bounds:
        print('Maximum r value is less than window minimum, skipping')
        return None

    r_avg_list = []
    sigma_list = []
    sigma_jack = []
    chisq_list = []
    min_points = 10

    print(f'window fitting has range {r_init} - {r_max}')
    # Loop through every window
    for r_start, r_end in bounds:
        mask = (V_fit['r'] >= r_start) & (V_fit['r'] <= r_end)
        r_window = V_fit['r'][mask]
        r_avg = r_start + window_width / 2.0
        r_avg_list.append(r_avg)

        # If the window contains too few points, skip it
        if(len(r_window) < min_points):
            sigma_list.append(np.nan)
            sigma_jack.append(np.zeros(len(V_fit_jack)))
            chisq_list.append(np.nan)
            continue

        # Fit to the window
        V_window = V_fit['V'][mask]
        V_err_window = V_fit['V_err'][mask]
        fit, pcov = curve_fit(
            fit_func,
            r_window,
            V_window,
            p0=fit_func.p0,
            sigma=V_err_window,
            absolute_sigma=True
        )
        sigma_list.append(fit[0])
        # Track the chisq values
        chisqpdf = fit_func.get_chisqpdf(r_window,
                                         V_window,
                                         *fit,
                                         y_err=V_err_window)
        chisq_list.append(chisqpdf)

        # Fit to the jackknife ensembles
        sigma_jack.append([])
        for V_j in V_fit_jack:
            V_jack_window = V_j['V'][mask]
            V_jack_err_window = V_j['V_err'][mask]
            fit, pcov = curve_fit(
                fit_func,
                r_window,
                V_jack_window,
                p0=fit_func.p0,
                sigma=V_jack_err_window,
                absolute_sigma=True,
            )
            sigma_jack[-1].append(fit[0])

    # Calculate the jackknife error from the jackknife averages
    moving_average = {}
    moving_average['r_avg'] = np.array(r_avg_list)
    moving_average['sigma'] = np.array(sigma_list)
    moving_average['sigma_err'] = sqp.calc_jack_err(sigma_jack, axis=1)
    moving_average['chisqpdf'] = np.array(chisq_list)

    return moving_average


def output_V_data(data, directory, out_name):
    """Write the potential data to an output file"""
    path = os.path.join(directory, out_name)
    array = np.array([data['r'], data['V'], data['V_err']]).T
    with open(path, 'w') as f:
        np.savetxt(f, array)


def output_fit_data(data, directory, out_name):
    """Write fit function with error bounds to an output file"""
    path = os.path.join(directory, out_name)
    with open(path, 'w+') as f:
        np.savetxt(f, data)


def output_sqp_data(data, directory, out_name):
    """Write the sqp moving average data to an output file"""
    path = os.path.join(directory, out_name)
    array = np.array([data['r_avg'],
                      data['sigma'],
                      data['sigma_err'],
                      data['chisqpdf']]).T
    with open(path, 'w+') as f:
        np.savetxt(f, array)


def parse_windows(fit_data):
    """Parse the fit windows to be used for loading f90 fit data

    This is done by returning a dictionary with elements x: (t_min, t_max).
    If fit_data[x] is None, omit it from the returned windows.

    :param fit_data: Fits as generated by fit_plateau
    :returns windows: Dictionary of fit windows as described above

    """

    windows = {}
    for x in fit_data:
        if fit_data[x] is not None:
            t_min = fit_data[x]['t_min']
            t_max = fit_data[x]['t_max']
            windows[x] = (t_min, t_max)
    return windows


# |--------------------- Execution starts here ----------------------|

if __name__ == '__main__':
    # Load config info
    config = sqp.load_config('config.yml')

    # Setup directories
    clean = config[0].get('clean', False)
    data_dir = sqp.assign_dir('./fitted_points', clean=clean)
    log_dir = sqp.assign_dir('./parameter_estimates', clean=clean)
    cache_dir = sqp.assign_dir("./cache")

    for cfg in config:
        print("Fitting {} configurations".format(cfg['label']))
        options = cfg['options']
        for prefer, window_method in options:
            print(f"Using options {prefer}, {window_method}")
            t0, dt = cfg['fit_params']
            suffix = '{}_t0{}dt{}_p{}_{}'.format(cfg['label'], t0, dt,
                                                 prefer, window_method)

            if window_method == 'fixed':
                # Use supplied t window if possible
                window = cfg.get('t_fit')
                if window is None:
                    # Otherwise load the optimal window
                    prefer = cfg.get('prefer', 'min')
                    filename = "optimal_windows_{}_p{}.pickle".format(cfg['label'], prefer)
                    filename = os.path.join(cache_dir, filename)
                    try:
                        windows = load_cache(filename)
                        window = windows[t0, dt]
                        if window == (None, None):
                            raise ValueError(f'No optimal window was found for t0={t0}, dt={dt}')
                    except (AssertionError, OSError) as e:
                        print(f'Cache failed to load due to the following exception: {e}')
                        print('No window was supplied, exiting')
                        sys.exit()
                else:
                    window = tuple(window)

            elif window_method == 'variable':
                # If using a variable window, load the window fits
                fit_cache_name = 'fits_{}.pickle'.format(suffix)
                fit_cache_name = os.path.join(cache_dir, fit_cache_name)
                try:
                    fit_data = load_cache(fit_cache_name)
                except (AssertionError, OSError) as e:
                    print(f'Cache failed to load due to the following exception: {e}')
                    print('Invalid cache for loading variable fit data, exiting')
                    sys.exit()
                window = parse_windows(fit_data)

            # Load the potential data
            max_chisqpdf = cfg.get('max_chisqpdf', None)
            V, V_jack = sqp.load_f90_V_data(cfg['sqpdir'],
                                            (t0, dt),
                                            window,
                                            x_max=cfg['x_max'],
                                            chisqmax=max_chisqpdf)
            if len(V) == 0:
                print('No data was loaded, skipping current options')
                continue

            # Perform fitting
            fit_params = {}
            logfile = os.path.join(log_dir, f"sqp_fit_{suffix}.log")
            if os.path.exists(logfile):
                os.remove(logfile)
            for fit_func in cfg['fit_func']:
                fit_params[fit_func], plot_values = fit_sqp(fit_func,
                                                            V,
                                                            V_jack,
                                                            cfg,
                                                            logfile=logfile)

                # Save the fitted function parameters as a pickle
                report_name = os.path.join(cache_dir, f"sqp_fit_{suffix}_{fit_func}.pickle")
                cache(fit_params[fit_func], report_name)

                # Save the numerical fit function data
                filename = f"V_{suffix}_{fit_func}.dat"
                output_fit_data(plot_values, data_dir, filename)

            moving_avg = sqp_moving_average_width(V, V_jack, cfg)

            # Output the potential and moving average points for plotting
            filename = f"V_{suffix}.dat"
            output_V_data(V, data_dir, filename)
            if moving_avg is not None:
                filename = f"sqp_{suffix}.dat"
                output_sqp_data(moving_avg, data_dir, filename)
            print()

    print('Finished')
