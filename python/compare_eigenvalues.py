"""Compare effective mass values at different energy levels.
Also compare the eigenvalues used to obtain the energy levels
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import sqp_helper as sqp
import importlib
import itertools as it
from cache import cache, load_cache


def compare_emf(emf_data, x):
    """Compare effective masses of different energy levels

    :param emf_data: Full on-axis effective mass data
    :returns t, [e1, ...], [e1_err, ...]:
    The time axis, a list of the energy levels
    and a list of their associated errors

    """
    emf = emf_data[emf_data['jack'] == 0]
    emf = emf[emf['x'] == x]
    t = np.unique(emf['t'])
    energy_levels = np.unique(emf['smear'])

    energies = []
    energies_err = []
    for e in energy_levels:
        emf_e = emf[emf['smear'] == e]
        energies.append(emf_e['emf'])
        energies_err.append(emf_e['emf_err'])

    return t, energies, energies_err


def plot_emf(fig, ax, t, energies, energies_err, filename):

    for i, (e, e_err) in enumerate(zip(energies, energies_err)):
        label = f'E = {i}'
        ax.errorbar(
            t,
            e,
            e_err,
            marker=".",
            markersize=2.5,
            elinewidth=1,
            capsize=2,
            linewidth=0,
            label=label,
        )

    plt.legend()
    plt.savefig(filename)
    print(f'Saved {filename}')


if __name__ == '__main__':
    # Load config info
    config = sqp.load_config("config.yml")

    # Setup output directories
    clean = config[0].get('clean', False)

    eval_dir = sqp.assign_dir("./eval_comparison/", clean=clean)
    emf_dir = sqp.assign_dir("./emf_energy_comparison/", clean=clean)

    DEFAULT_WIDTH = 6.4
    DEFAULT_HEIGHT = 4.8

    for i, cfg in enumerate(config):
        cfg_type = cfg["label"]
        chisqmax = cfg.get('max_chisqpdf', np.inf)

        for t0, dt in cfg["var_params"]:
            # Load emf
            emf = sqp.load_EMF_data(cfg, (t0, dt), split_on_off=True)
            emf = emf['OnAxis']
            x_values = np.unique(emf['x'])
            for x in x_values:
                suffix = '{}_t0{}dt{}_x{}'.format(cfg_type, t0, dt, x)
                t, energies, energies_err = compare_emf(emf, x)

                # Plot
                fig, ax = plt.subplots(
                    constrained_layout=True,
                    figsize=[1.5 * DEFAULT_WIDTH, DEFAULT_HEIGHT],
                )

                filename = os.path.join(emf_dir, f'emf_comparison_{suffix}.pdf')
                plot_emf(fig, ax, t, energies, energies_err, filename)
                plt.close(fig)
