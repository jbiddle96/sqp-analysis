import pickle
import os
from numpy.testing import assert_equal


def cache(func_or_data, cache_name=None, cache_meta=None):
    """Cache either a variable or a function's return value (by using as a decorator)
    as a pickle named 'cache_name'. Can also store cache metadata
    to be used later to check the cache is up-to-date

    Data should be loaded via a call to load_cache

    Note that kwargs cache_name and cache_meta
    are popped when this function is used as a decorator.

    :param func_or_data: Data to cache or
    function to call and then cache the return value
    :param cache_name: Name of the cache file
    :param cache_meta: Dictionary of metadata to be used
    to check whether the cache is up-to-date.

    """

    if callable(func_or_data):
        def cache_wrapper(*args, **kwargs):
            cache_name = kwargs.pop('cache_name', None)
            cache_meta = kwargs.pop('cache_meta', {})
            result = func_or_data(*args, **kwargs)
            if cache_name is not None:
                with open(cache_name, 'wb') as handle:
                    store = {'meta': cache_meta,
                             'cache': result}
                    pickle.dump(store, handle)
            return result
        return cache_wrapper
    else:
        if cache_name is not None:
            with open(cache_name, 'wb') as handle:
                store = {'meta': cache_meta,
                         'cache': func_or_data}
                pickle.dump(store, handle)


def load_cache(filename, depends=None, source=None):
    """Load cached data as stored by the cache function.
    Functionality to check whether the cache is up to date is
    proiovided throught 'depends' and 'source'

    :param filename: The cache file to load
    :param depends: (Optional, default = None) Dictionary of dependency variables.
    Must have keys that match with those found in the cache metadata,
    but may also contain additional keys.
    Will raise an AssertionError if dependencies are not met
    :param source: Scalar or list of file sources.
    Will raise an OSError if any dependency is newer than the cache
    :returns cached_data: The cached data

    """

    print(f'Loading {filename}')
    # Check if cache is newer than sources
    if source is not None:
        if not isinstance(source, list):
            source = [source]

        cache_mtime = os.path.getmtime(filename)
        source_mtimes = [os.path.getmtime(s) for s in source]

        tdiff = [cache_mtime - m for m in source_mtimes]
        if any([t < 0 for t in tdiff]):
            raise OSError('Cache is older than 1 or more source files')

    # Load the cache
    with open(filename, 'rb') as handle:
        result = pickle.load(handle)

    # Check if the cache metadata matches the provided metadata
    metadata = result['meta']
    if depends is not None:
        sub_depends = {key: depends.get(key, None) for key in metadata}
        assert_equal(metadata, sub_depends)

    cached_data = result['cache']
    return cached_data
