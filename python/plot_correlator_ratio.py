import numpy as np
import matplotlib.pyplot as plt
import os
import sqp_helper as sqp
import importlib

importlib.reload(sqp)


def plot_correlator_ratio(data, filename):
    """Plot the ratio of correlator off-diagonal components to the diagonal.

    :param data: Ratio data
    :param filename: Output plot filename

    """

    unique_r = np.unique(data["x"])
    fig, axes = plt.subplots(constrained_layout=True)
    offset = 0.05
    max_t = np.amax(data['t'])
    for i, r in enumerate(unique_r):
        this_data = data[(data['x'] == r)
                         & (data['y'] == 0)
                         & (data['z'] == 0)]
        t = this_data['t'] + i * offset
        y = this_data['ratio']

        axes.plot(t,
                  y,
                  marker=".",
                  markersize=2.5,
                  linewidth=0,
                  label=f"r = {r}"
                  )

    axes.set_xlabel(r'$t$')
    axes.set_ylabel(r'$\bar{C}_{\text{offdiag}}/\bar{C}_{\text{diag}}$')
    axes.legend(bbox_to_anchor=(1.05, 1), borderaxespad=0.,
                loc='upper left')
    axes.set_ylim(bottom=-0.1)
    axes.axhline(0, 0, max_t + 1, linestyle='--', c='black', alpha=0.5)
    axes.axhline(1, 0, max_t + 1, linestyle='--', c='black', alpha=0.5)
    plt.savefig(filename)
    plt.close('all')


def plot_correlator_components(data, cfg, filename, error=False):
    """Plot the correlator ratio components as a matrix of plots.

    :param data: Correlator data
    :param cfg: Configuration metadata
    :param filename: Output plot filename

    """
    unique_r = np.unique(data["x"])
    nsmear = cfg['nsmear']
    smear_sweeps = cfg['smearValues']
    max_t = np.amax(data['t'])

    default_width = 6.4
    default_height = 4.8
    size = [default_width * nsmear / 2, default_height * nsmear / 2]
    fig, axes = plt.subplots(constrained_layout=True,
                             nrows=nsmear, ncols=nsmear,
                             figsize=size)
    cmap = plt.get_cmap("jet")
    colours = cmap(np.linspace(0, 1.0, len(unique_r)))

    for ax in axes[-1, :]:
        ax.set_xlabel(r'$t$')
        # axes.set_ylabel(r'$C_{{{}{}}}$'.format(comp[0] + 1, comp[1] + 1))

    offset = 0.05
    for i in range(nsmear):
        for j in range(nsmear):
            comp = (i, j)

            for k, (r, c) in enumerate(zip(unique_r, colours)):
                this_data = data[(data['x'] == r)
                                 & (data['y'] == 0)
                                 & (data['z'] == 0)]
                t = this_data['t'] + k * offset
                y = this_data['g'][:, comp[0], comp[1]]
                if error:
                    y_err = this_data['g_err'][:, comp[0], comp[1]]
                    axes[i, j].errorbar(t,
                                        y,
                                        y_err,
                                        marker=".",
                                        markersize=2.5,
                                        elinewidth=1,
                                        capsize=2,
                                        linewidth=0,
                                        color=c,
                                        label=f"r = {r}"
                                        )
                else:
                    axes[i, j].plot(t,
                                    y,
                                    marker=".",
                                    markersize=2.5,
                                    linewidth=0,
                                    color=c,
                                    label=f"r = {r}"
                                    )
            title = "{}-{} sweeps".format(smear_sweeps[i], smear_sweeps[j])
            # print(i, j)
            axes[i, j].set_title(title)

    axes[-1, -1].legend(bbox_to_anchor=(1.05, 1), borderaxespad=0.,
                        loc='upper left')
    for ax in axes.flatten():
        ax.set_ylim(bottom=-0.1, top=2)
        ax.axhline(0, 0, max_t + 1, linestyle='--', c='black', alpha=0.5)
        ax.axhline(1, 0, max_t + 1, linestyle='--', c='black', alpha=0.5)

    plt.savefig(filename)
    plt.close('all')


if __name__ == '__main__':
    config = sqp.load_config("config.yml")

    clean = config[0].get('clean', False)
    plot_dir = sqp.assign_dir("./correlator_ratio_plots/", clean=clean)
    error = False
    for cfg in config:
        ratio_data = sqp.load_correlator_ratio(cfg)
        corr_data = sqp.load_correlator_components(cfg, error=error)
        filename = f'{cfg["label"]}_correlator_ratio.pdf'
        filename = os.path.join(plot_dir, filename)
        plot_correlator_ratio(ratio_data, filename)

        filename = '{}_correlator_components.pdf'.format(cfg['label'])
        filename = os.path.join(plot_dir, filename)
        plot_correlator_components(corr_data, cfg, filename, error=error)
