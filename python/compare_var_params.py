"""
Produce a comparison of different choices of (t0, dt) at a specified r value
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
from cache import load_cache, cache
import sqp_helper as sqp
import importlib

importlib.reload(sqp)


def make_plot(cfg, plot_data, plot_name, title=None):
    """Make the plot of the EMF as a function of variational parameters.
    Multiple data series corresponding to different on-axis r-values are plotted

    :param cfg: Configuration metadata
    :param plot_data: Data to be plotted
    :param plot_name: Output filename
    """

    fig, ax = plt.subplots()
    r_vals = set()
    for t0, dt in cfg['var_params']:
        this_r = plot_data[t0, dt]['r']
        r_vals = r_vals.union(set(this_r))

    r_vals = np.array(list(r_vals))
    r_vals = r_vals[r_vals > 0]
    t0_vals = np.unique(np.array(cfg['var_params'])[:, 0])
    t0_max = np.amax(t0_vals)
    t0_min = np.amin(t0_vals)
    dt_vals = np.unique(np.array(cfg['var_params'])[:, 1])
    dt_max = np.amax(dt_vals)
    dt_min = np.amin(dt_vals)

    cmap = plt.get_cmap("jet")
    colours = cmap(np.linspace(0, 1.0, len(r_vals)))

    default_width = 6.4
    default_height = 4.8

    fig, ax = plt.subplots(constrained_layout=True,
                           figsize=[2 * default_width, default_height])
    x_axis = range((t0_max - t0_min + 1) * (dt_max - dt_min + 1))

    ymax = -np.inf
    data_series = {}
    for c, r in zip(colours, r_vals):
        data_series[r] = {'V': [], 'V_err': []}
        for t0, dt in cfg['var_params']:
            V = plot_data[t0, dt][plot_data[t0, dt]['r'] == r]['V']
            V_err = plot_data[t0, dt][plot_data[t0, dt]['r'] == r]['V_err']

            if V.size != 0:
                data_series[r]['V'].append(V[0])
                data_series[r]['V_err'].append(V_err[0])
            else:
                data_series[r]['V'].append(np.NaN)
                data_series[r]['V_err'].append(np.NaN)
        label = f'r = {r}'

        # Keep track of max values
        ymax_new = np.nanmax(data_series[r]['V'])
        if ymax_new > ymax:
            ymax = ymax_new

        plt.errorbar(x_axis,
                     data_series[r]['V'],
                     data_series[r]['V_err'],
                     marker=".",
                     markersize=2.5,
                     elinewidth=1,
                     capsize=2,
                     linewidth=0,
                     label=label,
                     color=c)

    major_ticks = [n for n in x_axis]
    ax.set_xticks(major_ticks)
    ax.set_xticklabels([str(v) for v in cfg['var_params']])
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", va='center',
             rotation_mode="anchor")
    ax.legend(bbox_to_anchor=(1.05, 1), borderaxespad=0.,
              loc='upper left')

    ax.set_xlabel('Variational parameters')
    ax.set_ylabel('V')
    ymax *= 1.1
    ymin = 0
    ax.set_ylim(ymin, ymax)
    if title is not None:
        ax.set_title(title)
    plt.savefig(plot_name)
    print(f'Saved figure {plot_name}')


def load_variable_data(filename, depends):
    """Load variable fit window data from fit cache at a specific t0, dt

    Cache is produced by fit_plateau.py

    :param filename: Name of the cached data
    :param depends: Dependencies to verify cache validity
    :returns V: Plotting data

    """

    try:
        fit_data = load_cache(filename, depends=depends)
    except (AssertionError, OSError) as e:
        print(f'Cache failed to load due to the following exception: {e}')
        print('Invalid cache for loading variable fit data, exiting')
        sys.exit()
    x_full = np.array(list(fit_data.keys()))
    x_onaxis = x_full[(x_full[:, 1] == 0) & (x_full[:, 2] == 0)]
    n_points = len(x_onaxis)
    tp = np.dtype(
        [
            ("r", float),
            ("V", float),
            ("V_err", float),
        ]
    )
    V = np.zeros(n_points, dtype=tp)
    for i, x in enumerate(x_onaxis):
        this_entry = fit_data[tuple(x)]
        if this_entry is not None:
            V[i]['r'] = np.sqrt(x[0] ** 2
                                +x[1] ** 2
                                +x[2]** 2)
            V[i]['V'] = this_entry['fit']
            V[i]['V_err'] = this_entry['fit_err']

    return V


if __name__ == '__main__':
    # Load config info
    config = sqp.load_config('config.yml')

    keys = ['emfdir', 'sqpdir', 'nsmear', 'smearValues',
            'var_params', 'x_max', 'max_chisqpdf']

    clean = config[0].get('clean', False)
    plot_dir = sqp.assign_dir('var_comparison_plots', clean=clean)
    cache_dir = sqp.assign_dir("./cache/")

    for cfg in config:
        cfg_type = cfg['label']
        options = cfg['options']
        chisqmax = cfg.get('max_chisqpdf', np.inf)
        depends = {key: cfg[key] for key in keys}

        print(f'Analysing {cfg_type} configurations')

        for prefer, window_method in options:
            print(f'Options: {prefer}, {window_method}')
            plot_data = {}
            if window_method == 'variable':
                # For variable fits, load fit results from fit_plateau.py
                for t0, dt in cfg['var_params']:
                    suffix = '{}_t0{}dt{}_p{}_{}'.format(cfg_type, t0, dt,
                                                         prefer, window_method)
                    fit_cache_name = 'fits_{}.pickle'.format(suffix)
                    fit_cache_name = os.path.join(cache_dir, fit_cache_name)
                    plot_data[t0, dt] = load_variable_data(fit_cache_name, depends)

            elif window_method == 'fixed':
                # Load the optimal window for each t0, dt pair
                filename = "optimal_windows_{}_p{}.pickle".format(cfg_type, prefer)
                filename = os.path.join(cache_dir, filename)
                windows = load_cache(filename)
                for t0, dt in cfg['var_params']:
                    print(f'Using window: {windows[t0,dt]}')
                    V, V_jack = sqp.load_f90_V_data(cfg['sqpdir'],
                                                    (t0, dt),
                                                    windows[t0, dt],
                                                    x_max=cfg['x_max'],
                                                    chisqmax=chisqmax,
                                                    onaxis=True)
                    plot_data[t0, dt] = V

            # Make plots
            plot_name = f'var_comparison_{cfg["label"]}_p{prefer}_{window_method}.pdf'
            plot_name = os.path.join(plot_dir, plot_name)
            title = f'prefer: {prefer}, windows: {window_method}'
            make_plot(cfg, plot_data, plot_name)
            plt.close('all')
            print()

    print('Finished')
