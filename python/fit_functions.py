"""Module containing all fit functions and utilities used for SQP fitting."""
import numpy as np
from inspect import signature


class FitFunction:
    def __init__(self, func,
                 param_names,
                 p0=None,
                 bounds=None):
        self._func = func
        # Use curve_fit default if p0 not provided
        # Essential to allow __call__ to work with curve_fit,
        # as otherwise the number of arguments would be hidden
        if p0 is None:
            sig = signature(func)
            self.p0 = np.ones(len(sig.parameters) - 1)
        else:
            self.p0 = p0
        self.p0 = list(self.p0)

        nparams = len(self.p0)
        if bounds is None:
            self.bounds = (nparams * [-np.inf],
                           nparams * [np.inf])
        else:
            self.bounds = bounds

        self._param_names = param_names
        self._free_args = self._param_names.copy()
        self._fixed_args = {}

        self._saved_bounds = {}
        self._saved_p0 = {}

    def __call__(self, x, *args):
        # If there are no fixed args, call the function as usual
        if not self._fixed_args:
            return self._func(x, *args)

        # Else assemble the complete arguments from the provided and fixed values
        full_args = self.all_values(*args)
        return self._func(x, *full_args)

    def get_chisqpdf(self, x, y_exact, *args, y_err=1.0):
        """Calculate the chisq/dof from the data, y_exact
        """

        dof = self.get_dof(len(x))
        res = y_exact - self.__call__(x, *args)
        chisqpdf = sum((res / y_err) ** 2) / dof
        return chisqpdf

    def get_dof(self, n_points):
        """Determine the degrees of freedom for this function

        :param n_points: Number of data points
        :returns: Degrees of freedom

        """
        dof = n_points - len(self._free_args)
        return dof

    def set_bounds(self, bounds, param=None):
        """Set bounds for fitting

        :param bounds: 2-tuple of lists for all free parameters
        as required by curve fit.
        If param is supplied, should be iterable of two values specifying the min and max
        for the specified param.
        :param param: (default=None) Parameter to set bounds for.

        """
        if param is None:
            if np.shape(bounds) != self.bounds:
                raise ValueError(f"""Incorrect shape.
                Expected: {np.shape(self.bounds)}"
                Received: {np.shape(bounds)}""")

            self.bounds = bounds.copy()
            return

        idx = self._free_args.index(param)
        self.bounds[0][idx] = bounds[0]
        self.bounds[1][idx] = bounds[1]

    def fix_parameter(self, param, value):
        """Fix a specified parameter to a given value for all subsequent calls
        """
        if param not in self._param_names:
            raise ValueError("Parameter not in parameter name list.")
        if param in self._fixed_args:
            print(f"Parameter {param} already fixed")
            return

        idx = self._free_args.index(param)

        self._fixed_args[param] = value
        self._free_args.remove(param)

        # Pop and save bounds and initial conditions for later re-insertion
        self._saved_bounds[param] = (self.bounds[0].pop(idx),
                                     self.bounds[1].pop(idx))

        self._saved_p0[param] = self.p0.pop(idx)

    def free_parameter(self, param):
        """Free a previously fixed parameter
        """

        if param not in self._param_names:
            raise ValueError("Parameter not in parameter name list.")
        if param not in self._fixed_args:
            print("Parameter not fixed, returning.")
            return

        self._fixed_args.pop(param)
        # Keep the ordering of the free args same as param_names
        self._free_args = [p for p in self._param_names
                           if ((p in self._free_args) or (p == param))]

        idx = self._free_args.index(param)

        # Re-insert bounds and initial conditions
        self.bounds[0].insert(idx, self._saved_bounds[param][0])
        self.bounds[1].insert(idx, self._saved_bounds[param][1])
        self.p0.insert(idx, self._saved_p0[param])

    def parameters(self, group: str = 'all') -> list:
        if group == 'all':
            return self._param_names
        elif group == 'fixed':
            return list(self._fixed_args.keys())
        elif group == 'free':
            return self._free_args
        else:
            raise ValueError(f'Unrecognised group {group}')

    def fixed_parameters(self):
        return self._fixed_args

    def all_values(self, *args):
        """Generate the full set of parameters from only the free parameters

        Ordering is based on the ordering of param_names.

        :returns: List of arguments including fixed parameters

        """
        full_args = []
        i = 0
        for p in self._param_names:
            if p in self._fixed_args:
                full_args.append(self._fixed_args[p])
            else:
                full_args.append(args[i])
                i += 1
        return full_args


def set_fit_function(key):
    """Set the fit function based on a string key.

    :param key: Fit function string key
    :returns fit_func: FitFunction instance containing the specified fit function

    """
    if key == "linear":
        param_names = ["sigma", "V0"]
        fit_func = FitFunction(f_linear, param_names)
    elif key == "cornell":
        param_names = ["V0", "alpha", "sigma"]
        fit_func = FitFunction(f_cornell, param_names)
    elif key == "coulomb":
        param_names = ["V0", "alpha"]
        fit_func = FitFunction(f_coulomb, param_names)
    elif key == "screenedcornell":
        param_names = ["V0", "alpha", "sigma", "m"]
        fit_func = FitFunction(f_screenedcornell, param_names)
    elif key == "fixedcoulomb":
        param_names = ["V0", "sigma"]
        fit_func = FitFunction(f_fixedcoulomb, param_names)
    elif key == "step-coulomb":
        param_names = ["V0", "alpha", "R"]
        r_init = 5.0
        p0 = [1.0, 1.0, r_init]
        bounds = ([-np.inf, -np.inf, 0],
                  [np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_stepcoulomb, param_names,
                               p0=p0, bounds=bounds)
    elif key == "step-coulomb-linear":
        param_names = ["V0", "alpha", "sigma", "R"]
        r_init = 5.0
        p0 = [1.0, 1.0, 1.0, r_init]
        bounds = ([-np.inf, -np.inf, -np.inf, 0],
                  [np.inf, np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_stepcoulomblinear, param_names,
                               p0=p0, bounds=bounds)
    elif key == "running-coulomb":
        param_names = ["V0", "alpha", 'f']
        fit_func = FitFunction(f_runningcoulomb, param_names)
    elif key == "running-coulomb-linear":
        param_names = ["V0", "alpha", 'f', 'sigma']
        fit_func = FitFunction(f_runningcoulomblinear, param_names)
    elif key == 'exp-coulomb':
        param_names = ["V0", "alpha", "rho"]
        bounds = ([-np.inf, 0, 0],
                  [np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_expcoulomb, param_names, bounds=bounds)
    elif key == 'exp-coulomb-linear':
        param_names = ["V0", "alpha", "sigma", "rho"]
        bounds = ([-np.inf, 0, 0, 0],
                  [np.inf, np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_expcoulomblinear, param_names, bounds=bounds)
    elif key == 'formfactor':
        param_names = ["V0", "alpha", "rho"]
        bounds = ([-np.inf, 0, -np.inf],
                  [np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_formfactor, param_names, bounds=bounds)
    elif key == 'formfactor-linear':
        param_names = ["V0", "alpha", "rho", "sigma"]
        bounds = ([-np.inf, 0, -np.inf, 0],
                  [np.inf, np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_formfactorlinear, param_names, bounds=bounds)
    elif key == 'dipole':
        param_names = ["V0", "alpha", "R", "n"]
        bounds = ([-np.inf, 0, 0, 0],
                  [np.inf, np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_dipole, param_names, bounds=bounds)
    elif key == 'dipole-linear':
        param_names = ["V0", "alpha", "R", "n", "sigma"]
        bounds = ([-np.inf, 0, 0, 0, 0],
                  [np.inf, np.inf, np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_dipolelinear, param_names, bounds=bounds)
    elif key == 'logistic':
        param_names = ["V0", "alpha", "rho", "R"]
        bounds = ([-np.inf, 0, 0, 1],
                  [np.inf, np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_logistic, param_names, bounds=bounds)

    elif key == 'logistic-linear':
        param_names = ["V0", "alpha", "rho", "R", "sigma"]
        bounds = ([-np.inf, 0, 0, 1, 0],
                  [np.inf, np.inf, np.inf, np.inf, np.inf])
        fit_func = FitFunction(f_logisticlinear, param_names, bounds=bounds)
    else:
        raise ValueError(f"Unrecognised fit function {key}, exiting.")

    return fit_func


def f_const(x, const):
    """
    Function to return a constant.
    Used for plateau fitting.
    """
    return const


def f_linear(x, sigma, b):
    """General linear function with slope sigma and y intercept b"""
    return sigma * x + b


def f_cornell(x, V0, alpha, sigma):
    """Coulomb + linear ansatz"""
    return V0 - alpha / x + sigma * x


def f_coulomb(x, V0, alpha):
    """Coulombic function"""
    return V0 - alpha / x


def f_screenedcornell(x, V0, alpha, sigma, m):
    """Screened Cornell potential ansatz"""
    return V0 + (sigma / m) * (1 - np.exp(-m * x)) - (alpha * m + (alpha / x) * np.exp(-m * x))


def f_fixedcoulomb(x, V0, sigma):
    """Coulomb + linear ansatz with a fixed coulomb term"""
    alpha = 0.323273
    return V0 - alpha / x + sigma * x


def f_runningcoulomb(x, V0, alpha, f):
    """Coulomb function modified to accomodate for running of the Coulomb term"""
    return V0 - (alpha / x) * (1 + f * x) ** -1


def f_runningcoulomblinear(x, V0, alpha, f, sigma):
    """Coulomb function modified to accomodate for running of the Coulomb term.

    Also includes a linear term
    """
    return V0 - (alpha / x) * (1 + f * x) ** -1 + sigma * x


def f_stepcoulomb(x, V0, alpha, r_lim):
    """Coulomb + flat potential with meeting point between regions fixed"""
    return (V0 - np.heaviside(r_lim - x, 0.5) * (alpha / x)
            - np.heaviside(x - r_lim, 0.5) * (alpha / r_lim))


def f_stepcoulomblinear(x, V0, alpha, sigma, r_lim):
    """Coulomb + flat potential with meeting point between regions fixed.

    Also includes a linear term
    """
    return (V0 + sigma * x - np.heaviside(r_lim - x, 0.5) * (alpha / x)
            - np.heaviside(x - r_lim, 0.5) * (alpha / r_lim))


def f_expcoulomb(x, V0, alpha, rho):
    """Short-distance regulated coulomb-like potential."""
    return V0 - alpha * (1 - np.exp(-x * rho)) ** (-1)


def f_expcoulomblinear(x, V0, alpha, sigma, rho):
    """Short-distance regulated coulomb-like potential with linear term."""
    return V0 - alpha * (1 - np.exp(-x * rho)) ** (-1) + sigma * x


def f_formfactor(x, V0, alpha, rho):
    """Coulomb potential with form-factor-like regulation."""
    return V0 - alpha / x * np.exp(-x * rho)


def f_formfactorlinear(x, V0, alpha, rho, sigma):
    """Coulomb potential with form-factor-like regulation.
    Includes linear term.

    """
    return V0 - alpha / x * np.exp(-x * rho) + sigma * x


def f_dipole(x, V0, alpha, R, n):
    """Coulomb potential with dipole-like regulation."""
    return V0 - alpha / x * (1 / (1 + (x / R) ** n))


def f_dipolelinear(x, V0, alpha, R, n, sigma):
    """Coulomb potential with dipole-like regulation.
    Includes linear term.

    """
    reg = (1 / (1 + (x / R) ** n))
    return V0 - alpha / x * reg + sigma * x


def f_logistic(x, V0, alpha, rho, r_lim):
    """Coulomb potential with logistic regulation."""
    return V0 - alpha / x * (1 / (1 + np.exp(-2 * rho * (r_lim - x))))


def f_logisticlinear(x, V0, alpha, rho, r_lim, sigma):
    """Coulomb potential with logistic regulation."""
    return V0 - alpha / x * (1 / (1 + np.exp(-2 * rho * (r_lim - x)))) + sigma * x
