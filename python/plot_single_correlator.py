import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import sqp_helper as sqp
import importlib

importlib.reload(sqp)


def plot_correlator(data, cfg, filename, log=False, max_t=None, ylabel=None):
    """Plot the correlator components as a matrix of plots.

    :param data: Correlator data
    :param cfg: Configuration metadata
    :param filename: Output plot filename

    """
    nsmear = cfg['nsmear']
    smear_sweeps = cfg['smearValues']

    default_width = 6.4
    default_height = 4.8
    size = [default_width * nsmear / 2, default_height * nsmear / 2]
    fig, axes = plt.subplots(constrained_layout=True,
                             nrows=nsmear, ncols=nsmear,
                             figsize=size)
    cmap = plt.get_cmap("jet")

    for ax in axes[-1, :]:
        ax.set_xlabel(r'$t$')

    if ylabel is not None:
        for ax in axes[:, 0]:
            ax.set_ylabel(ylabel)

    offset = 0.05
    ymin = np.inf
    ymax = -np.inf
    for i in range(nsmear):
        for j in range(nsmear):
            unique_r = np.unique(data[i, j]["x"])
            if max_t is None:
                max_t = np.amax(data[i, j]['t'])
            colours = cmap(np.linspace(0, 1.0, len(unique_r)))
            for k, (r, c) in enumerate(zip(unique_r, colours)):
                this_data = data[i, j]
                this_data = this_data[(this_data['x'] == r)
                                      & (this_data['y'] == 0)
                                      & (this_data['z'] == 0)]
                this_data = this_data[this_data['t'] <= max_t]
                t = this_data['t'] + k * offset
                y = this_data['emf']
                y_err = this_data['emf_err']

                if log:
                    y_err = y_err / y
                    y = np.log(np.abs(y))

                # Keep track of min and max values
                if np.amax(y) > ymax:
                    ymax = np.amax(y)
                if np.amin(y) < ymin:
                    ymin = np.amin(y)

                axes[i, j].errorbar(t,
                                    y,
                                    y_err,
                                    marker=".",
                                    markersize=2.5,
                                    elinewidth=1,
                                    capsize=2,
                                    linewidth=0,
                                    color=c,
                                    label=f"r = {r}"
                                    )
            title = "{}-{} sw".format(smear_sweeps[i], smear_sweeps[j])
            # print(i, j)
            axes[i, j].set_title(title)

    # axes[0, -1].legend(bbox_to_anchor=(1.05, 1), borderaxespad=0.,
    #                     loc='upper left')
    handles, labels = axes[0, -1].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper right',
               bbox_transform=axes[0, -1].transAxes,
               bbox_to_anchor=(1.4, 1.0))
    # fig.subplots_adjust(right=0.9)
    for ax in axes.flatten():
        ax.set_ylim(bottom=ymin - 0.15 * abs(ymin), top=ymax + 0.15 * abs(ymax))
        # ax.axhline(0, 0, max_t + 1, linestyle='--', c='black', alpha=0.5)
        # ax.axhline(1, 0, max_t + 1, linestyle='--', c='black', alpha=0.5)

    fig.savefig(filename, bbox_inches='tight')
    plt.close('all')


def compare_symmetry(data, cfg, base_filename, max_t=None,
                     log=False, type='onaxis'):
    """Plot the symmetric correlator components to ensure symmetry

    :param data: Correlator data
    :param cfg: Configuration info
    :param base_filename: Base output filename.
    Final filenames have the radius under condsideration inserted
    :param max_t: (Optional, default=None) Max time value to plot
    :param log: (Optional, default=False) Toggle whether to take the log of the correlator
    :param type: (Optional, default='onaxis') Choose whether to analyse on or off-axis points,
    or make a plot of the difference between on-axis symmetric values.
    Off-axis points at a given r are within +-0.5 of the specified r value.

    """

    nsmear = cfg['nsmear']
    smear_sweeps = cfg['smearValues']
    comb = [(i, j) for i in range(nsmear) for j in range(i + 1, nsmear)]

    default_width = 6.4
    default_height = 4.8
    size = [default_width, default_height * nsmear / 2]

    unique_r = np.unique(data[0, 0]["x"])

    ymin = np.inf
    ymax = -np.inf
    figures = []
    for r in unique_r:
        fig, axes = plt.subplots(constrained_layout=True,
                                 nrows=len(comb), figsize=size)
        if len(comb) == 1:
            axes = [axes]
        axes[0].set_title(f'r = {r}')
        for ax, (i, j) in zip(axes, comb):
            if max_t is None:
                max_t = np.amax(data[i, j]['t'])

            data1 = data[j, i]
            data1 = data1[data1['t'] <= max_t]
            data2 = data[i, j]
            data2 = data2[data2['t'] <= max_t]
            labels = [(smear_sweeps[j], smear_sweeps[i]),
                      (smear_sweeps[i], smear_sweeps[j])]
            if type == 'diff':
                new_ymin, new_ymax = make_diff_plot(ax, data1, data2, r,
                                                    labels, log)
            else:
                new_ymin, new_ymax = make_sym_plot(ax, data1, data2, r,
                                                   labels, type, log)

            if new_ymax > ymax:
                ymax = new_ymax
            if new_ymin < ymin:
                ymin = new_ymin

            if type == 'diff':
                ax.legend(title='(Source smear, Sink smear)')
            else:
                ax.legend()

        figures.append((fig, axes))

    # Save the plots
    for r, (fig, axes) in zip(unique_r, figures):
        for ax in axes:
            ax.set_ylim(bottom=ymin - 0.1 * abs(ymin),
                        top=ymax + 0.1 * abs(ymax))
        base, ext = os.path.splitext(base_filename)
        if type == 'onaxis':
            filename = base + f'_r{r}_onaxis'
        elif type == 'offaxis':
            filename = base + f'_r{r}_offaxis'
        elif type == 'diff':
            filename = base + f'_r{r}_diff'
        if log:
            filename += '_log'
        filename += ext
        fig.savefig(filename)
        base, ext = os.path.splitext(base_filename)

    plt.close('all')


def make_diff_plot(ax, data1, data2, r,  labels, log):
    if len(data1) != len(data2):
        raise ValueError('Cannot take difference, data size mismatch')
    data_diff = np.zeros(len(data1), data1.dtype)
    data_diff[['x', 'y', 'z', 't']] = data1[['x', 'y', 'z', 't']]
    if log:
        data_diff['emf'] = np.log(np.abs(data1['emf'])) - np.log(np.abs(data2['emf']))
        data_diff['emf_err'] = np.sqrt((data1['emf_err'] / data1['emf']) ** 2
                                       + (data2['emf_err'] / data2['emf']) ** 2)
    else:
        data_diff['emf'] = data1['emf'] - data2['emf']
        data_diff['emf_err'] = np.sqrt(data1['emf_err'] ** 2
                                       + data2['emf_err'] ** 2)
    data_diff = data_diff[(data_diff['x'] == r)
                          & (data_diff['y'] == 0)
                          & (data_diff['z'] == 0)]

    t = data_diff['t']
    y = data_diff['emf']
    y_err = data_diff['emf_err']

    # Keep track of min and max values
    if len(y) > 0:
        ymax = np.amax(y)
        ymin = np.amin(y)
    else:
        ymax = -np.inf
        ymin = np.inf

    ax.errorbar(t,
                y,
                y_err,
                marker=".",
                markersize=2.5,
                elinewidth=1,
                capsize=2,
                linewidth=0,
                label="${} - {}$".format(labels[0], labels[1])
                )
    ax.set_xlabel('$t$')
    if log:
        ax.set_ylabel('$\\ln(G_1(t))-ln(G_2(t))$')
    else:
        ax.set_ylabel('$G_1(t)-G_2(t)$')
    return ymin, ymax


def make_sym_plot(ax, data1, data2, r, labels, type, log):
    for d, l in zip([data1, data2], labels):
        if type == 'onaxis':
            d = d[(d['x'] == r)
                  & (d['y'] == 0)
                  & (d['z'] == 0)]
        elif type == 'offaxis':
            mask = (np.sqrt(d['x'] ** 2 + d['y'] ** 2 + d['z'] ** 2) >= r - 0.5) \
                & (np.sqrt(d['x'] ** 2 + d['y'] ** 2 + d['z'] ** 2) <= r + 0.5) \
                & (d['y'] > 0) & (d['z'] > 0)
            d = d[mask]

        t = d['t']
        y = d['emf']
        y_err = d['emf_err']
        if log:
            y_err = y_err / y
            y = np.log(np.abs(y))

        # Keep track of min and max values
        if len(y) > 0:
            ymax = np.amax(y)
            ymin = np.amin(y)
        else:
            ymax = -np.inf
            ymin = np.inf

        ax.errorbar(t,
                    y,
                    y_err,
                    marker=".",
                    markersize=2.5,
                    elinewidth=1,
                    capsize=2,
                    linewidth=0,
                    label="{}-{} sweeps".format(l[0], l[1])
                    )
        ax.set_xlabel('$t$')
        if log:
            ax.set_ylabel('$\\ln(G(t))$')
        else:
            ax.set_ylabel('$G(t)$')
    return ymin, ymax


# |--------------------- Execution begins here ----------------------|

if __name__ == '__main__':
    # Load config info
    config = sqp.load_config("config.yml")

    # mpl.rcParams.update({'font.size': 16})

    clean = config[0].get('clean', False)
    single_dir = "./single_correlator_plots/"
    sqp.assign_dir(single_dir, clean=clean)

    DEFAULT_WIDTH = 6.4
    DEFAULT_HEIGHT = 4.8
    for i, cfg in enumerate(config):
        emf = sqp.load_single_data(cfg, split_on_off=False)
        corr = sqp.load_single_data(cfg, datatype='corr', split_on_off=False)

        filename = "{}_single_corr.pdf".format(cfg['label'])
        filename = os.path.join(single_dir, filename)
        plot_correlator(corr, cfg, filename, log=True, ylabel='$\\ln(G(t))$')

        filename = "{}_single_emf.pdf".format(cfg['label'])
        filename = os.path.join(single_dir, filename)
        plot_correlator(emf, cfg, filename, ylabel='$\\ln\\left(G(t)/G(t+1)\\right)$')

        base_filename = "{}_symmetry.pdf".format(cfg['label'])
        base_filename = os.path.join(single_dir, base_filename)
        compare_symmetry(corr, cfg, base_filename, log=True)
        compare_symmetry(corr, cfg, base_filename, log=True, type='offaxis')
        compare_symmetry(corr, cfg, base_filename, log=True, type='diff')

        compare_symmetry(corr, cfg, base_filename)
        compare_symmetry(corr, cfg, base_filename, type='offaxis')
        compare_symmetry(corr, cfg, base_filename, type='diff')
