import numpy as np
import sqp_helper as sqp
import os
import yaml
import importlib
import pandas as pd
import itertools as it
from cache import load_cache
from collections import OrderedDict
importlib.reload(sqp)


def rreplace(s, old, new, occurrence):
    li = s.rsplit(old, occurrence)
    return new.join(li)


def get_dataframe(config_info, drop_columns=None):
    """Get a dataframe of all fit results

    :param config_info: List of ensemble info dictionaries
    :param drop_columns: Columns to remove
    :returns: Datframe of the results

    """
    fit_list = []
    data_dir = "cache/"
    for ensemble in config_info:
        directory = config_info[ensemble]['directory']
        filename = os.path.join(directory, "config.yml")
        config = sqp.load_config(filename)
        for cfg in config:
            data_file = os.path.join(directory, data_dir)
            fit_params = load_fit_params(data_file, cfg, method)
            for fit_func, params in fit_params.items():
                row = params.copy()

                row['ensemble'] = ensemble
                row['ensemble_label'] = config_info[ensemble]['label']
                row['type'] = cfg['label']
                row['fit_func'] = fit_func
                fit_list.append(row)

    series_list = []
    for row in fit_list:
        series_list.append(pd.Series(row))

    df = pd.concat(series_list, axis=1).T
    # Format the fit range to be a tuple of same-length numbers
    format_range(df)

    # Reorder columns
    df.insert(0, 'fit_func', df.pop('fit_func'))
    df.insert(0, 'range', df.pop('range'))
    df.insert(0, 'type', df.pop('type'))
    df.insert(0, 'ensemble_label', df.pop('ensemble_label'))
    df.insert(0, 'ensemble', df.pop('ensemble'))

    if drop_columns is not None:
        for col in drop_columns:
            df = df.drop(col, axis=1)

    return df


def get_plot_df(df, config_info):
    """Cut df to just contain the plot function data

    :param df: Dataframe of all fit function results
    :param config_info: Table configuration
    :returns: Reduced dataframe containing only the plot function data

    """
    df_pf = df.copy()
    for ensemble in config_info:
        directory = config_info[ensemble]['directory']
        filename = os.path.join(directory, "config.yml")
        config = sqp.load_config(filename)
        for cfg in config:
            plot_func = cfg['plot_func']
            df_pf = df_pf.drop(df_pf[(df_pf.ensemble == ensemble)
                                     & (df_pf.type == cfg['label'])
                                     & (df_pf.fit_func != plot_func)].index)

    # Drop columns that are all NaN
    df_pf = df_pf.dropna(axis=1, how='all')
    return df_pf


def format_range(df):
    # Convert r_min and r_max columns to strings
    df['r_min'] = df['r_min'].apply(lambda s: f'{s:.2f}')
    df['r_max'] = df['r_max'].apply(lambda s: f'{s:.2f}')

    # Pad columns to be same width
    for col in ['r_min', 'r_max']:
        max_len = df[col].str.len().max()
        df[col] = df[col].apply(pad, args=(max_len, r'\phantom{0}'))

    df['range'] = list(zip(df.r_min, df.r_max))
    df.drop(['r_min', 'r_max'], axis=1, inplace=True)


def pad(str: str, length: int, pad_char: str = '0') -> str:
    """Pad a string up to length with pad character pad_char

    :param str: Input string
    :param length: Length to pad string to
    :param pad_char: Padding string to insert
    :returns: Padded string

    """
    str_len = len(str)
    if str_len >= length:
        return str

    pad_len = length - str_len
    str = pad_char * pad_len + str
    return str


def convert_sigma(row, a):
    sigma = row['sigma']
    sigma_err = row['sigma_err']
    sigma_phys, sigma_phys_err = sqp.convert_param(sigma,
                                                   sigma_err,
                                                   'sigma',
                                                   a)
    return sigma_phys, sigma_phys_err


def load_fit_params(directory: str, cfg: dict, method: str):
    """Load the fit parameters"""
    fit_funcs = cfg['fit_func']
    label = cfg['label']
    t0 = cfg['fit_params'][0]
    dt = cfg['fit_params'][1]
    prefer = method['prefer']
    window_method = method['window_method']
    fit_params = {}
    for fit_func in fit_funcs:
        suffix = f'{label}_t0{t0}dt{dt}_p{prefer}_{window_method}_{fit_func}'
        report_name = os.path.join(directory, f"sqp_fit_{suffix}.pickle")
        this_fit = load_cache(report_name)
        fit_params[fit_func] = this_fit['fitted']

    return fit_params


def load_table_config(filename: str):
    """
    Load the table configuration file
    containing directories to be processed
    """
    with open(filename, "r") as f:
        table_config = yaml.load(f, Loader=yaml.FullLoader)

    general = table_config.pop('general', {})

    try:
        ordered_config = OrderedDict(sorted(table_config.items(),
                                            key=lambda x: x[1]['order']))
        table_config = ordered_config
    except KeyError:
        print("order key not found in 1 or more entries, using default order")

    return table_config, general


def make_latex_table(df, header_dict={}, rule_str=r'\hrule', nan_str='-'):
    """Generate a latex table from the contents of df

    :param df: Dataframe to tabulate
    :param header_dict: (default {}) Dictionary of header names to display.
    Keys must be columns in df
    :param nan_str: (default '-') String to insert in place of NaN values
    :returns: List of strings containing each row of the table

    """

    err_keys = [p for p in df.columns if '_err' in p]
    param_keys = [rreplace(p, '_err', '', 1) for p in err_keys]
    param_names = [header_dict.get(p, p) for p in param_keys]

    header_keys = [h for h in df.columns
                   if h not in param_keys and h not in err_keys]
    header_names = [header_dict.get(h, h) for h in header_keys]

    # Merge the header into one string
    header = header_names + param_names
    header = ' & '.join(header) + ' \\\\'

    table = [header, rule_str]
    n_rows = len(df.index)
    for i in range(n_rows):
        row = df.iloc[i].copy()
        if 'range' in row:
            row['range'] = '({}, {})'.format(row['range'][0], row['range'][1])
        if 'chisqpdf' in row:
            row['chisqpdf'] = '{:.2f}'.format(row['chisqpdf'])
        row_str_list = [row[h] for h in header_keys]

        param_strings = [sqp.format_error(row[p], row[p_err], 'Invalid')
                         if ~np.isnan(row[p]) else nan_str
                         for (p, p_err) in zip(param_keys, err_keys)]
        row_str_list = row_str_list + param_strings
        row_str_list = [str(p) for p in row_str_list]
        row_string = ' & '.join(row_str_list) + ' \\\\'
        table.append(row_string)

    # Remove trailing '\\' from last row
    table[-1] = rreplace(table[-1], r'\\', '', 1)

    return table


def make_multitable(tables: dict, rule_str: str = r'\hrule') -> list:
    multitable = []

    for name, table in tables.items():
        # If multitable is empty, add the header row
        if not multitable:
            multitable.append(table[0])

        multitable.append(rule_str)
        # Add the multitable header
        sec_str = r'\multicolumn{{6}}{{l}}{{{}}}\\'.format(name)
        multitable.append(sec_str)
        # Insert the table contents
        multitable.extend(table[1:])
        # Re-insert the trailing '\\'
        multitable[-1] = multitable[-1] + r'\\'

    # Remove trailing '\\' from last row
    multitable[-1] = rreplace(multitable[-1], r'\\', '', 1)

    return multitable


def make_sqp_df(df, physical=False, spacings=None):
    """Generate the SQP dataframe from df

    :param df: Dataframe containing the sigma values for the UT and VO results
    :returns: Dataframe containing the UT and VO sigma values, as well as their ratio

    """
    df_sqp = df.loc[(df['type'] == 'UT') | (df['type'] == 'VO')].copy()
    df_sqp = df_sqp[['fit_func',
                     'type',
                     'ensemble',
                     'ensemble_label',
                     'sigma',
                     'sigma_err']]
    df_sqp = df_sqp.dropna(axis=1, how='all')
    if physical:
        for ensemble in df_sqp.ensemble.unique():
            a = spacings[ensemble]
            this_df = df_sqp.loc[df_sqp.ensemble == ensemble, ['sigma', 'sigma_err']]
            this_df = this_df.apply(convert_sigma,
                                    axis='columns',
                                    args=(a,),
                                    result_type='broadcast')
            df_sqp.loc[df_sqp.ensemble == ensemble, ['sigma', 'sigma_err']] = this_df

    groups = df_sqp.groupby('ensemble')

    rows = []

    for ensemble, group in groups:
        sigma_VO = group.loc[group['type'] == 'VO']['sigma'].iloc[0]
        sigma_err_VO = group.loc[group['type'] == 'VO']['sigma_err'].iloc[0]
        group['ratio'] = sigma_VO / group['sigma']
        group['ratio_err'] = ((sigma_err_VO / sigma_VO) ** 2
                              + (group['sigma_err'] / group['sigma']) ** 2)
        group['ratio_err'] = pd.to_numeric(group['ratio_err'])
        group['ratio_err'] = group['ratio'] * np.sqrt(group['ratio_err'])

        row = {'ensemble_label': group['ensemble_label'].iloc[0]}
        row['VO_sigma'] = group.loc[group['type'] == 'VO', 'sigma'].iloc[0]
        row['VO_sigma_err'] = group.loc[group['type'] == 'VO', 'sigma_err'].iloc[0]
        df_UT = group[group['type'] == 'UT']
        for fit_func in df_UT.fit_func.unique():
            row[fit_func + '_sigma'] = df_UT.loc[df_UT['fit_func'] == fit_func,
                                                 'sigma'].iloc[0]
            row[fit_func + '_sigma_err'] = df_UT.loc[df_UT['fit_func'] == fit_func,
                                                     'sigma_err'].iloc[0]
            row[fit_func + '_ratio'] = df_UT.loc[df_UT['fit_func'] == fit_func,
                                                 'ratio'].iloc[0]
            row[fit_func + '_ratio_err'] = df_UT.loc[df_UT['fit_func'] == fit_func,
                                                     'ratio_err'].iloc[0]

        rows.append(row)

    rows = {k: [d.get(k) for d in rows]
            for k in set().union(*rows)}
    df_sqp = pd.DataFrame.from_dict(rows)
    return df_sqp


def generate_table(df, group_by, general_config, suffix):
    """Generate and save all tables based on df and group_by

    :param df: Dataframe of data to tabulate
    :param group_by: Column to group on. Either 'type' or 'ensemble'
    :param suffix: Suffix for the filename

    """
    header_dict = general_config.get('header_values', [])
    nan_str = general_config.get('nan_str', '-')
    rule_str = general_config.get('rule_str', r'\hline')

    group_dfs = {}
    tables = {}
    # Split data into seperate dataframes based on 'group_by'
    groups = df.groupby(group_by)
    for (group_name, group) in groups:
        group_df = group.copy()
        group_df = group_df.drop(group_by, axis=1)
        group_df = group_df.dropna(axis=1, how='all')
        if group_by == 'type':
            group_df = group_df.drop('ensemble', axis=1)
        if group_by == 'ensemble':
            group_df = group_df.drop('ensemble_label', axis=1)

        group_dfs[group_name] = group_df

    if group_by == 'ensemble':
        prefixes = list(table_config.keys())
    else:
        prefixes = [g for g, _ in groups]

    # Generate the tables
    for key, pref in zip(group_dfs, prefixes):
        table = make_latex_table(group_dfs[key], header_dict,
                                 nan_str=nan_str, rule_str=rule_str)
        tables[key] = table
        table_name = os.path.join(tex_dir, f'{pref}_{suffix}.tex')
        write_table(table_name, table)

    # Make multitable
    # Relabel the tables dictionary to use the full names as the keys
    if group_by == 'ensemble':
        for key, cfg in table_config.items():
            fullname = cfg.get('ensemble_name', cfg['label'])
            tables[fullname] = tables.pop(key)

    multitable = make_multitable(tables, rule_str)

    table_name = os.path.join(tex_dir, f'{group_by}_{suffix}_multi.tex')
    write_table(table_name, multitable)


def generate_sqp_table(df, filename,
                       function_strings=None,
                       types=None,
                       header=None,
                       sigma_template='{}',
                       ratio_template='{}'):

    function_strings = {} if function_strings is None else function_strings
    header = {} if header is None else header.copy()
    types = ['sigma', 'ratio'] if types is None else types
    types = [sub for t in types for sub in (t, t + '_err')]
    cols = ['ensemble_label', 'VO_sigma', 'VO_sigma_err']

    if function_strings:
        cols = cols + [f'{key}_{type}' for key in function_strings
                       for type in types]
        cols = [c for c in cols if c in df.columns]
        df = df[cols]

    for key, val in function_strings.items():
        # template = r'\mc{{\sigma_{{\rm UT}}^{{\rm {}}}~\si{{GeV/fm}}}}'
        header[f'{key}_sigma'] = sigma_template.format('UT', val)
        # template = r'\mc{{\sigma_{{\rm VO}}/\sigma_{{\rm UT}}^{{\rm {}}}}}'
        header[f'{key}_ratio'] = ratio_template.format(val)

    header['VO_sigma'] = sigma_template.format('VO', '')

    table = make_latex_table(df, header, rule_str=rule_str)
    # table_name = os.path.join(tex_dir, 'sqp_table_phys.tex')
    write_table(filename, table)


def write_table(filename, table_data):
    """Output the table to a tex file
    """

    with open(filename, 'w+') as f:
        for row in table_data:
            f.write(row + '\n')

    print(f'Saved {filename}')


if __name__ == '__main__':
    table_config, table_general = load_table_config("./table_config.yml")

    metadata = sqp.load_metadata("/g/data/e31/jb3708/data/MetaData/ConfigInfo.yml")
    spacings = {key: metadata[key]['a'] for key in table_config}
    method = {'prefer': table_general.get('prefer', 'min'),
              'window_method': table_general.get('window_method', 'fixed')}
    header = table_general.get('header_values', [])
    drop_columns = table_general.get('drop_columns', [])
    nan_str = table_general.get('nan_str', '-')
    rule_str = table_general.get('rule_str', r'\hline')

    tex_dir = "./"
    if not os.path.exists(tex_dir):
        os.makedirs(tex_dir)

    df = get_dataframe(table_config,
                       drop_columns=drop_columns)
    df_plot = get_plot_df(df, table_config)

    # Set strings to insert into template for various functions
    function_strings = table_general.get('function_strings', {})

    # Make string tension tables if the UT and VO columns are present
    if ('UT' in df['type'].values) and ('VO' in df['type'].values):
        # Make physical tables
        df_sqp = make_sqp_df(df, physical=True, spacings=spacings)
        df_sqp_plot = make_sqp_df(df_plot, physical=True, spacings=spacings)

        sigma_template = r'\mc{{\sigma_{{\rm UT}}^{{\rm {}}}~\si{{GeV/fm}}}}'
        ratio_template = r'\mc{{\sigma_{{\rm VO}}/\sigma_{{\rm UT}}^{{\rm {}}}}}'

        table_name = os.path.join(tex_dir, 'sqp_table_phys_allfunctions.tex')
        generate_sqp_table(df_sqp, table_name,
                           function_strings=function_strings,
                           types=['ratio'],
                           header=header,
                           sigma_template=sigma_template,
                           ratio_template=ratio_template)

        table_name = os.path.join(tex_dir, 'sqp_table_phys.tex')
        generate_sqp_table(df_sqp_plot, table_name,
                           types=['sigma', 'ratio'],
                           function_strings={'cornell': ''},
                           header=header,
                           sigma_template=sigma_template,
                           ratio_template=ratio_template)

        # Make lattice tables
        df_sqp = make_sqp_df(df)
        df_sqp_plot = make_sqp_df(df_plot)
        sigma_template = r'\mc{{a^2\,\sigma_{{\rm {}}}^{{\rm {}}}}}'
        ratio_template = r'\mc{{\sigma_{{\rm VO}}/\sigma_{{\rm UT}}^{{\rm {}}}}}'

        table_name = os.path.join(tex_dir, 'sqp_table_lat_allfunctions.tex')
        generate_sqp_table(df_sqp, table_name,
                           function_strings=function_strings,
                           types=['ratio'],
                           header=header,
                           sigma_template=sigma_template,
                           ratio_template=ratio_template)

        table_name = os.path.join(tex_dir, 'sqp_table_lat.tex')
        generate_sqp_table(df_sqp_plot, table_name,
                           types=['sigma', 'ratio'],
                           function_strings={'cornell': ''},
                           header=header,
                           sigma_template=sigma_template,
                           ratio_template=ratio_template)

    df_plot = df_plot.drop(['fit_func'], axis=1)
    generate_table(df_plot, 'ensemble', table_general, 'table')
    generate_table(df_plot, 'type', table_general, 'table')
    generate_table(df, 'ensemble', table_general, 'table_allfunctions')
    generate_table(df, 'type', table_general, 'table_allfunctions')

    # Cleanup function strings
    function_strings = {key: f'$V_{{\\rm {f}}}$' for key, f in function_strings.items()}
    function_strings.pop('cornell')
    for key, val in function_strings.items():
        if key.endswith('linear'):
            function_strings[key] = function_strings[key][:-1] + r' + \sigma\, r$'

    keep_functions = list(function_strings.keys())
    mask = df['fit_func'].isin(keep_functions)
    df_funcs = df.loc[mask, :].copy()
    df_funcs = df_funcs.replace({'fit_func': function_strings})
    df_funcs['type'] = pd.Categorical(df_funcs.type, categories=['VR', 'UT'], ordered=True)
    df_funcs = df_funcs.sort_values('type')
    ut_rho = df_funcs.loc[df_funcs.type == 'UT','rho']
    df_funcs.loc[df_funcs.type == 'UT','rho'] = ut_rho.astype(float).round(2)
    generate_table(df_funcs, 'ensemble', table_general, 'table_selectfunctions')
