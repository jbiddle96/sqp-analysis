import numpy as np
from numpy.linalg import inv
import pandas as pd
import os
import sys
import glob
import shutil
import matplotlib as mpl
from scipy.optimize import curve_fit
import itertools as it
import yaml
from math import floor, log10
from copy import deepcopy
import astropy.units as u
import natpy
from cache import cache, load_cache
import fit_functions as ff

mpl.use('Agg')
mpl.rc('text', usetex=True)
mpl.rc('font', family='serif')
mpl.rcParams['axes.unicode_minus'] = False
mpl.rc('text.latex', preamble=r'\usepackage{amsmath}')


def assign_dir(dir_name, clean=False):
    """Assign a directory and ensure it exists

    :param dir_name: The directory to assign
    :param clean: (Optional, default=False) Empty the directory
    :returns dir_name:

    """
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    if clean:
        for filename in os.listdir(dir_name):
            file_path = os.path.join(dir_name, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))

    return dir_name


def merge_structured_arrays(array1, array2):
    """
    Merge 2 structured arrays with the same dtype.
    """
    n1 = len(array1)
    n2 = len(array2)
    array_out = array1.copy()
    array_out.resize(n1 + n2)
    array_out[n1:] = array2
    return array_out


def find_files(base_dir, pattern):
    """Find files in a directory according to pattern

    :param base_dir: Directory to search
    :param pattern: Glob pattern to match
    :returns file_list: List of matched files

    """

    full_pattern = os.path.join(base_dir, pattern)
    file_list = glob.glob(full_pattern)
    file_list.sort()
    return file_list


def fit_t_window(emf, sigma, t_min, t_max):
    """Fit emf using covariance matrix sigma at fixed x to the
    window t_min - t_max.

    :param emf: Effective mass to fit
    :param sigma: Covariance matrix of the effective mass
    :param t_min: Start of the fit window
    :param t_max: End of the fit window
    :returns (fit, chisq, chisqpdf): The fit value, chisquared and chisq/dof

    """
    window_size = t_max - t_min + 1
    # Fitting to a constant so t_window just needs to be the right size
    t_window = np.arange(window_size)
    emf_window = emf[t_min - 1: t_max]
    sigma_window = sigma[t_min - 1: t_max, t_min - 1: t_max]
    if len(emf_window) == 0:
        print("Something went wrong, tmin={}, tmax={}".format(t_min, t_max))
        print("EMF window")
        print(emf_window)
        sys.exit()
    fit, pcov = curve_fit(
        ff.f_const, t_window, emf_window, sigma=sigma_window, absolute_sigma=True
    )
    fit = fit[0]
    dof = window_size - 1
    r = emf_window - fit * np.ones(window_size)
    chisq = r.T @ inv(sigma_window) @ r
    chisqpdf = chisq / dof
    return fit, chisq, chisqpdf


def calc_jack_err(samples, axis=0):
    """Calculate the jackknife error of a set of jackknife ensembles

    :param samples: 1d or 2d array, containing jackknife ensemble(s)
    :param axis: Optional, default = 0. In the case of a 2d array, the axis to be jackknifed over
    :returns err: The jackknife error

    """

    samples = np.array(samples)
    shape = np.shape(samples)

    if len(shape) == 1:
        n_jack = shape[0]
        err = np.sqrt(
            (n_jack - 1) * np.mean((samples[:] - np.mean(samples[:])) ** 2))
    elif len(shape) == 2:
        n_jack = shape[axis]
        n_samples = shape[(axis + 1) % 2]
        err = np.zeros(n_samples)
        for i in range(n_samples):
            this_sample = np.take(samples, i, axis=(axis + 1) % 2)
            # print(this_sample)
            err[i] = np.sqrt(
                (n_jack - 1) * np.mean((this_sample - np.mean(this_sample)) ** 2))
    else:
        raise ValueError(f'Invalid value of axis = {axis}')
    return err


def calc_cov(V_in, V_jack_in, r_min, r_max):
    V = deepcopy(V_in)
    V_jack = deepcopy(V_jack_in)
    V = V[(V['r'] >= r_min) & (V['r'] <= r_max)]
    n_points = len(V)
    sample_size = len(V_jack)

    mat = np.zeros((n_points, sample_size))
    for i, col in enumerate(V_jack):
        this_sample = col['V'][(col['r'] >= r_min) & (col['r'] <= r_max)]
        mat[:, i] = this_sample

    cov = np.cov(mat)
    cov *= sample_size / (sample_size - 1)
    return cov


def convert_param(param, param_err, label, a):
    """
    Converts the sqp parameters to physical units
    using lattice spacing a.
    """
    a = a * u.fm

    if label == 'V0':
        V0 = param / a
        V0_err = param_err / a
        V0 = natpy.convert(V0, u.GeV).value
        V0_err = natpy.convert(V0_err, u.GeV).value
        return V0, V0_err

    if label == 'alpha':
        alpha = param
        alpha_err = param_err
        alpha = natpy.convert(alpha, u.GeV * u.fm).value
        alpha_err = natpy.convert(alpha_err, u.GeV * u.fm).value
        return alpha, alpha_err

    if label == 'sigma':
        sigma = param / a ** 2
        sigma_err = param_err / a ** 2
        # sigma = natpy.convert(sigma, u.GeV / u.fm).value
        # sigma_err = natpy.convert(sigma_err, u.GeV / u.fm).value
        sigma = natpy.convert(sigma, u.GeV / u.fm).value
        sigma_err = natpy.convert(sigma_err, u.GeV / u.fm).value
        return sigma, sigma_err

    raise ValueError(f'Label "{label}" not recognised, cannot convert')


def round_sf(num, sf=1):
    """Round to a specified number of significant figures"""
    return round(num, (sf - 1) - floor(log10(abs(num))))


def mantissa_and_exponent(num):
    """Find the mantissa and exponent of num"""
    e = floor(log10(abs(num)))
    m = num / (10 ** e)
    return m, e


def format_error(val, err, invalid_str=''):
    """Format a number and it's error to read x.x(e).

    Formatting is based on the highest significant digit of the error.

    :param val: Value to format
    :param err: Error in the value
    :param invalid_str: (default '') Return value in case of an invalid input.

    """
    if err == 0:
        return f'{val}(0)'

    val_m, val_e = mantissa_and_exponent(val)
    err_m, err_e = mantissa_and_exponent(err)
    if err_e > val_e:
        print(f'Error of {err} is more significant than value of {val}, returning')
        return invalid_str

    diff_e = err_e - val_e

    # Round value to the most significant digit of err
    val = round(val_m, -diff_e) * 10 ** val_e
    # Round err to its most significant digit
    err = round(err, -err_e)

    # Get the most significant digit of err as an integer
    msd_err = round(err / (10**floor(log10(err))))

    if(err_e < 1):
        string = '{num:0.{dp}f}'.format(num=val, dp=-err_e) + f'({msd_err})'
    else:
        string = '{num:0.{dp}e}'.format(num=val, dp=-diff_e) + f'({msd_err})'
    return string

# |-------------------------- I/O routines --------------------------|


def load_EMF_data(config_info, var_params, split_on_off=False, energy_level=None):
    """Load EMF data for configuration specified in config_info

    :param config_info: Configuration metadata
    :param var_params: t0, dt to load
    :param split_on_off: (Optional, default = False)
    Toggle splitting the returned data in on and off-axis data
    :param energy_level: (Optional, default = None)
    Select a single energy level to load. Otherwise load all
    :returns: Effective mass data

    """

    data_dir = config_info['emfdir']
    x_max = config_info.get('x_max', None)
    t0, dt = var_params

    if config_info['nsmear'] == 1:
        data_file = "emf.dat"
    else:
        data_file = "emf_t0{}dt{}.dat".format(t0, dt)
    f = os.path.join(data_dir, data_file)

    print("Reading", f)
    tp = np.dtype([('x', int),
                  ('y', int),
                  ('z', int),
                  ('t', int),
                  ('smear', int),
                  ('jack', int),
                  ('emf', float),
                  ('emf_err', float)])
    names = ['x', 'y', 'z', 't', 'smear', 'jack', 'emf', 'emf_err']
    df = pd.read_csv(f, sep=r'\s+', dtype=tp, header=None, names=names)
    s = df.dtypes
    data = np.array([tuple(x) for x in df.values],
                    dtype=list(zip(s.index, s)))

    if x_max is not None:
        data = data[data['x'] <= x_max]

    if energy_level is not None:
        data = data[data['smear'] == energy_level]

    if split_on_off:
        # Split data into on and off axis
        onaxis = (data['y'] == 0) & (data['z'] == 0)
        offaxis = (data['y'] > 0) | (data['z'] > 0)

        data = {"OnAxis": data[onaxis],
                "OffAxis": data[offaxis]}

    return data


def load_correlator_ratio(config_info):
    """Load correlator ratio data for the configuration specified by config_info

    :param config_info: Configuration metadata dictionary
    :returns data: Array of correlator ratio data

    """

    data_dir = config_info['emfdir']

    data_file = "avg_corr_off-on_diag_ratio.dat"
    f = os.path.join(data_dir, data_file)

    print("Reading", f)
    tp = np.dtype([('x', int),
                   ('y', int),
                   ('z', int),
                   ('t', int),
                   ('ratio', float)])
    data = np.loadtxt(f, dtype=tp)

    return data


def load_correlator_components(config_info, error=False):
    """Load correlator component data for the configuration specified by config_info

    :param config_info: Configuration metadata dictionary
    :returns data: Array of correlator component data

    """

    data_dir = config_info['emfdir']
    nsmear = int(config_info['nsmear'])
    data_file = "norm_corr_components.dat"
    f = os.path.join(data_dir, data_file)

    print("Reading", f)
    if error:
        tp = np.dtype([('x', int), ('y', int), ('z', int), ('t', int),
                       ('g', float, (nsmear, nsmear)),
                       ('g_err', float, (nsmear, nsmear))])
    else:
        tp = np.dtype([('x', int), ('y', int), ('z', int), ('t', int),
                       ('g', float, (nsmear, nsmear))])

    data = np.loadtxt(f, dtype=tp)

    return data


def load_single_data(config_info, split_on_off=True, datatype='emf'):
    """Load single EMF or correlator data from all files in data_dir

    :param config_info: Configuration metadata dictionary
    :param split_on_off: (Optional, default = True)
    Toggle splitting the data into off and on-axis components
    :param datatype: (Optional, default = 'emf') Control whether to load correlators ('corr')
    or effective masses ('emf')
    :returns data_dict: Dictionary of data keyed by (t0, dt)

    """
    data_dir = config_info['emfdir']
    nsmear = config_info['nsmear']
    x_max = config_info.get('x_max', None)
    data_dict = {}

    if nsmear > 1:
        ijcomb = list(it.product(range(nsmear), repeat=2))
    else:
        ijcomb = [(0, 0)]

    for ismear, jsmear in ijcomb:
        if datatype == 'emf':
            if nsmear > 1:
                data_file = "emf_single_i{}j{}.dat".format(
                    ismear + 1, jsmear + 1)
            else:
                data_file = "emf_single.dat"
        elif datatype == 'corr':
            if nsmear > 1:
                data_file = "corr_single_i{}j{}.dat".format(
                    ismear + 1, jsmear + 1)
            else:
                data_file = "corr_single.dat"
        f = os.path.join(data_dir, data_file)

        print("Reading", f)
        tp = np.dtype([('x', int),
                      ('y', int),
                      ('z', int),
                      ('t', int),
                      ('smear', int),
                      ('jack', int),
                      ('emf', float),
                      ('emf_err', float)])
        data = np.loadtxt(f, dtype=tp)

        if x_max is not None:
            data = data[data['x'] <= x_max]

        if split_on_off:
            # Split data into on and off axis
            onaxis = (data['y'] == 0) & (data['z'] == 0)
            offaxis = (data['y'] > 0) | (data['z'] > 0)

            data_dict[ismear, jsmear] = {"OnAxis": data[onaxis],
                                         "OffAxis": data[offaxis]}

        else:
            data_dict[ismear, jsmear] = data

    return data_dict


@cache
def load_C_data(config_info, var_params, t_max, split_on_off=False):
    """Load covariance data for configuartion specified by config_info

    :param config_info: Configuration metadata
    :param var_params: t0, dt to load
    :param t_max: Maximum temporal extent of data
    :param split_on_off: (Optional, default = False)
    Toggle splitting the returned data in on and off-axis data

    :returns C_data: Covariance matrix data
    """
    data_dir = config_info['emfdir']
    x_max = config_info.get('x_max', None)
    t0, dt = var_params

    if config_info['nsmear'] == 1:
        data_file = "C.dat"
    else:
        data_file = "C_t0{}dt{}.dat".format(t0, dt)
    f = os.path.join(data_dir, data_file)

    print("Reading", f)
    tp = np.dtype([('x', int), ('y', int), ('z', int), ('smear', int),
                   ('jack', int), ('C', float, (t_max, t_max))])

    data = np.loadtxt(f, dtype=tp)
    # Note: no need to transpose due to fortran's column-major
    # output as covariance matrix is symmetric
    if x_max is not None:
        data = data[data['x'] <= x_max]

    if split_on_off:
        # Split into on-axis and off-axis
        onaxis = (data['y'] == 0) & (data['z'] == 0)
        offaxis = (data['y'] > 0) | (data['z'] > 0)

        data = {"OnAxis": data[onaxis],
                "OffAxis": data[offaxis]}

    return data


def load_f90_V_data(data_dir, var_params, windows,
                    x_max=None, chisqmax=None, onaxis=False):
    """Load potential data from all files in data_dir
    with variational parameters specified by var_params.

    :param data_dir: Location of the data
    :param var_params: Variational parameter pair tuple
    :param windows: Tuple or dictionary specifying windows to load
    If a tuple, specifies a fixed t_min, t_max pair to load.
    If a dictionary, each element specifies x: (t_min, t_max),
    a fit window for each x value.
    :param x_max: (Optional, default = None) Maximum x value to load
    :param chisqmax: (Optional, default = None) maximum chisq/dof to load
    :param onaxis: (Optional, default = False) Toggle loading just on-axis data
    :returns V, V_jack: The 0'th and 1st order jackknife results

    """

    t0, dt = var_params
    data_file = "fit_data_t0{}-dt{}.dat".format(t0, dt)
    f = os.path.join(data_dir, data_file)

    print("Reading", f)
    tp = np.dtype([('x', int),
                   ('y', int),
                   ('z', int),
                   ('jack', int),
                   ('t_min', int),
                   ('t_max', int),
                   ('V', float),
                   ('V_err', float),
                   ('chisqpdf', float)])
    # data = np.loadtxt(f, dtype=tp, skiprows=1)
    names = ['x', 'y', 'z', 'jack', 't_min', 't_max', 'V', 'V_err', 'chisqpdf']
    # Load using pandas for speed
    df = pd.read_csv(f, sep=r'\s+', dtype=tp, header=0, names=names)
    s = df.dtypes
    data = np.array([tuple(x) for x in df.values],
                    dtype=list(zip(s.index, s)))

    # Slice array on a single window
    if isinstance(windows, tuple):
        t_min = windows[0]
        t_max = windows[1]
        data = data[(data['t_min'] == t_min)
                    & (data['t_max'] == t_max)]
    elif isinstance(windows, dict):
        # Slice each x value on a seperate window
        new_data = []
        for x in windows:
            this_data = data[(data['x'] == x[0])
                             & (data['y'] == x[1])
                             & (data['z'] == x[2])]
            t_min = windows[x][0]
            t_max = windows[x][1]
            this_data = this_data[(this_data['t_min'] == t_min)
                                  & (this_data['t_max'] == t_max)]
            # print(t_min, t_max)
            # print(this_data)
            if len(this_data) == 0:
                print(f'No data for x = {x}')
                print(f'Using window {t_min}-{t_max}')
            new_data.extend(this_data)
        data = np.array(new_data, dtype=tp)
    else:
        raise TypeError(f'windows has unexpected type {type(windows)}')

    if onaxis:
        onaxis_mask = (data['y'] == 0) & (data['z'] == 0)
        data = data[onaxis_mask]

    if x_max is not None:
        data = data[data['x'] <= x_max]

    if chisqmax is not None:
        data = data[data['chisqpdf'] <= chisqmax]

    tp = np.dtype([('r', float),
                   ('V', float),
                   ('V_err', float)])
    n_points = len(data[data['jack'] == 0])
    V = np.zeros(n_points, dtype=tp)

    data0 = data[data['jack'] == 0]
    V['r'] = np.sqrt(data0['x'] ** 2
                     + data0['y'] ** 2
                     + data0['z'] ** 2)
    V['V'] = data0['V']
    V['V_err'] = data0['V_err']
    V.sort(order='r')

    jack_range = np.sort(np.unique(data["jack"]))[1:]
    n_jack = len(jack_range)
    tp = np.dtype(
        [
            ("r", float, (n_points)),
            ("V", float, (n_points)),
            ("V_err", float, (n_points)),
        ]
    )

    V_jack = np.zeros(n_jack, dtype=tp)

    if len(V) == 0:
        return V, V_jack

    for ij, j in enumerate(jack_range):
        data_jack = data[data['jack'] == j]
        V_jack[ij]['r'] = np.sqrt(data_jack['x'] ** 2
                                  + data_jack['y'] ** 2
                                  + data_jack['z'] ** 2)
        V_jack[ij]['V'] = data_jack['V']
        V_jack[ij]['V_err'] = data_jack['V_err']
        if n_points > 1:
            mask = np.argsort(V_jack[ij]['r'])
            V_jack[ij]['r'] = V_jack[ij]['r'][mask]
            V_jack[ij]['V'] = V_jack[ij]['V'][mask]
            V_jack[ij]['V_err'] = V_jack[ij]['V_err'][mask]

    return V, V_jack


def load_config(config_name):
    """Load config yaml file and return the ensemble metadata.

    This function should be used to obtain metadata
    to be passed to other analysis routines.

    A 'general' key may be supplied, with its values being copied
    to all configurations that don't specify an option

    :param config_name: Name of the yaml file to be loaded
    :returns result: Tuple of ensemble metadata dictionaries

    """
    config_file = open(config_name, 'r')
    config = yaml.load(config_file, Loader=yaml.FullLoader)

    # Obtain configuration-independent data
    general = config.pop('general', None)
    if general is not None:
        general['options'] = set_options(general)

    for key in config:
        config[key]['fit_params'] = tuple(config[key]['fit_params'])
        t0_list = np.arange(config[key]['t0_min'],
                            config[key]['t0_max'] + 1, 1)
        dt_list = np.arange(config[key]['dt_min'],
                            config[key]['dt_max'] + 1, 1)
        config[key]['var_params'] = list(it.product(t0_list, dt_list))
        config[key].setdefault('fit_func', [])
        if not isinstance(config[key]['fit_func'], list):
            config[key]['fit_func'] = [config[key]['fit_func']]
        if 'fit_max' not in config[key]:
            config[key]['fit_max'] = None
        if 'fit_min' not in config[key]:
            config[key]['fit_min'] = None
        if ('plot_func' not in config[key]) and (len(config[key]['fit_func']) == 1):
            config[key]['plot_func'] = config[key]['fit_func'][0]
        elif 'plot_func' not in config[key]:
            config[key]['plot_func'] = ''

    result = [config[cfg] for cfg in config.keys()]
    if len(result) == 0:
        raise ValueError("Configuration list is empty.")

    # Copy general data into each config if it's not already specified
    if general is not None:
        for data in result:
            for key in general:
                if data.get(key) is None:
                    data[key] = general[key]

    return tuple(result)


def set_options(cfg_dict):
    """Process plot options

    :param cfg_dict: Input dictionary
    :returns options: List of all options if a specific option was not specified

    """

    # Use user-specified fit method if possible
    # Otherwise, run all available fit methods
    prefer = cfg_dict.get('prefer', None)
    window_method = cfg_dict.get('window_method', None)
    if prefer is None:
        fit_options = ['min', 'max']
    else:
        fit_options = [prefer]

    if window_method is None:
        window_options = ['fixed', 'variable']
    else:
        window_options = [window_method]

    options = list(it.product(fit_options, window_options))
    return options


def load_metadata(filename):
    with open(filename, "r") as f:
        metadata_dict = yaml.load(f, Loader=yaml.FullLoader)
    return metadata_dict


def load_V_data(directory, fname):
    """Load the potential data as output by fit_sqp.py.
    Data is in a convenient format for plotting

    :param directory: Directory containing the data
    :param fname: File name to load
    :returns V: Stuctured array containing the potential data

    """
    fname = os.path.join(directory, fname)
    V = np.loadtxt(fname, dtype=[("r", float), ("V", float), ("V_err", float)])
    return V


def load_sqp_data(directory, fname):
    """Load the moving average data as output by fit_sqp.py.
    Data is in a convenient format for plotting

    :param directory: Directory containing the data
    :param fname: File name to load
    :returns sqp: Stuctured array containing the moving average data

    """
    fname = os.path.join(directory, fname)
    print("Reading ", fname)
    try:
        sqp = np.loadtxt(
            fname, dtype=[("r_avg", float),
                          ("sigma", float),
                          ("sigma_err", float),
                          ("chisqpdf", float)]
        )
    except OSError as e:
        print(e)
        return None

    return sqp


def load_fit_data(directory, cfg, suffix):
    """Load the numerical fit data as output by fit_sqp.py.
    Result contains mean values as well as 1-sigma error bounds

    :param directory: Directory containing the data
    :param cfg: Configuration metadata
    :returns fit_data: Dictionary of stuctured arrays containing the fit data.
    Dictionary is keyed by the available fit functions.

    """
    fit_data = {}
    fit_functions = cfg['fit_func']
    for fit_func in fit_functions:
        fname = f"V_{suffix}_{fit_func}.dat"
        fname = os.path.join(directory, fname)
        print("Reading ", fname)
        fit_data[fit_func] = np.loadtxt(fname,
                                        dtype=[("r", float),
                                               ("mean", float),
                                               ("upper", float),
                                               ("lower", float)])
    return fit_data


def load_fit_params(directory, cfg, suffix):
    """Load a dictionary of fit parameters from a pickle.

    :param directory: Directory containing the data
    :param cfg: Configuration metadata
    :returns: Dictionary of dictionaries keyed by the available fit functions.

    """
    fit_params = {}
    for fit_func in cfg['fit_func']:
        report_name = os.path.join(
            directory, f"sqp_fit_{suffix}_{fit_func}.pickle")
        fit_params[fit_func] = load_cache(report_name)

    return fit_params


def load_eigenvalues(config_info, var_params, split_on_off=False):
    data_dir = config_info['emfdir']
    x_max = config_info.get('x_max', None)
    t0, dt = var_params

    data_file = "eval_t0{}dt{}.dat".format(t0, dt)
    f = os.path.join(data_dir, data_file)

    print("Reading", f)
    tp = np.dtype([('x', int),
                  ('y', int),
                  ('z', int),
                   ])
    names = ['x', 'y', 'z', 't', 'smear', 'jack', 'emf', 'emf_err']
    df = pd.read_csv(f, sep=r'\s+', dtype=tp, header=None, names=names)
    s = df.dtypes
    data = np.array([tuple(x) for x in df.values],
                    dtype=list(zip(s.index, s)))

    if x_max is not None:
        data = data[data['x'] <= x_max]

    if split_on_off:
        # Split data into on and off axis
        onaxis = (data['y'] == 0) & (data['z'] == 0)
        offaxis = (data['y'] > 0) | (data['z'] > 0)

        data = {"OnAxis": data[onaxis],
                "OffAxis": data[offaxis]}

    return data
