t0_min=1
t0_max=2
dt_min=1
dt_max=3

for (( t0=$t0_min; t0<=$t0_max; t0++ ))
do
    for (( dt=$dt_min; dt<=$dt_max; dt++ ))
    do
	qsub -v t0=$t0,dt=$dt sqp.sh
    done

done
