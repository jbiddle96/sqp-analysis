#!/bin/bash
#PBS -S /bin/bash
#PBS -P e31
#PBS -q normal
#PBS -l ncpus=1
#PBS -l mem=16gb
#PBS -l walltime=4:00:00
#PBS -l storage=gdata/e31
#PBS -l wd
#PBS -j oe

ulimit -s unlimited

exeName="/home/566/jb3708/sqp_analysis/bin/EffectiveMass.x"

# Set number of ape sweeps to match Wilson loops
nSmear=3
smearSweeps="6 10 18"

datadir="/g/data/e31/jb3708/data/VarWilsonLoops/UT/32x64_PG/"
outdir="./EMF/"
mkdir -p ${outdir}

input="InputParams"

# Make file list and remove header
dirlist.py "${datadir}" "su3*" ".lat" ".log" ".wloop" --dropext
listname=${datadir}"dirlist.txt"

# Clean old files
rm -f ${outdir}*.dat

# Variation suffix
baseSuffix=".unWi-j-a7t12"

# Set wilson loop extents
nx2=16
ny2=3
nt2=12

# Set variational parameter ranges
t0_min=1
t0_max=2
dt_min=1
dt_max=3

sortEMF=F

nbins=0

cat <<EOF > $input".emf"
$datadir
$listname
$baseSuffix
$nx2
$ny2
$nt2
$nSmear
$smearSweeps
$t0_min
$t0_max
$dt_min
$dt_max
$sortEMF
$outdir
$nbins
EOF

$exeName <<EOF > $outdir"report.out"
$input
EOF

# rm -f *.sqp
