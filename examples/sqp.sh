#!/bin/bash
#PBS -S /bin/bash
#PBS -P gb41
#PBS -q normal
#PBS -l ncpus=1
#PBS -l mem=16gb
#PBS -l walltime=16:00:00
#PBS -l storage=gdata/e31
#PBS -l wd
#PBS -j oe

ulimit -s unlimited

# t0=1
# dt=1

exeName="/home/566/jb3708/sqp_analysis/bin/SQP.x"

# Set number of ape sweeps to match Wilson loops
# nape=10

datadir="/g/data/e31/jb3708/data/VarWilsonLoops/UT/32x64_PG/"
outdir="./SQP/"
mkdir -p $outdir

# outStub=$outdir"VJack/"
# mkdir -p $outStub

input="InputParams-t0${t0}-dt${dt}"
#exeName =  "sqpfitwindow."$size"n"$ncon".x"

# Make file list and remove header
dirlist.py "${datadir}" "su3*" ".lat" ".log" ".wloop" --dropext
listname=${datadir}"dirlist.txt"

# Variation suffix
baseSuffix=".unWi-j-a7t12"

# Set wilson loop extents
nx2=16
ny2=3
nt2=12

# ncon=$(ls $datadir*-nape$nape | wc -l)

# Set start and end times
itb=4
jtb=5
ite=8

# Variational parameters
nSmear=3
smearSweeps="6 10 18"

# Set number of bins. Choose 0 to not bin data
nbins=0

cat <<EOF > $input".sqp"
$datadir
$listname
$baseSuffix
$nx2
$ny2
$nt2
$itb        ntb1
$jtb        ntb2
$ite        nte2
$nSmear
$smearSweeps
$t0
$dt
$outdir
$nbins
EOF

$exeName <<EOF > $outdir"reportfit"$fit".out"
$input
EOF

# rm -f *.sqp
